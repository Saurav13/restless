<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quarter extends Model
{
    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    public function getStartDateAttribute(){
        return $this->start_month.'-01';
    }

    public function getEndDateAttribute(){
        return $this->end_month.'-01';
    }

    public function getTermAttribute(){
        $years = $this->project->years;
        $quarters = $this->project->quarters()->where('year',$this->year)->get();
        $c = 1;
        foreach($quarters as $quarter){
            if($quarter->id == $this->id){
                return 'Quater '.$c.' Of Year '.$this->year;
            }
            $c += 1;
        }
    }
}
