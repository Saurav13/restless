<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MNECategory extends Model
{
    protected $table="mne_categories";
}
