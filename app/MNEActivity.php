<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MNEActivity extends Model
{
    protected $table="mne_activities";

    public function project()
    {
        return $this->belongsTo('App\Project');

    }

    public function quarter()
    {
        return $this->belongsTo('App\Quarter');
    }
    
    public function budget_items()
    {
        return $this->morphMany('App\BudgetItem', 'indicatorable');
    }

    public function getExpenseAttribute(){
        $expense = 0;

        foreach($this->budget_items as $item){
            $expense += $item->budget_plans()->sum('amount');
        }

        return $expense;
    }
}
