<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IndicatorBeneficiary extends Model
{
    protected $table = 'indicator_beneficiary';

    public function beneficiary()
    {
        return $this->belongsTo('App\Beneficiary');
    }
}
