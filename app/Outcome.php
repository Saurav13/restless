<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outcome extends Model
{
    public function project()
    {
        return $this->belongsTo('App/Project');
    }

    public function outputs()
    {
        return $this->hasMany('App/Output');
    }
    
    public function indicators()
    {
        return $this->morphMany('App\Indicator', 'indicatorable');
    }

    public function indicator_beneficiaries()
    {
        return $this->hasManyThrough('App\IndicatorBeneficiary', 'App\Indicator','indicatorable_id')->where('indicatorable_type','App\Outcome');
    }

    public function progress_logs()
    {
        return $this->morphMany('App\ProgressLog', 'loggable');
    }
}
