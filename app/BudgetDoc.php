<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetDoc extends Model
{
    public function uploader()
    {
        return $this->belongsTo('App\User', 'created_by');
    }
}
