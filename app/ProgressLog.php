<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgressLog extends Model
{
    public function creater()
    {
        return $this->belongsTo('App\User', 'created_by');
    }
}
