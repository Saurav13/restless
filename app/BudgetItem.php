<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetItem extends Model
{
    protected $appends = ['expense'];
    
    public function budget_plans()
    {
        return $this->hasMany('App\BudgetPlan')->orderBy('year')->orderBy('month');
    }

    public function getExpenseAttribute(){
        return $this->budget_plans()->sum('amount');
    }
}
