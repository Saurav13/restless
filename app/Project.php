<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Output;


class Project extends Model
{
    public function outcomes()
    {
        return $this->hasMany('App\Outcome');
    }

    public function beneficiaries()
    {
        return $this->hasMany('App\Beneficiary');
    }

    public function outputs()
    {
        return $this->hasManyThrough('App\Output', 'App\Outcome');
    }
    
    public function activities()
    {
        $output_ids=$this->outputs->pluck('id');
        return Activity::whereIn('output_id',$output_ids)->get();
    }

    public function getStartYearAttribute()
    {
        return strval(Carbon::parse($this->start_date)->format('Y'));
    }
    public function getStartMonthAttribute()
    {
        return strval(Carbon::parse($this->start_date)->format('m'));
    }
    public function getEndYearAttribute()
    {
        return strval(Carbon::parse($this->end_date)->format('Y'));
    }
    public function getEndMonthAttribute()
    {
        return strval(Carbon::parse($this->end_date)->format('m'));
    }
    public function getStartTermAttribute()
    {
        return strval(Carbon::parse($this->start_date)->format('Y-m'));
    }
    public function getEndTermAttribute()
    {
        return strval(Carbon::parse($this->end_date)->format('Y-m'));
    }
    public function  termArray($year)
    {
        $start_year = Carbon::parse($this->start_date);
        $end_year = Carbon::parse($this->end_date);
        $i = 1;
        
        while($start_year <= $end_year){
            
            $start = $start_year->format('Y-m');
            if($i==$this->n_years){
                $end=$end_year->format('Y-m');
            }
            else
                $end = $start_year->addMonths(11)->format('Y-m');

            if($end > $end_year) $end = $end_year->format('Y-m');
            

            if($i == $year) {
                $period = \Carbon\CarbonPeriod::create($start,'1 month',$end)->toArray();

                $dates = [];
                foreach ($period as $date) {
                    $dates []= $date->format('Y-m');
                }

                return $dates;
            }

            $i++;
            $start_year->addMonth();

        }
        // return "non";

        
    }
    public function getStartMonthOfYear($n)
    {
        return Carbon::parse($this->start_date)->addMonths(12*($n-1))->format('Y-m');

    }
    public function getEndMonthOfYear($n)
    {
        if($this->n_years==$n)
            return Carbon::parse($this->end_date)->format('Y-m');
        else
            return Carbon::parse($this->start_date)->addMonths(12*($n-1)+11)->format('Y-m');
    }
   
    public function getNYearsAttribute()
    {

        $start_year=Carbon::parse($this->start_date)->format('Y');
        $start_month=Carbon::parse($this->start_date)->format('m');

        $end_year=Carbon::parse($this->end_date)->format('Y');
        $end_month=Carbon::parse($this->end_date)->format('m');

        $n_years=$end_year-$start_year;
        
        if($end_month>=$start_month)
            $n_years+=1;
        return $n_years;
    }

    public function indicators()
    {
        return $this->morphMany('App\Indicator', 'indicatorable');
    }

    public function partners()
    {
        return $this->belongsToMany('App\Partner', 'project_partners', 'project_id', 'partner_id');
    }

    public function quarters()
    {
        return $this->hasMany('App\Quarter')->orderBy('year')->orderBy('start_month');
    }

    public function geographic_regions()
    {
        return $this->hasMany('App\GeographicRegion');
    }

    public function mneactivities()
    {
        return $this->hasMany('App\MNEActivity');
    }

    public function narrative_records()
    {
        return $this->hasMany('App\NarrativeRecord');
    }

    public function budget_docs()
    {
        return $this->hasMany('App\BudgetDoc');
    }

    public function project_docs()
    {
        return $this->hasMany('App\ProjectDoc');
    }

    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    public function indicator_beneficiaries()
    {
        return $this->hasManyThrough('App\IndicatorBeneficiary', 'App\Indicator','indicatorable_id')->where('indicatorable_type','App\Project');
    }
    
    public function progress_logs()
    {
        return $this->morphMany('App\ProgressLog', 'loggable');
    }

    public function getYearsAttribute(){

        $start_year = Carbon::parse($this->start_date);
        $end_year = Carbon::parse($this->end_date);
        $i = 1;
        while($start_year < $end_year){
            
            $start = $start_year->format('Y-m');
            $end = $start_year->addMonths(11)->format('Y-m');

            if($end > $end_year) $end = $end_year->format('Y-m');

            if($start == $end) $text = $start;
            else $text = $start.' - '.$end;
            $years[$i] = 'Year '.$i .' ('.$text.')';
            $i++;
            $start_year->addMonth();

        }
        return $years;
        $duration = ceil(($start_year->diffInMonths($end_year))/12);

        $years = [];
        $start_date = Carbon::parse($this->start_date);
        for($i = 1; $i <= $duration; $i++){
            
            $start = $start_date->format('Y-m');
            if($i == $duration) $end = $end_year->format('Y-m');
            else $end = $start_date->addMonths(11)->format('Y-m');

            if($start == $end) $text = $start;
            else $text = $start.' - '.$end;
            $years[(string)$i] = 'Year '.$i .' ('.$text.')';
            $start_date->addMonth();
        }
        return $years;
    }

    public function getQuarterYearsAttribute(){
        // $start_year = Carbon::parse($this->start_date);
        // $end_year = Carbon::parse($this->end_date);
        // $duration = ceil(($start_year->diffInMonths($end_year))/12);

        $quaters = $this->quarters->groupBy('year');

        $years = [];
        // for($i = 1; $i <= $duration; $i++){
        //     $years['Year '.$i .' ('] = $quaters->where('year',$i);
        // }
        foreach($quaters as $year=>$quarter){
            $start = Carbon::parse($quarter->first()->start_month)->format('Y');
            $end = Carbon::parse($quarter->last()->end_month)->format('Y');
            if($start == $end) $text = $start;
            else $text = $start.' - '.$end;
            $years['Year '.$year .'<br>('.$text.')'] = $quarter;

        }
        return $years;
    }

    public function getActivitiesAttribute(){
        $output_ids = $this->outputs()->pluck('outputs.id')->toArray();

        return Activity::whereIn('output_id',$output_ids)->get();
    }

    public function getRemainingBudgetAttribute(){
        $total_budget = $this->total_budget;
        $expense = 0;

        foreach($this->mneactivities as $item){
            $expense += $item->expense;
        }

        foreach($this->activities as $item){
            $expense += $item->expense;
        }

        return ($total_budget - $expense);
    }
}

