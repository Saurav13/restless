<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Indicator extends Model
{
    public function beneficiaries()
    {
        return $this->hasMany('App\IndicatorBeneficiary');
    }
}
