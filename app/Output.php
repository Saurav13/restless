<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Output extends Model
{
    public function outcome()
    {
        return $this->belongsTo('App\Outcome');
    }

    public function indicators()
    {
        return $this->morphMany('App\Indicator', 'indicatorable');
    }

    public function indicator_beneficiaries()
    {
        return $this->hasManyThrough('App\IndicatorBeneficiary', 'App\Indicator','indicatorable_id')->where('indicatorable_type','App\Output');
    }

    public function progress_logs()
    {
        return $this->morphMany('App\ProgressLog', 'loggable');
    }
}
