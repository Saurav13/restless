<?php

namespace App\Http\Controllers\Admin;

use App\Organization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrganizationProfileController extends Controller
{
    public $base_view;

    public function __construct()
    {
        $this->base_view = "admin.organization";
    }


    public function profile()
    {
        $profile_info = Organization::first();
        return view($this->base_view.'.profile',compact('profile_info'));
    }

    public function UpdateProfile(Request $request)
    {
        $this->validate($request,[
            'fullname' => 'required|max:255',
            'shortname' => 'required|max:255',
            'logo' => 'nullable|mimes:jpg,jpeg,png,bmp,tiff,svg|max:4096',
        ]);
        $profile_info = Organization::first();
        $profile_info->fullname = $request->get('fullname');
        $profile_info->shortname = $request->get('shortname');
        $profile_info->description = $request->get('description');
        $profile_info->office_address = $request->get('office_address');
        $profile_info->contact_number = $request->get('contact_number');
        $profile_info->contact_person = $request->get('contact_person');
        $profile_info->reg_number = $request->get('reg_number');
        $profile_info->pan_number = $request->get('pan_number');
        $profile_info->email = $request->get('email');
        $profile_info->website = $request->get('website');

        if($request->hasFile('logo')){
            if($profile_info->logo != null){
                unlink(public_path('images/organization_images'.'/'.$profile_info->logo));
            }
            $photo = $request->file('logo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('images/organization_images');
            $photo->move($location,$filename);
            $profile_info->logo = $filename;
        }
        $profile_info->save();

        $request->session()->flash('success', 'Organization Profile was updated');
        return redirect()->back();
    }
}
