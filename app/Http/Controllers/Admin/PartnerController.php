<?php

namespace App\Http\Controllers\Admin;

use App\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class PartnerController extends Controller
{
    public $panel;
    public $base_view;
    public $base_route;
    public $model;

    public function __construct()
    {
        $this->panel = 'Partners';
        $this->base_view = 'admin.partners';
        $this->base_route = 'partners';
        $this->model = new Partner();
    }


    public function index()
    {
        $partners = Partner::paginate(15);
        return view($this->base_view.'.index')->with('panel',$this->panel)->with('base_route',$this->base_route)->with('partners',$partners);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->base_view.'.create')->with('base_view',$this->base_view)->with('panel',$this->panel)->with('base_route',$this->base_route);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'fullname' => 'required|max:255',
            'shortname' => 'required|max:255',
            'logo' => 'nullable|mimes:jpg,jpeg,png,bmp,tiff,svg|max:4096',
        ]);
        $reg_number = $request->get('reg_number');
        $reg_details = $request->get('reg_detail');
        $registration_details = [];
        if(count($reg_number) == count($reg_details))
        {
            for ($i = 0; $i < count($reg_details); $i++) {
                $elements = [
                    'registration_number' => $reg_number[$i],
                    'registration_details' => $reg_details[$i]
                ];
                array_push($registration_details,$elements);
              }
        }
        else{
            Session::flash('error','Invalid request');
            return redirect()->back();
        }
        $partner = new Partner();
        $partner->fullname = $request->get('fullname');
        $partner->shortname = $request->get('shortname');
        $partner->description = $request->get('description');
        $partner->office_address = $request->get('office_address');
        $partner->contact_person = $request->get('contact_person');
        $partner->contact_email = $request->get('contact_email');
        $partner->contact_number = $request->get('contact_number');
        $partner->reg_details = json_encode($registration_details);
        $partner->pan_number = $request->get('pan_number');
        $partner->organization_email = $request->get('organization_email');
        $partner->website = $request->get('website');
        if($request->hasFile('logo')){
            $photo = $request->file('logo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('images/partners');
            $photo->move($location,$filename);
            $partner->logo = $filename;
        }
        $partner->save();
        Session::flash('success', $this->panel.' "'.$partner->fullname.'" was created successfully!');
        return redirect()->route($this->base_route.'.show',$partner->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->model::find($id);
        $registration_details = isset($data->reg_details) ? json_decode($data->reg_details) : null;
        
        $view_data = [
            'Full Name' => $data->fullname,
            'Short Name' => $data->shortname,
            'description' => $data->description ? $data->description : 'N/A',
            'Logo' => $data->logo ? '<img class="img" height="100" width="120" src="'.asset('images/partners'.'/'.$data->logo).'">' : 'N/A',
            'office_address' => $data->office_address ? $data->office_address : 'N/A',
            'contact_person' => $data->contact_person ? $data->contact_person : 'N/A',
            'contact_email' => $data->contact_email ? $data->contact_email : 'N/A',
            'contact_number' => $data->contact_number ? $data->contact_number : 'N/A',
            'Registration Details' => view($this->base_view.'.includes.registration_details',compact('registration_details'))->render(),
            'Pan Number' => $data->pan_number ? $data->pan_number : 'N/A',
            'organization_email' => $data->organization_email ? $data->organization_email : 'N/A',
            'Website Link' => $data->website ? $data->website : 'N/A',
            'created at' => date('l M j, Y h:i A', strtotime($data->created_at)).' <b><i style="font-size: 12px; color: #ed1c24;">('. $data->created_at->diffForHumans().')</i></b>',
            'updated_at' => date('l M j, Y h:i A', strtotime($data->updated_at)).' <b><i style="font-size: 12px; color: #ed1c24;">('. $data->updated_at->diffForHumans().')</i></b>',


        ];
        return view($this->base_view.'.show')->with('base_view',$this->base_view)->with('panel',$this->panel)->with('base_route',$this->base_route)->with('view_data',$view_data)->with('data',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $partners = Partner::find($id);
        return view($this->base_view.'.edit')->with('base_view',$this->base_view)->with('panel',$this->panel)->with('base_route',$this->base_route)->with('partners',$partners);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'fullname' => 'required|max:255',
            'shortname' => 'required|max:255',
            'logo' => 'nullable|mimes:jpg,jpeg,png,bmp,tiff,svg|max:4096',
        ]);
        $reg_number = $request->get('reg_number');
        $reg_details = $request->get('reg_detail');
        $registration_details = [];
        if(count($reg_number) == count($reg_details))
        {
            for ($i = 0; $i < count($reg_details); $i++) {
                $elements = [
                    'registration_number' => $reg_number[$i],
                    'registration_details' => $reg_details[$i]
                ];
                array_push($registration_details,$elements);
              }
        }
        else{
            Session::flash('error','Invalid request');
            return redirect()->back();
        }
        $partner = Partner::find($id);
        $partner->fullname = $request->get('fullname');
        $partner->shortname = $request->get('shortname');
        $partner->description = $request->get('description');
        $partner->office_address = $request->get('office_address');
        $partner->contact_person = $request->get('contact_person');
        $partner->contact_email = $request->get('contact_email');
        $partner->contact_number = $request->get('contact_number');
        $partner->reg_details = json_encode($registration_details);
        $partner->pan_number = $request->get('pan_number');
        $partner->organization_email = $request->get('organization_email');
        $partner->website = $request->get('website');
        if($request->hasFile('logo')){
            if($partner->logo != null){
                unlink(public_path('images/partners'.'/'.$partner->logo));
            }
            $photo = $request->file('logo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('images/partners');
            $photo->move($location,$filename);
            $partner->logo = $filename;
        }
        $partner->save();
        Session::flash('success', $this->panel.' "'.$partner->fullname.'" was updated successfully!');
        return redirect()->route($this->base_route.'.show',$partner->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if($request->ajax())
        {
            $data = $this->model::find($id);
            if($data->logo != null){
                unlink(public_path('images/partners'.'/'.$data->logo));
            }
            $data->delete();
        }else{
            $data = $this->model::find($id);
            if($data->logo != null){
                unlink(public_path('images/partners'.'/'.$data->logo));
            }
            $data->delete();
            Session::flash('success',$this->panel.' "'.$data->fullname.'" deleted successfully!');
            return redirect()->route($this->base_route.'.index');
        }

    }
}
