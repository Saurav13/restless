<?php

namespace App\Http\Controllers\Admin;

use App\Project;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\User;

class ProjectController extends Controller
{

    public $panel;
    public $base_view;
    public $base_route;
    public $model;

    public function __construct()
    {
        $this->panel = 'Project';
        $this->base_view = 'admin.projects';
        $this->base_route = 'projects';
        $this->model = new Project();
    }

    public function index()
    {
        $projects = Project::paginate(15);
        return view($this->base_view.'.index')->with('panel',$this->panel)->with('base_route',$this->base_route)->with('projects',$projects);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->base_view.'.create')->with('base_view',$this->base_view)->with('panel',$this->panel)->with('base_route',$this->base_route);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|max:255',
            'goal_term' => 'required|max:255',
            'status' => 'required|in:drafted,ongoing,completed',
            'start_date' => 'date_format:Y-m-d',
            'end_date' => 'date_format:Y-m-d|after:start_date',
        ]);

        $project = new Project();
        $project->title = $request->get('title');
        $project->description = $request->get('description');
        $project->goal_description = $request->get('goal_description');
        $project->goal_term = $request->get('goal_term');
        $project->initial_situation = $request->get('initial_situation');
        $project->expected_situation = $request->get('expected_situation');
        $project->status = $request->get('status');
        $project->start_date = $request->get('start_date');
        $project->end_date = $request->get('end_date');
        $project->total_budget = $request->get('total_budget');
        $project->created_by = auth()->user()->id;
        $project->updated_by = auth()->user()->id;
        $project->save();
        Session::flash('success','Project "'.$project->title.'" was created successfully!');
        return redirect()->route($this->base_route.'.show',$project->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->model::find($id);
        $view_data = [
            'title' => $data->title,
            'code' => $data->code ? '<span class="badge badge-pill badge-info">'.$data->code.'</span>' : 'N/A',
            'description' => $data->description,
            'goal description' => $data->goal_description,
            'goal term' => $data->goal_term,
            'initial situation' => $data->initial_situation,
            'expected situation' => $data->expected_situation,
            'status' => '<span class="badge badge-pill badge-warning">'.$data->status.'</span>',
            'start date' => date_format(Carbon::parse($data->start_date), "M d, Y"),
            'end_date' => date_format(Carbon::parse($data->end_date), "M d, Y") ,
            'total budget' => $data->total_budget,
            'created by' => User::find($data->created_by)->name,
            'updated by' => $data->updated_by ? User::find($data->updated_by)->name : 'N/A',
            'created at' => date('l M j, Y h:i A', strtotime($data->created_at)).' <b><i style="font-size: 12px; color: #ed1c24;">('. $data->created_at->diffForHumans().')</i></b>',
            'updated_at' => date('l M j, Y h:i A', strtotime($data->updated_at)).' <b><i style="font-size: 12px; color: #ed1c24;">('. $data->updated_at->diffForHumans().')</i></b>',
        ];
        return view($this->base_view.'.show')->with('base_view',$this->base_view)->with('panel',$this->panel)->with('base_route',$this->base_route)->with('view_data',$view_data)->with('data',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $projects = Project::find($id);
        return view($this->base_view.'.edit')->with('base_view',$this->base_view)->with('panel',$this->panel)->with('base_route',$this->base_route)->with('projects',$projects);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required|max:255',
            'goal_term' => 'required|max:255',
            'status' => 'required|in:drafted,ongoing,completed',
            'start_date' => 'date_format:Y-m-d',
            'end_date' => 'date_format:Y-m-d|after:start_date',
        ]);

        $project = Project::find($id);
        $project->title = $request->get('title');
        $project->description = $request->get('description');
        $project->goal_description = $request->get('goal_description');
        $project->goal_term = $request->get('goal_term');
        $project->initial_situation = $request->get('initial_situation');
        $project->expected_situation = $request->get('expected_situation');
        $project->status = $request->get('status');
        $project->start_date = $request->get('start_date');
        $project->end_date = $request->get('end_date');
        $project->total_budget = $request->get('total_budget');
        $project->updated_by = auth()->user()->id;
        $project->save();
        Session::flash('success','Project titled "'.$project->title.'" updated successfully!');
        return redirect()->route($this->base_route.'.show',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if($request->ajax())
        {
            $data = $this->model::find($id);
            $data->delete();
        }else{
            $data = $this->model::find($id);
            $data->delete();
            Session::flash('success','Project titled "'.$data->title.'" deleted successfully!');
            return redirect()->route($this->base_route.'.index');
        }

    }
}
