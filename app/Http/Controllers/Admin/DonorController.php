<?php

namespace App\Http\Controllers\Admin;

use App\Donor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class DonorController extends Controller
{
    public $panel;
    public $base_view;
    public $base_route;
    public $model;
    public $country_list;

    public function __construct()
    {
        $this->panel = 'Donor';
        $this->base_view = 'admin.donors';
        $this->base_route = 'donors';
        $this->model = new Donor();
        $this->country_list = ["Afghanistan" => "Afghanistan",
  "Albania" => "Albania",
  "Algeria" => "Algeria",
  "American Samoa" => "American Samoa",
  "Andorra" => "Andorra",
  "Angola" => "Angola",
  "Anguilla" => "Anguilla",
  "Antarctica" => "Antarctica",
  "Antigua and Barbuda" => "Antigua and Barbuda",
  "Argentina" => "Argentina",
  "Armenia" => "Armenia",
  "Aruba" => "Aruba",
  "Australia" => "Australia",
  "Austria" => "Austria",
  "Azerbaijan" => "Azerbaijan",
  "Bahamas" => "Bahamas",
  "Bahrain" => "Bahrain",
  "Bangladesh" => "Bangladesh",
  "Barbados" => "Barbados",
  "Belarus" => "Belarus",
  "Belgium" => "Belgium",
  "Belize" => "Belize",
  "Benin" => "Benin",
  "Bermuda" => "Bermuda",
  "Bhutan" => "Bhutan",
  "Bolivia" => "Bolivia",
  "Bosnia and Herzegowina" => "Bosnia and Herzegowina",
  "Botswana" => "Botswana",
  "Bouvet Island" => "Bouvet Island",
  "Brazil" => "Brazil",
  "British Indian Ocean Territory" => "British Indian Ocean Territory",
  "Brunei Darussalam" => "Brunei Darussalam",
  "Bulgaria" => "Bulgaria",
  "Burkina Faso" => "Burkina Faso",
  "Burundi" => "Burundi",
  "Cambodia" => "Cambodia",
  "Cameroon" => "Cameroon",
  "Canada" => "Canada",
  "Cape Verde" => "Cape Verde",
  "Cayman Islands" => "Cayman Islands",
  "Central African Republic" => "Central African Republic",
  "Chad" => "Chad",
  "Chile" => "Chile",
  "China" => "China",
  "Christmas Island" => "Christmas Island",
  "Cocos (Keeling) Islands" => "Cocos (Keeling) Islands",
  "Colombia" => "Colombia",
  "Comoros" => "Comoros",
  "Congo" => "Congo",
  "Congo, the Democratic Republic of the" => "Congo, the Democratic Republic of the",
  "Cook Islands" => "Cook Islands",
  "Costa Rica" => "Costa Rica",
  "Cote d'Ivoire" => "Cote d'Ivoire",
  "Croatia (Hrvatska)" => "Croatia (Hrvatska)",
  "Cuba" => "Cuba",
  "Cyprus" => "Cyprus",
  "Czech Republic" => "Czech Republic",
  "Denmark" => "Denmark",
  "Djibouti" => "Djibouti",
  "Dominica" => "Dominica",
  "Dominican Republic" => "Dominican Republic",
  "East Timor" => "East Timor",
  "Ecuador" => "Ecuador",
  "Egypt" => "Egypt",
  "El Salvador" => "El Salvador",
  "Equatorial Guinea" => "Equatorial Guinea",
  "Eritrea" => "Eritrea",
  "Estonia" => "Estonia",
  "Ethiopia" => "Ethiopia",
  "Falkland Islands (Malvinas)" => "Falkland Islands (Malvinas)",
  "Faroe Islands" => "Faroe Islands",
  "Fiji" => "Fiji",
  "Finland" => "Finland",
  "France" => "France",
  "France Metropolitan" => "France Metropolitan",
  "French Guiana" => "French Guiana",
  "French Polynesia" => "French Polynesia",
  "French Southern Territories" => "French Southern Territories",
  "Gabon" => "Gabon",
  "Gambia" => "Gambia",
  "Georgia" => "Georgia",
  "Germany" => "Germany",
  "Ghana" => "Ghana",
  "Gibraltar" => "Gibraltar",
  "Greece" => "Greece",
  "Greenland" => "Greenland",
  "Grenada" => "Grenada",
  "Guadeloupe" => "Guadeloupe",
  "Guam" => "Guam",
  "Guatemala" => "Guatemala",
  "Guinea" => "Guinea",
  "Guinea-Bissau" => "Guinea-Bissau",
  "Guyana" => "Guyana",
  "Haiti" => "Haiti",
  "Heard and Mc Donald Islands" => "Heard and Mc Donald Islands",
  "Holy See (Vatican City State)" => "Holy See (Vatican City State)",
  "Honduras" => "Honduras",
  "Hong Kong" => "Hong Kong",
  "Hungary" => "Hungary",
  "Iceland" => "Iceland",
  "India" => "India",
  "Indonesia" => "Indonesia",
  "Iran (Islamic Republic of)" => "Iran (Islamic Republic of)",
  "Iraq" => "Iraq",
  "Ireland" => "Ireland",
  "Israel" => "Israel",
  "Italy" => "Italy",
  "Jamaica" => "Jamaica",
  "Japan" => "Japan",
  "Jordan" => "Jordan",
  "Kazakhstan" => "Kazakhstan",
  "Kenya" => "Kenya",
  "Kiribati" => "Kiribati",
  "Korea, Democratic People's Republic of" => "Korea, Democratic People's Republic of",
  "Korea, Republic of" => "Korea, Republic of",
  "Kuwait" => "Kuwait",
  "Kyrgyzstan" => "Kyrgyzstan",
  "Lao, People's Democratic Republic" => "Lao, People's Democratic Republic",
  "Latvia" => "Latvia",
  "Lebanon" => "Lebanon",
  "Lesotho" => "Lesotho",
  "Liberia" => "Liberia",
  "Libyan Arab Jamahiriya" => "Libyan Arab Jamahiriya",
  "Liechtenstein" => "Liechtenstein",
  "Lithuania" => "Lithuania",
  "Luxembourg" => "Luxembourg",
  "Macau" => "Macau",
  "Macedonia, The Former Yugoslav Republic of" => "Macedonia, The Former Yugoslav Republic of",
  "Madagascar" => "Madagascar",
  "Malawi" => "Malawi",
  "Malaysia" => "Malaysia",
  "Maldives" => "Maldives",
  "Mali" => "Mali",
  "Malta" => "Malta",
  "Marshall Islands" => "Marshall Islands",
  "Martinique" => "Martinique",
  "Mauritania" => "Mauritania",
  "Mauritius" => "Mauritius",
  "Mayotte" => "Mayotte",
  "Mexico" => "Mexico",
  "Micronesia, Federated States of" => "Micronesia, Federated States of",
  "Moldova, Republic of" => "Moldova, Republic of",
  "Monaco" => "Monaco",
  "Mongolia" => "Mongolia",
  "Montserrat" => "Montserrat",
  "Morocco" => "Morocco",
  "Mozambique" => "Mozambique",
  "Myanmar" => "Myanmar",
  "Namibia" => "Namibia",
  "Nauru" => "Nauru",
  "Nepal" => "Nepal",
  "Netherlands" => "Netherlands",
  "Netherlands Antilles" => "Netherlands Antilles",
  "New Caledonia" => "New Caledonia",
  "New Zealand" => "New Zealand",
  "Nicaragua" => "Nicaragua",
  "Niger" => "Niger",
  "Nigeria" => "Nigeria",
  "Niue" => "Niue",
  "Norfolk Island" => "Norfolk Island",
  "Northern Mariana Islands" => "Northern Mariana Islands",
  "Norway" => "Norway",
  "Oman" => "Oman",
  "Pakistan" => "Pakistan",
  "Palau" => "Palau",
  "Panama" => "Panama",
  "Papua New Guinea" => "Papua New Guinea",
  "Paraguay" => "Paraguay",
  "Peru" => "Peru",
  "Philippines" => "Philippines",
  "Pitcairn" => "Pitcairn",
  "Poland" => "Poland",
  "Portugal" => "Portugal",
  "Puerto Rico" => "Puerto Rico",
  "Qatar" => "Qatar",
  "Reunion" => "Reunion",
  "Romania" => "Romania",
  "Russian Federation" => "Russian Federation",
  "Rwanda" => "Rwanda",
  "Saint Kitts and Nevis" => "Saint Kitts and Nevis",
  "Saint Lucia" => "Saint Lucia",
  "Saint Vincent and the Grenadines" => "Saint Vincent and the Grenadines",
  "Samoa" => "Samoa",
  "San Marino" => "San Marino",
  "Sao Tome and Principe" => "Sao Tome and Principe",
  "Saudi Arabia" => "Saudi Arabia",
  "Senegal" => "Senegal",
  "Seychelles" => "Seychelles",
  "Sierra Leone" => "Sierra Leone",
  "Singapore" => "Singapore",
  "Slovakia (Slovak Republic)" => "Slovakia (Slovak Republic)",
  "Slovenia" => "Slovenia",
  "Solomon Islands" => "Solomon Islands",
  "Somalia" => "Somalia",
  "South Africa" => "South Africa",
  "South Georgia and the South Sandwich Islands" => "South Georgia and the South Sandwich Islands",
  "Spain" => "Spain",
  "Sri Lanka" => "Sri Lanka",
  "St. Helena" => "St. Helena",
  "St. Pierre and Miquelon" => "St. Pierre and Miquelon",
  "Sudan" => "Sudan",
  "Suriname" => "Suriname",
  "Svalbard and Jan Mayen Islands" => "Svalbard and Jan Mayen Islands",
  "Swaziland" => "Swaziland",
  "Sweden" => "Sweden",
  "Switzerland" => "Switzerland",
  "Syrian Arab Republic" => "Syrian Arab Republic",
  "Taiwan, Province of China" => "Taiwan, Province of China",
  "Tajikistan" => "Tajikistan",
  "Tanzania, United Republic of" => "Tanzania, United Republic of",
  "Thailand" => "Thailand",
  "Togo" => "Togo",
  "Tokelau" => "Tokelau",
  "Tonga" => "Tonga",
  "Trinidad and Tobago" => "Trinidad and Tobago",
  "Tunisia" => "Tunisia",
  "Turkey" => "Turkey",
  "Turkmenistan" => "Turkmenistan",
  "Turks and Caicos Islands" => "Turks and Caicos Islands",
  "Tuvalu" => "Tuvalu",
  "Uganda" => "Uganda",
  "Ukraine" => "Ukraine",
  "United Arab Emirates" => "United Arab Emirates",
  "United Kingdom" => "United Kingdom",
  "United States" => "United States",
  "United States Minor Outlying Islands" => "United States Minor Outlying Islands",
  "Uruguay" => "Uruguay",
  "Uzbekistan" => "Uzbekistan",
  "Vanuatu" => "Vanuatu",
  "Venezuela" => "Venezuela",
  "Vietnam" => "Vietnam",
  "Virgin Islands (British)" => "Virgin Islands (British)",
  "Virgin Islands (U.S.)" => "Virgin Islands (U.S.)",
  "Wallis and Futuna Islands" => "Wallis and Futuna Islands",
  "Western Sahara" => "Western Sahara",
  "Yemen" => "Yemen",
  "Yugoslavia" => "Yugoslavia",
  "Zambia" => "Zambia",
  "Zimbabwe" => "Zimbabwe",
];

    }

    public function index()
    {
        $donors = Donor::paginate(15);
        return view($this->base_view.'.index')->with('panel',$this->panel)->with('base_route',$this->base_route)->with('donors',$donors);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $country_list = $this->country_list;
        return view($this->base_view.'.create')->with('base_view',$this->base_view)->with('panel',$this->panel)->with('base_route',$this->base_route)->with('country_list',$country_list);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'fullname' => 'required|max:255',
            'shortname' => 'required|max:255',
            'logo' => 'nullable|mimes:jpg,jpeg,png,bmp,tiff,svg|max:4096',
        ]);

        $donors = new Donor();
        $donors->fullname = $request->get('fullname');
        $donors->shortname = $request->get('shortname');
        $donors->country = $request->get('country');
        $donors->description = $request->get('description');
        $donors->office_address = $request->get('office_address');
        $donors->mou_signatory = $request->get('mou_signatory');
        $donors->contact_person = $request->get('contact_person');
        $donors->contact_number = $request->get('contact_number');
        $donors->reg_number = $request->get('reg_number');
        $donors->pan_number = $request->get('pan_number');
        $donors->email = $request->get('email');
        $donors->website = $request->get('website');
        if($request->hasFile('logo')){
            $photo = $request->file('logo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('images/donors');
            $photo->move($location,$filename);
            $donors->logo = $filename;
        }
        $donors->save();
        Session::flash('success', $this->panel.' "'.$donors->fullname.'" was created successfully!');
        return redirect()->route($this->base_route.'.show',$donors->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->model::find($id);
        $view_data = [
            'Full Name' => $data->fullname,
            'Short Name' => $data->shortname,
            'description' => $data->description ? $data->description : 'N/A',
            'Country' => $data->country ? $data->country : 'N/A',
            'Logo' => $data->logo ? '<img class="img" height="100" width="120" src="'.asset('images/donors'.'/'.$data->logo).'">' : 'N/A',
            'MOU Signature' => $data->shortname ? $data->shortname : 'N/A',
            'office_address' => $data->office_address ? $data->office_address : 'N/A',
            'contact_person' => $data->contact_person ? $data->contact_person : 'N/A',
            'contact_number' => $data->contact_number ? $data->contact_number : 'N/A',
            'Registration Number' => $data->reg_number ? $data->reg_number : 'N/A',
            'Pan Number' => $data->pan_number ? $data->pan_number : 'N/A',
            'Email' => $data->email ? $data->email : 'N/A',
            'Website Link' => $data->website ? $data->website : 'N/A',
            'created at' => date('l M j, Y h:i A', strtotime($data->created_at)).' <b><i style="font-size: 12px; color: #ed1c24;">('. $data->created_at->diffForHumans().')</i></b>',
            'updated_at' => date('l M j, Y h:i A', strtotime($data->updated_at)).' <b><i style="font-size: 12px; color: #ed1c24;">('. $data->updated_at->diffForHumans().')</i></b>',


        ];
        return view($this->base_view.'.show')->with('base_view',$this->base_view)->with('panel',$this->panel)->with('base_route',$this->base_route)->with('view_data',$view_data)->with('data',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $donors = Donor::find($id);
        $country_list = $this->country_list;
        return view($this->base_view.'.edit')->with('base_view',$this->base_view)->with('panel',$this->panel)->with('base_route',$this->base_route)->with('donors',$donors)->with('country_list',$country_list);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'fullname' => 'required|max:255',
            'shortname' => 'required|max:255',
            'logo' => 'nullable|mimes:jpg,jpeg,png,bmp,tiff,svg|max:4096',
        ]);

        $donors = Donor::find($id);
        $donors->fullname = $request->get('fullname');
        $donors->shortname = $request->get('shortname');
        $donors->country = $request->get('country');
        $donors->description = $request->get('description');
        $donors->office_address = $request->get('office_address');
        $donors->mou_signatory = $request->get('mou_signatory');
        $donors->contact_person = $request->get('contact_person');
        $donors->contact_number = $request->get('contact_number');
        $donors->reg_number = $request->get('reg_number');
        $donors->pan_number = $request->get('pan_number');
        $donors->email = $request->get('email');
        $donors->website = $request->get('website');
        if($request->hasFile('logo')){
            if($donors->logo != null){
                unlink(public_path('images/donors'.'/'.$donors->logo));
            }
            $photo = $request->file('logo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();
            $location = public_path('images/donors');
            $photo->move($location,$filename);
            $donors->logo = $filename;
        }
        $donors->save();
        Session::flash('success', $this->panel.' "'.$donors->fullname.'" was updated successfully!');
        return redirect()->route($this->base_route.'.show',$donors->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if($request->ajax())
        {
            $data = $this->model::find($id);
            if($data->logo != null){
                unlink(public_path('images/donors'.'/'.$data->logo));
            }
            $data->delete();
        }else{
            $data = $this->model::find($id);
            if($data->logo != null){
                unlink(public_path('images/donors'.'/'.$data->logo));
            }
            $data->delete();
            Session::flash('success',$this->panel.' "'.$data->fullname.'" deleted successfully!');
            return redirect()->route($this->base_route.'.index');
        }
    }
}
