<?php

namespace App\Http\Controllers\Admin;

use App\BudgetCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class BudgetCategoryController extends Controller
{
    public $base_view;
    public $base_route;
    public $panel;
    public $model;

    public function __construct()
    {
        $this->panel = 'Budget Category';
        $this->base_view = 'admin.budget-categories';
        $this->base_route = 'budget-categories';
        $this->model = new BudgetCategory();
    }

    public function index()
    {
        $budget_categories = BudgetCategory::paginate(15);
        return view($this->base_view.'.index')->with('panel',$this->panel)->with('base_route',$this->base_route)->with('budget_categories',$budget_categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->base_view.'.create')->with('base_view',$this->base_view)->with('panel',$this->panel)->with('base_route',$this->base_route);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|max:255',
        ]);
        $budget_categories = new BudgetCategory();
        $budget_categories->title = $request->get('title');
        $budget_categories->save();
        Session::flash('success', $this->panel.' "'.$budget_categories->title.'" was created successfully!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required|max:255',
        ]);
        $budget_categories = BudgetCategory::find($id);
        $budget_categories->title = $request->get('title');
        $budget_categories->save();
        Session::flash('success', $this->panel.' "'.$budget_categories->title.'" was updated successfully!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if($request->ajax())
        {
            $data = $this->model::find($id);
            $data->delete();
        }else{
            $data = $this->model::find($id);
            $data->delete();
            Session::flash('success','Project titled "'.$data->title.'" deleted successfully!');
            return redirect()->route($this->base_route.'.index');
        }

    }
}
