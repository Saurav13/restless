<?php

namespace App\Http\Controllers\Admin;

use App\Ethnicity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;

class EthnicityController extends Controller
{
    public $panel;
    public $base_view;
    public $base_route;
    public $model;

    public function __construct()
    {
        $this->panel = 'Ethnicity';
        $this->base_view = 'admin.ethnicities';
        $this->base_route = 'ethnicities';
        $this->model = new Ethnicity();
    }

    public function index()
    {
        $ethnicities = Ethnicity::paginate(15);
        return view($this->base_view.'.index')->with('panel',$this->panel)->with('base_route',$this->base_route)->with('ethnicities',$ethnicities);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|max:255',
        ]);
        $ethnicities = new Ethnicity();
        $ethnicities->name = $request->get('name');
        $ethnicities->save();
        Session::flash('success', $this->panel.' "'.$ethnicities->name.'" was created successfully!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|max:255',
        ]);
        $ethnicities = Ethnicity::find($id);
        $ethnicities->name = $request->get('name');
        $ethnicities->save();
        Session::flash('success', $this->panel.' "'.$ethnicities->name.'" was updated successfully!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if($request->ajax())
        {
            $data = $this->model::find($id);
            $data->delete();
        }else{
            $data = $this->model::find($id);
            $data->delete();
            Session::flash('success',$this->panel.' "'.$data->name.'" deleted successfully!');
            return redirect()->route($this->base_route.'.index');
        }

    }
}
