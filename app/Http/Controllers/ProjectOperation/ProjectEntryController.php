<?php

namespace App\Http\Controllers\ProjectOperation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rules\NoEmptyContent;
use App\Project;
use App\ProgressLog;
use Auth;
use Carbon\Carbon;

class ProjectEntryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEvaluation($project_id)
    {
        $project = Project::findOrFail($project_id);
        $indicators = $project->indicators;

        return view('operations.project.indicator_evaluation',compact('project','indicators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function saveEvaluation($project_id, Request $request)
    {
        $project = Project::findOrFail($project_id);
        $indicator_beneficiaries = $project->indicator_beneficiaries;

        $request->validate([
            'beneficiaries' => 'required|array|size:'.$indicator_beneficiaries->count(),
            'beneficiaries.*.id' => 'required|exists:indicator_beneficiary,id',
            'beneficiaries.*.actual_number' => 'required|integer|min:0',
            'beneficiaries.*.actual_qualitative_description' => 'required',
        ],[
            'beneficiaries.*.actual_number.required' => 'This field is required',
            'beneficiaries.*.actual_number.min' => 'Value must be greater than or equal to 0.',
            'beneficiaries.*.actual_number.integer' => 'Value must be an integer',
            'beneficiaries.*.actual_qualitative_description.required' => 'This field is required',
        ]);

        foreach($request->beneficiaries as $b){
            $reach = $indicator_beneficiaries->where('id',$b['id'])->first();
            $reach->actual_number = $b['actual_number'];
            $reach->actual_qualitative_description = $b['actual_qualitative_description'];

            $reach->save();
        }

        $request->session()->flash('success', 'Successfully Saved');

        return redirect()->back();
    }

    public function getReport($project_id, Request $request)
    {
        $project = Project::findOrFail($project_id);

        if(!$request->sort || $request->sort == 'desc')
            $logs = $project->progress_logs()->orderBy('created_at','desc')->paginate(10);
        else
            $logs = $project->progress_logs()->orderBy('created_at','asc')->paginate(10);

        return view('operations.project.progress_report',compact('project','logs'));
    }

    public function saveReport($project_id, Request $request)
    {
        $project = Project::findOrFail($project_id);

        $request->validate([           
            'log' => ['required', new NoEmptyContent],
        ],[
            'log.required' => 'This field is required',
        ]);

        $log = new ProgressLog;
        $log->log = $request->log;
        $log->date = Carbon::now();
        $log->created_by = Auth::user()->id;
        $log->updated_by = Auth::user()->id;

        $project->progress_logs()->save($log);

        $request->session()->flash('success', 'Successfully Created');

        return redirect()->back();
    }
}
