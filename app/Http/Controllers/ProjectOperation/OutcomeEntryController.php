<?php

namespace App\Http\Controllers\ProjectOperation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rules\NoEmptyContent;
use App\Project;
use App\ProgressLog;
use Auth;
use Carbon\Carbon;

class OutcomeEntryController extends Controller
{
    public function indexEvaluation($project_id){
        $project = Project::findOrFail($project_id);
        $outcomes = $project->outcomes;

        return view('operations.outcomes.indicator_evaluation',compact('project','outcomes'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEvaluation($project_id, $outcome_id)
    {
        $project = Project::findOrFail($project_id);
        $outcomes = $project->outcomes;
        $outcome = $outcomes->where('id',$outcome_id)->first();

        if(!$outcome) abort(404);

        $indicators = $outcome->indicators;

        return view('operations.outcomes.indicator_evaluation',compact('project','outcomes','outcome','indicators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function saveEvaluation($project_id, $outcome_id, Request $request)
    {
        $project = Project::findOrFail($project_id);
        $outcomes = $project->outcomes;
        $outcome = $outcomes->where('id',$outcome_id)->first();

        if(!$outcome) abort(404);

        $indicator_beneficiaries = $outcome->indicator_beneficiaries;
        $indicators = $outcome->indicators;
        
        $request->validate([
            'indicators' => 'required|array|size:'.$indicators->count(),
            'indicators.*.id' => 'required|in:'.implode(',', $indicators->pluck('id')->toArray()),
            'indicators.*.actual_number' => 'required|integer|min:0',
            'indicators.*.actual_qualitative_description' => 'required',
            'beneficiaries' => 'required|array|size:'.$indicator_beneficiaries->count(),
            'beneficiaries.*.id' => 'required|in:'.implode(',', $indicator_beneficiaries->pluck('id')->toArray()),
            'beneficiaries.*.actual_number' => 'required|integer|min:0',
            'beneficiaries.*.actual_qualitative_description' => 'required',
        ],[
            'indicators.*.actual_number.required' => 'This field is required',
            'indicators.*.actual_number.min' => 'Value must be greater than or equal to 0.',
            'indicators.*.actual_number.integer' => 'Value must be an integer',
            'indicators.*.actual_qualitative_description.required' => 'This field is required',
    
            'beneficiaries.*.actual_number.required' => 'This field is required',
            'beneficiaries.*.actual_number.min' => 'Value must be greater than or equal to 0.',
            'beneficiaries.*.actual_number.integer' => 'Value must be an integer',
            'beneficiaries.*.actual_qualitative_description.required' => 'This field is required',
        ]);

        foreach($request->indicators as $b){
            $indicator = $indicators->where('id',$b['id'])->first();
            $indicator->actual_number = $b['actual_number'];
            $indicator->actual_qualitative_description = $b['actual_qualitative_description'];

            $indicator->save();
        }

        foreach($request->beneficiaries as $b){
            $reach = $indicator_beneficiaries->where('id',$b['id'])->first();
            $reach->actual_number = $b['actual_number'];
            $reach->actual_qualitative_description = $b['actual_qualitative_description'];

            $reach->save();
        }

        $request->session()->flash('success', 'Successfully Saved');

        return redirect()->back();
    }

    public function indexReport($project_id){
        $project = Project::findOrFail($project_id);
        $outcomes = $project->outcomes;

        return view('operations.outcomes.progress_report',compact('project','outcomes'));
    }

    public function getReport($project_id, $outcome_id, Request $request)
    {
        $project = Project::findOrFail($project_id);
        $outcomes = $project->outcomes;
        $outcome = $outcomes->where('id',$outcome_id)->first();

        if(!$outcome) abort(404);

        if(!$request->sort || $request->sort == 'desc')
            $logs = $outcome->progress_logs()->orderBy('created_at','desc')->paginate(10);
        else
            $logs = $outcome->progress_logs()->orderBy('created_at','asc')->paginate(10);

        return view('operations.outcomes.progress_report',compact('project','outcomes','outcome','logs'));
    }

    public function saveReport($project_id, $outcome_id, Request $request)
    {
        $project = Project::findOrFail($project_id);
        $outcomes = $project->outcomes;
        $outcome = $outcomes->where('id',$outcome_id)->first();

        if(!$outcome) abort(404);

        $request->validate([           
            'log' => ['required', new NoEmptyContent],
        ],[
            'log.required' => 'This field is required',
        ]);

        $log = new ProgressLog;
        $log->log = $request->log;
        $log->date = Carbon::now();
        $log->created_by = Auth::user()->id;
        $log->updated_by = Auth::user()->id;

        $outcome->progress_logs()->save($log);

        $request->session()->flash('success', 'Successfully Created');

        return redirect()->back();
    }
}
