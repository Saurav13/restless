<?php

namespace App\Http\Controllers\ProjectOperation;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rules\NoEmptyContent;
use App\Project;
use App\ProgressLog;
use Auth;
use Carbon\Carbon;

class OutputEntryController extends Controller
{
    public function indexEvaluation($project_id){
        $project = Project::findOrFail($project_id);
        $outcomes = $project->outcomes;
        $outputs = $project->outputs;

        return view('operations.outputs.indicator_evaluation',compact('project','outputs','outcomes'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getEvaluation($project_id, $output_id)
    {
        $project = Project::findOrFail($project_id);
        $outcomes = $project->outcomes;
        $outputs = $project->outputs;
        $output = $outputs->where('id',$output_id)->first();

        if(!$output) abort(404);

        $indicators = $output->indicators;

        return view('operations.outputs.indicator_evaluation',compact('project','outcomes','outputs','output','indicators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function saveEvaluation($project_id, $output_id, Request $request)
    {
        $project = Project::findOrFail($project_id);
        $outputs = $project->outputs;
        $output = $outputs->where('id',$output_id)->first();

        if(!$output) abort(404);

        $indicator_beneficiaries = $output->indicator_beneficiaries;
        $indicators = $output->indicators;
        
        $request->validate([
            'indicators' => 'required|array|size:'.$indicators->count(),
            'indicators.*.id' => 'required|in:'.implode(',', $indicators->pluck('id')->toArray()),
            'indicators.*.actual_number' => 'required|integer|min:0',
            'indicators.*.actual_qualitative_description' => 'required',
            'beneficiaries' => 'required|array|size:'.$indicator_beneficiaries->count(),
            'beneficiaries.*.id' => 'required|in:'.implode(',', $indicator_beneficiaries->pluck('id')->toArray()),
            'beneficiaries.*.actual_number' => 'required|integer|min:0',
            'beneficiaries.*.actual_qualitative_description' => 'required',
        ],[
            'indicators.*.actual_number.required' => 'This field is required',
            'indicators.*.actual_number.min' => 'Value must be greater than or equal to 0.',
            'indicators.*.actual_number.integer' => 'Value must be an integer',
            'indicators.*.actual_qualitative_description.required' => 'This field is required',
    
            'beneficiaries.*.actual_number.required' => 'This field is required',
            'beneficiaries.*.actual_number.min' => 'Value must be greater than or equal to 0.',
            'beneficiaries.*.actual_number.integer' => 'Value must be an integer',
            'beneficiaries.*.actual_qualitative_description.required' => 'This field is required',
        ]);

        foreach($request->indicators as $b){
            $indicator = $indicators->where('id',$b['id'])->first();
            $indicator->actual_number = $b['actual_number'];
            $indicator->actual_qualitative_description = $b['actual_qualitative_description'];

            $indicator->save();
        }

        foreach($request->beneficiaries as $b){
            $reach = $indicator_beneficiaries->where('id',$b['id'])->first();
            $reach->actual_number = $b['actual_number'];
            $reach->actual_qualitative_description = $b['actual_qualitative_description'];

            $reach->save();
        }

        $request->session()->flash('success', 'Successfully Saved');

        return redirect()->back();
    }

    public function indexReport($project_id){
        $project = Project::findOrFail($project_id);
        $outcomes = $project->outcomes;
        $outputs = $project->outputs;

        return view('operations.outputs.progress_report',compact('project','outcomes','outputs'));
    }

    public function getReport($project_id, $output_id, Request $request)
    {
        $project = Project::findOrFail($project_id);
        $outcomes = $project->outcomes;
        $outputs = $project->outputs;
        $output = $outputs->where('id',$output_id)->first();

        if(!$output) abort(404);

        if(!$request->sort || $request->sort == 'desc')
            $logs = $output->progress_logs()->orderBy('created_at','desc')->paginate(10);
        else
            $logs = $output->progress_logs()->orderBy('created_at','asc')->paginate(10);

        return view('operations.outputs.progress_report',compact('project','outcomes','outputs','output','logs'));
    }

    public function saveReport($project_id, $output_id, Request $request)
    {
        $project = Project::findOrFail($project_id);
        $outputs = $project->outputs;
        $output = $outputs->where('id',$output_id)->first();

        if(!$output) abort(404);

        $request->validate([           
            'log' => ['required', new NoEmptyContent],
        ],[
            'log.required' => 'This field is required',
        ]);

        $log = new ProgressLog;
        $log->log = $request->log;
        $log->date = Carbon::now();
        $log->created_by = Auth::user()->id;
        $log->updated_by = Auth::user()->id;

        $output->progress_logs()->save($log);

        $request->session()->flash('success', 'Successfully Created');

        return redirect()->back();
    }
}
