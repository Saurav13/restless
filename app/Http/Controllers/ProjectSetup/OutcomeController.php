<?php

namespace App\Http\Controllers\ProjectSetup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Outcome;
use Auth;

class OutcomeController extends Controller
{
    public function index($id)
    {
        $outcomes=Outcome::where('project_id',$id)->paginate(10);
        return view('project.outcomes.index',compact('outcomes'));
    }
    public function create($id)
    {
        return view('project.outcomes.create');

    }

    public function store($id, Request $request)
    {
        $request->validate(array(
            'title'=>'required|max:191',
            'description'=> 'required'
        ),array(
            'title.required' => 'Title of Outcome is required',
            'description.required' => 'Details of Outcome is required'
        ));

        $outcome= new Outcome;
        $outcome->title=$request->title;
        $outcome->description=$request->description;
        $outcome->initial_situation=$request->initial_situation;
        $outcome->expected_situation=$request->expected_situation;
        $outcome->approach=$request->approach;
        $outcome->project_id=$id;
        $outcome->created_by=Auth::user()->id;
        $outcome->updated_by=Auth::user()->id;

        $outcome->save();

        $request->session()->flash('success', 'New Outcome created');
        return redirect()->to('/project'.'/'.$id.'/outcomes');

    }
    public function edit($id, $outcome_id)
    {
        $outcome=Outcome::findOrFail($outcome_id);
        return view('project.outcomes.edit',compact('outcome'));
    }

    public function update($id,$outcome_id,Request $request )
    {
        $request->validate(array(
            'title'=>'required|max:191',
            'description'=> 'required'
        ),array(
            'title.required' => 'Title of Outcome is required',
            'description.required' => 'Details of Outcome is required'
        ));

        $outcome= Outcome::findOrFail($outcome_id);
        $outcome->title=$request->title;
        $outcome->description=$request->description;
        $outcome->initial_situation=$request->initial_situation;
        $outcome->expected_situation=$request->expected_situation;
        $outcome->approach=$request->approach;
        $outcome->updated_by=Auth::user()->id;

        $outcome->save();   

        $request->session()->flash('success', 'Outcome edited successfully');
        return redirect()->to('/project'.'/'.$id.'/outcomes');
    }

    public function destroy($id,$outcome_id,Request $request)
    {
        $outcome=Outcome::findOrFail($outcome_id);
        $outcome->delete();
        $request->session()->flash('success', 'Outcome deleted successfully');

        return redirect()->to('/project'.'/'.$id.'/outcomes');

    }
}
