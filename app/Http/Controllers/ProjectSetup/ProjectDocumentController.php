<?php

namespace App\Http\Controllers\ProjectSetup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;
use App\ProjectDoc;
use Auth;

class ProjectDocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($project_id)
    {
        $project = Project::findOrFail($project_id);
        $project_docs = $project->project_docs()->orderBy('id','desc')->paginate(10);

        return view('project.documents.index',compact('project_docs'));
    }

    public function create($project_id)
    {
        return view('project.documents.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store($project_id, Request $request)
    {
        $project = Project::findOrFail($project_id);
        $request->validate(array(
            'title'=>'required|max:191',
            'file'=> 'required|file'
        ),array(
            'title.required' => 'Title of Project Doc is required',
            'file.required' => 'File is required',
            'file.file' => 'File is invalid',
        ));
        
        $project_doc= new ProjectDoc;
        $project_doc->title = $request->title;
        
        if($request->hasFile('file')){
            $file = $request->file('file');
            $filenameWithExt = $file->getClientOriginalName();
            $name = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            $filename = str_replace(' ','',$name).'_'.time(). '.' . $file->getClientOriginalExtension();
            $path = $file->storeAs('public/project_docs', $filename);
            $project_doc->file = $filename;
        }
        
        $project_doc->project_id = $project_id;
        $project_doc->created_by=Auth::user()->id;
        $project_doc->updated_by=Auth::user()->id;

        $project_doc->save();

        $request->session()->flash('success', 'New Project Doc created');
        return redirect()->route('docs.index',$project_id);
    }

    public function show($project_id, $project_doc_id){
        $project = Project::findOrFail($project_id);
        $project_doc = $project->project_docs()->findOrFail($project_doc_id);

        try{
            $path = \Storage::get('public/project_docs/'.$project_doc->file);
            $mimetype = \Storage::mimeType('public/project_docs/'.$project_doc->file);

            return \Response::make($path, 200, ['Content-Type' => $mimetype]);
        }
        catch (\Exception $e) {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $project_id
     * @return \Illuminate\Http\Response
     */
    public function edit($project_id, $project_doc_id)
    {
        $project = Project::findOrFail($project_id);
        $project_doc = $project->project_docs()->findOrFail($project_doc_id);
        
        return view('project.documents.edit',compact('project_doc'));
    }

    public function update($project_id,$project_doc_id,Request $request )
    {
        $project = Project::findOrFail($project_id);
        
        $request->validate(array(
            'title'=>'required|max:191',
            'file'=> 'nullable|file'
        ),array(
            'title.required' => 'Title of Project Doc is required',
            'file.file' => 'File is invalid',
        ));
        
        $project_doc = $project->project_docs()->findOrFail($project_doc_id);
        
        $project_doc->title = $request->title;
        
        if($request->hasFile('file')){

            if(file_exists(storage_path('public/project_docs/'.$project_doc->file)))
                unlink(storage_path('public/project_docs/'.$project_doc->file));

            $file = $request->file('file');
            $filenameWithExt = $file->getClientOriginalName();
            $name = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            $filename = str_replace(' ','',$name).'_'.time(). '.' . $file->getClientOriginalExtension();
            $path = $file->storeAs('public/project_docs', $filename);
            $project_doc->file = $filename;
        }

        $project_doc->updated_by=Auth::user()->id;
        $project_doc->save();   

        $request->session()->flash('success', 'Project Doc edited successfully');
        return redirect()->route('docs.index',$project_id);
    }

    public function destroy($project_id,$project_doc_id, Request $request)
    {
        $project = Project::findOrFail($project_id);
        $project_doc = $project->project_docs()->findOrFail($project_doc_id);

        if(file_exists(storage_path('public/project_docs/'.$project_doc->file)))
            unlink(storage_path('public/project_docs/'.$project_doc->file));

        $project_doc->delete();
        $request->session()->flash('success', 'Project Doc deleted successfully');
        
        return redirect()->back();

    }
}
