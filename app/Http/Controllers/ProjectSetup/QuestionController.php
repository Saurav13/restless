<?php

namespace App\Http\Controllers\ProjectSetup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;
use App\Question;
use Auth;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($project_id)
    {
        $project = Project::findOrFail($project_id);
        $questions = $project->questions()->orderBy('id','desc')->paginate(10);

        return view('project.settings.questions.index',compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store($project_id, Request $request)
    {
        $project = Project::findOrFail($project_id);
        $request->validate(array(
            'question'=>'required|max:191',
            'question_type'=> 'required|in:choice,non-choice',
        ),array(
            'question.required' => 'Question is required',
            'question_type.required' => 'Question Type is required',
            'question_type.in' => 'Question Type is invalid'
        ));
        
        $question= new Question;
        $question->question = $request->question;
        $question->question_type = $request->question_type;
        $question->project_id = $project_id;
        $question->created_by=Auth::user()->id;
        $question->updated_by=Auth::user()->id;

        $question->save();

        $request->session()->flash('success', 'New Question created');
        return redirect()->route('settings.questions-planner.index',$project_id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $project_id
     * @return \Illuminate\Http\Response
     */
    public function edit($project_id, $question_id)
    {
        $project = Project::findOrFail($project_id);
        $question = $project->questions()->findOrFail($question_id);
        
        return view('project.settings.questions.edit',compact('question'));
    }

    public function update($project_id,$question_id,Request $request )
    {
        $project = Project::findOrFail($project_id);
        
        $request->validate(array(
            'question'=>'required|max:191',
            'question_type'=> 'required|in:choice,non-choice',
        ),array(
            'question.required' => 'Question is required',
            'question_type.required' => 'Question Type is required',
            'question_type.in' => 'Question Type is invalid'
        ));

        $question = $project->questions()->findOrFail($question_id);

        $question->question = $request->question;
        $question->question_type = $request->question_type;
        $question->updated_by=Auth::user()->id;
        $question->save();   

        $request->session()->flash('success', 'Question edited successfully');
        return redirect()->route('settings.questions-planner.index',$project_id);
    }

    public function destroy($project_id,$question_id, Request $request)
    {
        $project = Project::findOrFail($project_id);
        $question = $project->questions()->findOrFail($question_id);

        $question->delete();
        $request->session()->flash('success', 'Question deleted successfully');
        
        return redirect()->back();

    }
}
