<?php

namespace App\Http\Controllers\ProjectSetup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;
use App\Output;
use Auth;
class OutputController extends Controller
{
     public function index($id)
    {
        $outcomes=Project::findOrFail($id)->outcomes;
        $outputs=Project::findOrFail($id)->outputs;
        return view('project.outputs.index',compact('outputs','outcomes'));
    }
    public function create($id,Request $request)
    {
        $outcome_id=$request->outcome_id;
        $outcomes=Project::findOrFail($id)->outcomes;
       
        return view('project.outputs.create',compact('outcome_id','outcomes'));

    }

    public function store($id, Request $request)
    {

        $request->validate(array(
            'title'=>'required|max:191',
            'description'=> 'required',
            'outcome_id'=>'required|exists:outcomes,id'
        ),array(
            'title.required' => 'Title of Output is required',
            'description.required' => 'Details of Output is required',
            'outcome_id.required' => 'Please Select Outcome',
            'outcome_id.exists' => 'Invalid Input'

        ));

        $output= new Output;
        $output->title=$request->title;
        $output->description=$request->description;
        $output->initial_situation=$request->initial_situation;
        $output->expected_situation=$request->expected_situation;
        $output->approach=$request->approach;
        $output->outcome_id=$request->outcome_id;
        $output->created_by=Auth::user()->id;
        $output->updated_by=Auth::user()->id;

        $output->save();

        $request->session()->flash('success', 'New Output created');
        return redirect()->to('/project'.'/'.$id.'/outputs');

    }
    public function edit($id, $output_id)
    {
        $output=Output::findOrFail($output_id);
        return view('project.outputs.edit',compact('output'));
    }

    public function update($id,$output_id,Request $request )
    {
        $request->validate(array(
            'title'=>'required|max:191',
            'description'=> 'required'
        ),array(
            'title.required' => 'Title of Output is required',
            'description.required' => 'Details of Output is required'
        ));

        $output= Output::findOrFail($output_id);
        $output->title=$request->title;
        $output->description=$request->description;
        $output->initial_situation=$request->initial_situation;
        $output->expected_situation=$request->expected_situation;
        $output->approach=$request->approach;
        $output->updated_by=Auth::user()->id;

        $output->save();   

        $request->session()->flash('success', 'Output edited successfully');
        return redirect()->to('/project'.'/'.$id.'/outputs');
    }

    public function destroy($id,$output_id)
    {
        $output=Output::findOrFail($output_id);
        $output->delete();
        $request->session()->flash('success', 'Output deleted successfully');

        return redirect()->to('/project'.'/'.$id.'/outputs');

    }
}
