<?php

namespace App\Http\Controllers\ProjectSetup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;
use App\BudgetCategory;
use App\BudgetItem;
use App\BudgetPlan;
use Auth;

class MNEBudgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($project_id)
    {
        $project = Project::findOrFail($project_id);
        $activities = $project->mneactivities;
        
        return view('project.finance.mne_budget_planner',compact('project','activities'));
    }

    public function show($project_id, $activity_id, Request $request)
    {
        $project = Project::findOrFail($project_id);
        $activities = $project->mneactivities;

        $activity = $activities->where('id',$activity_id)->first();

        if(!$activity) abort(404);

        $budget_categories = BudgetCategory::orderBy('title')->get();
        $partners = $project->partners()->orderBy('fullname')->get();
        $budget_items = $activity->budget_items()->with('budget_plans')->get();
        
        return view('project.finance.mne_budget_planner',compact('project','activities','activity','budget_categories','partners','budget_items'));
    }

    public function update(Request $request, $project_id, $activity_id)
    {
        $project = Project::findOrFail($project_id);
        $partners = $project->partners()->pluck('partners.id')->toArray();

        $request->validate(array(
            // 'budget_items.*.id'=>'nullable|exists:budget_items,id',
            'budget_items.*.budget_category'=>'required',
            'budget_items.*.partner_id' => 'required|in:'.implode(',', $partners),
            'budget_items.*.description'=> 'required',
            'budget_items.*.budget_plans'=> 'required|min:1|array',
            'budget_items.*.budget_plans.*.amount'=> 'required|min:0|integer',
            'budget_items.*.budget_plans.*.year'=> 'required',
            'budget_items.*.budget_plans.*.month'=> 'required|integer|between:1,12',
        ));
        
        $activity = $project->mneactivities->where('id',$activity_id)->first();

        if(!$activity) abort(404);

        $activity->budget_items()->delete();

        foreach($request->budget_items as $item){
            $budget_item= new BudgetItem;
            $budget_item->budget_category = $item['budget_category'];
            $budget_item->partner_id = $item['partner_id'];
            $budget_item->description = $item['description'];
            $budget_item->created_by=Auth::user()->id;
            $budget_item->updated_by=Auth::user()->id;

            $budget_item = $activity->budget_items()->save($budget_item);
            
            $budget_plans = [];
            foreach($item['budget_plans'] as $plan){
                $budget_plan= new BudgetPlan;
                $budget_plan->year = $plan['year'];
                $budget_plan->month = str_pad($plan['month'], 2, "0", STR_PAD_LEFT);
                $budget_plan->amount = $plan['amount'];

                $budget_plans []= $budget_plan;
            }
            $budget_item->budget_plans()->saveMany($budget_plans);
        }

        $request->session()->flash('success', 'M&E Activity Budget Planner updated Successfully.');
        return redirect()->route('finance.mne-budget-planner',[$project_id,$activity_id]);
    }
}
