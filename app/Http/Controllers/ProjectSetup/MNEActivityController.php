<?php

namespace App\Http\Controllers\ProjectSetup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MNEActivity;
use App\Project;
use Auth;

class MNEActivityController extends Controller
{
    public function index($id)
    {
        $mneactivities=MNEActivity::where('project_id',$id)->paginate(10);
        return view('project.mneactivities.index',compact('mneactivities'));
    }
    public function create($id)
    {
        $project = Project::findOrFail($id);

        return view('project.mneactivities.create',compact('project'));

    }

    public function store($id, Request $request)
    {
        $project = Project::findOrFail($id);

        $request->validate(array(
            'description'=> 'required',
            'mne_category'=> 'required|exists:mne_categories,title',
            'quarters' => 'required|array|min:1',
            'quarters.*' => 'required|exists:quarters,id,project_id,'.$id,
            'measures.*' => 'nullable|in:Baseline,Midline,Endline',

        ),array(
            'description.required' => 'Details of M&E Activity is required',
            'mne_category.required' => 'Please Select M&E category',
            'mne_category.exists' => 'Invalid M&E Category',
            'measures.*' => 'Invalid Entry'

        ));

        foreach($request->measures as $quarter => $measure){

            if(!$measure) continue;
            
            $mneactivity= new MNEActivity;
            $mneactivity->description=$request->description;
            $mneactivity->mne_category=$request->mne_category;
            $mneactivity->quarter_id = $quarter;
            $mneactivity->measure = $measure;
            $mneactivity->project_id=$id;
            $mneactivity->created_by=Auth::user()->id;
            $mneactivity->updated_by=Auth::user()->id;

            $mneactivity->save();
        }

        $request->session()->flash('success', 'New M&E Activity created');
        return redirect()->to('/project'.'/'.$id.'/mneactivities');

    }
    public function edit($id, $mneactivity_id)
    {
        $project = Project::findOrFail($id);
        
        $mneactivity=MNEActivity::findOrFail($mneactivity_id);
        return view('project.mneactivities.edit',compact('mneactivity','project'));
    }

    public function update($id,$mneactivity_id,Request $request )
    {
        $request->validate(array(
            'description'=> 'required',
            'mne_category'=> 'required|exists:mne_categories,title',
            'quarter' => 'required|exists:quarters,id,project_id,'.$id,
            'measures.'.$request->quarter => 'required|in:Baseline,Midline,Endline',

        ),array(
            'description.required' => 'Details of M&E Activity is required',
            'mne_category.required' => 'Please Select M&E category',
            'mne_category.exists' => 'Invalid M&E Category',
            'measures.*' => 'Invalid Entry'
            
        ));

        $mneactivity= MNEActivity::findOrFail($mneactivity_id);
        $mneactivity->description=$request->description;
        $mneactivity->mne_category=$request->mne_category;
        $mneactivity->quarter_id = $request->quarter;
        $mneactivity->measure = $request->measures[$request->quarter];

        $mneactivity->updated_by=Auth::user()->id;

        $mneactivity->save();   

        $request->session()->flash('success', 'MNEActivity edited successfully');
        return redirect()->to('/project'.'/'.$id.'/mneactivities');
    }

    public function destroy($id,$mneactivity_id,Request $request)
    {
        $mneactivity=MNEActivity::findOrFail($mneactivity_id);
        $mneactivity->delete();
        $request->session()->flash('success', 'MNEActivity deleted successfully');

        return redirect()->to('/project'.'/'.$id.'/mneactivities');

    }
}
