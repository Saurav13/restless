<?php

namespace App\Http\Controllers\ProjectSetup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;
use App\ActivityInstance;
use Auth;

class ActivityPlannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all($project_id)
    {
        $project = Project::findOrFail($project_id);
        $activities = $project->activities;
        return view('project.activities.planner.index',compact('project','activities'));
    }

    public function index($project_id, $activity_id, Request $request)
    {
        $project = Project::findOrFail($project_id);
        $activities = $project->activities;

        $activity = $activities->where('id',$activity_id)->first();

        if(!$activity) abort(404);

        $activity_instances = $activity->instances()->paginate(10);
        
        return view('project.activities.planner.index',compact('project','activities','activity','activity_instances'));
    }

    public function create($project_id, $activity_id)
    {
        $project = Project::findOrFail($project_id);
        $activities = $project->activities;
    
        $activity = $activities->where('id',$activity_id)->first();

        if(!$activity) abort(404);

        $partners = $project->partners;
        $geographic_regions = $project->geographic_regions;
        
        return view('project.activities.planner.create',compact('project','activity','geographic_regions','partners'));
    }

    public function store($project_id, $activity_id, Request $request)
    {
        $project = Project::findOrFail($project_id);
        $activities = $project->activities;
    
        $activity = $activities->where('id',$activity_id)->first();

        if(!$activity) abort(404);

        $partners = $project->partners()->pluck('partners.id')->toArray();

        $request->validate(array(
            'geographic_region'=>'required|exists:geographic_regions,id,project_id,'.$project_id,
            'partners' => 'required|array|min:1',
            'partners.*' => 'required|in:'.implode(',', $partners),
            'title'=> 'required',
            'description'=> 'required',
            'objective'=> 'required',
            'approach'=> 'required',
            'participant_target_type'=> 'required|in:Limited,Mass',
            'quarters' => 'required|array|min:1',
            'quarters.*' => 'required|exists:quarters,id,project_id,'.$project_id,
        ));

        foreach($request->quarters as $quarter){
            $instance= new ActivityInstance;
            $instance->geographic_region_id = $request->geographic_region;
            $instance->title = $request->title;
            $instance->description = $request->description;
            $instance->objective = $request->objective;
            $instance->approach = $request->approach;
            $instance->participant_target_type = $request->participant_target_type;
            $instance->quarter_id = $quarter;
            $instance->created_by=Auth::user()->id;
            $instance->updated_by=Auth::user()->id;

            $instance = $activity->instances()->save($instance); 
            
            $instance->partners()->attach($request->partners);
        }
        
        $request->session()->flash('success', 'Activity Planner created Successfully.');
        return redirect()->route('activities.activity-planner.index',[$project_id,$activity_id]);
    }

    public function edit($project_id, $activity_id,$instance_id)
    {
        $project = Project::findOrFail($project_id);
        $activities = $project->activities;
    
        $activity = $activities->where('id',$activity_id)->first();

        if(!$activity) abort(404);

        $partners = $project->partners;

        $geographic_regions = $project->geographic_regions;
        $instance = $activity->instances()->findOrFail($instance_id);
        
        return view('project.activities.planner.edit',compact('project','activity','geographic_regions','partners','instance'));
    }

    public function update(Request $request, $project_id, $activity_id, $instance_id)
    {
        $project = Project::findOrFail($project_id);

        $activities = $project->activities;
    
        $activity = $activities->where('id',$activity_id)->first();

        if(!$activity) abort(404);

        $partners = $project->partners()->pluck('partners.id')->toArray();

        $request->validate(array(
            'geographic_region'=>'required|exists:geographic_regions,id,project_id,'.$project_id,
            'title'=> 'required',
            'description'=> 'required',
            'objective'=> 'required',
            'approach'=> 'required',
            'partners' => 'required|array|min:1',
            'partners.*' => 'required|in:'.implode(',', $partners),
            'participant_target_type'=> 'required|in:Limited,Mass',
            'quarter' => 'required|exists:quarters,id,project_id,'.$project_id,
        ));

        $instance = $activity->instances()->findOrFail($instance_id);

        $instance->geographic_region_id = $request->geographic_region;
        $instance->title = $request->title;
        $instance->description = $request->description;
        $instance->objective = $request->objective;
        $instance->approach = $request->approach;
        $instance->participant_target_type = $request->participant_target_type;
        $instance->quarter_id = $request->quarter;
        $instance->updated_by=Auth::user()->id;

        $instance->save();

        $instance->partners()->sync($request->partners);

        $request->session()->flash('success', 'Activity Planner updated Successfully.');
        return redirect()->route('activities.activity-planner.index',[$project_id,$activity_id]);
    }

    public function destroy($project_id, $activity_id,$instance_id,Request $request)
    {
        $project = Project::findOrFail($project_id);
        $activities = $project->activities;
    
        $activity = $activities->where('id',$activity_id)->first();

        if(!$activity) abort(404);

        $activity->instances()->where('id',$instance_id)->delete();
        
        $request->session()->flash('success', 'Activity deleted Successfully.');
        return redirect()->route('activities.activity-planner.index',[$project_id,$activity_id]);
        
    }
}
