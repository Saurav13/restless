<?php

namespace App\Http\Controllers\ProjectSetup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;
use App\Indicator;
use App\IndicatorBeneficiary;
use Auth;

class ProjectIndicatorController extends Controller
{
    public function index($project_id)
    {
        $project = Project::findOrFail($project_id);
        
        $indicators = $project->indicators()->orderBy('id','desc')->paginate(10);

        return view('project.projectindicators.index',compact('indicators'));
    }

    public function create($project_id)
    {
        $project = Project::findOrFail($project_id);
        
        $beneficiaries = $project->beneficiaries;

        return view('project.projectindicators.create',compact('beneficiaries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store($project_id, Request $request)
    {
        $project = Project::findOrFail($project_id);
       
        $request->validate([
            'file'=> 'nullable|file',
            'title'=> 'required',
            'description'=> 'required',
            'reporting_frequency'=> 'required',
            'means_of_verification'=> 'required',
            'overall_target_type'=> 'required|in:Number,Percentage',
            'overall_target'=> 'required|integer|min:0',
            'beneficiaries' => 'required|array|min:1',
            'beneficiaries.*.id'=>'required|exists:beneficiaries,id,project_id,'.$project_id,
            'beneficiaries.*.target_type' => 'required|in:Number,Percentage',
            'beneficiaries.*.target'=> 'required|integer|min:0'
        ],array(
            'title.required' => 'Title is required',
            'file.file' => 'File is invalid',
            'beneficiaries.required' => 'Reach Target is required',
            'beneficiaries.array' => 'Reach Target is invalid',
            'beneficiaries.min' => 'Reach Target is invalid',
        ));
        
        $indicator= new Indicator;
        $indicator->title = $request->title;
        $indicator->description = $request->description;
        $indicator->reporting_frequency = $request->reporting_frequency;
        $indicator->means_of_verification = $request->means_of_verification;
        $indicator->overall_target_type = $request->overall_target_type;
        $indicator->overall_target = $request->overall_target;
        $indicator->created_by=Auth::user()->id;
        $indicator->updated_by=Auth::user()->id;
        
        if($request->hasFile('file')){
            $file = $request->file('file');
            $filenameWithExt = $file->getClientOriginalName();
            $name = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            $filename = str_replace(' ','',$name).'_'.time(). '.' . $file->getClientOriginalExtension();
            $path = $file->storeAs('public/indicator_files', $filename);
            $indicator->file = $filename;
        }
        
        $indicator = $project->indicators()->save($indicator);

        $beneficiaries = [];
        foreach($request->beneficiaries as $b){
            $beneficiary = new IndicatorBeneficiary;
            $beneficiary->target = $b['target'];	
            $beneficiary->target_type = $b['target_type'];
            $beneficiary->indicator_id = $indicator->id;
            $beneficiary->beneficiary_id = $b['id'];
            $beneficiary->created_by=Auth::user()->id;
            $beneficiary->updated_by=Auth::user()->id;
            $beneficiaries []= $beneficiary;
        }

        $indicator->beneficiaries()->saveMany($beneficiaries);

        $request->session()->flash('success', 'New Indicator created');
        return redirect()->route('projects.indicators.index',[$project_id]);
    }

    public function show($project_id,$type, $id,$indicator_id){
        $project = Project::findOrFail($project_id);
        
        $indicator = $project->indicators()->findOrFail($indicator_id);

        try{
            $path = \Storage::get('public/indicator_files/'.$indicator->file);
            $mimetype = \Storage::mimeType('public/indicator_files/'.$indicator->file);

            return \Response::make($path, 200, ['Content-Type' => $mimetype]);
        }
        catch (\Exception $e) {
            abort(404);

        
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $project_id
     * @return \Illuminate\Http\Response
     */
    public function edit($project_id, $indicator_id)
    {
        $project = Project::findOrFail($project_id);

        $indicator = $project->indicators()->findOrFail($indicator_id);
        $beneficiaries = $project->beneficiaries;

        $indicator_beneficiaries = $indicator->beneficiaries;
        
        return view('project.projectindicators.edit',compact('indicator','beneficiaries','indicator_beneficiaries'));
    }

    public function update($project_id,$indicator_id,Request $request )
    {

        $project = Project::findOrFail($project_id);

        
        $request->validate([
            'file'=> 'nullable|file',
            'title'=> 'required',
            'description'=> 'required',
            'reporting_frequency'=> 'required',
            'means_of_verification'=> 'required',
            'overall_target_type'=> 'required|in:Number,Percentage',
            'overall_target'=> 'required|integer|min:0',
            'beneficiaries' => 'required|array|min:1',
            'beneficiaries.*.id'=>'required|exists:beneficiaries,id,project_id,'.$project_id,
            'beneficiaries.*.target_type' => 'required|in:Number,Percentage',
            'beneficiaries.*.target'=> 'required|integer|min:0'
        ],array(
            'title.required' => 'Title is required',
            'file.file' => 'File is invalid',
            'beneficiaries.required' => 'Reach Target is required',
            'beneficiaries.array' => 'Reach Target is invalid',
            'beneficiaries.min' => 'Reach Target is invalid',
        ));
        
        $indicator= $project->indicators()->findOrFail($indicator_id);

        $indicator->title = $request->title;
        $indicator->description = $request->description;
        $indicator->reporting_frequency = $request->reporting_frequency;
        $indicator->means_of_verification = $request->means_of_verification;
        $indicator->overall_target_type = $request->overall_target_type;
        $indicator->overall_target = $request->overall_target;
        $indicator->updated_by=Auth::user()->id;
        
        if($request->hasFile('file')){
            if($indicator->file && file_exists(storage_path('public/indicator_files/'.$indicator->file)))
                unlink(storage_path('public/indicator_files/'.$indicator->file));

            $file = $request->file('file');
            $filenameWithExt = $file->getClientOriginalName();
            $name = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            $filename = str_replace(' ','',$name).'_'.time(). '.' . $file->getClientOriginalExtension();
            $path = $file->storeAs('public/indicator_files', $filename);
            $indicator->file = $filename;
        }
        
        $indicator->save();
        
        $indicator->beneficiaries()->delete();

        $beneficiaries = [];
        foreach($request->beneficiaries as $b){
            $beneficiary = new IndicatorBeneficiary;
            $beneficiary->target = $b['target'];	
            $beneficiary->target_type = $b['target_type'];
            $beneficiary->indicator_id = $indicator->id;
            $beneficiary->beneficiary_id = $b['id'];
            $beneficiary->created_by=Auth::user()->id;
            $beneficiary->updated_by=Auth::user()->id;
            $beneficiaries []= $beneficiary;
        }

        $indicator->beneficiaries()->saveMany($beneficiaries);

        $request->session()->flash('success', 'Indicator edited successfully');
        return redirect()->route('projects.indicators.index',[$project_id]);

    }

    public function destroy($project_id,$indicator_id, Request $request)
    {
        $project = Project::findOrFail($project_id);

        $indicator = $project->indicators()->findOrFail($indicator_id);

        if($indicator->file && file_exists(storage_path('public/indicator_files/'.$indicator->file)))
            unlink(storage_path('public/indicator_files/'.$indicator->file));

        $indicator->delete();
        $request->session()->flash('success', 'Indicator deleted successfully');
        
        return redirect()->back();

    }
}
