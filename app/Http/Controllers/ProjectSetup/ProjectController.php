<?php

namespace App\Http\Controllers\ProjectSetup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    public function index()
    {
        return view('project.dashboard');
    }
}
