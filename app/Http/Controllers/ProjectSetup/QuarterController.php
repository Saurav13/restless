<?php

namespace App\Http\Controllers\ProjectSetup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;
use App\Quarter;
use Carbon\Carbon;
use Auth;

class QuarterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $quarters=Quarter::where('project_id',$id)->get();
        $project=Project::findOrFail($id);
        return view('project.quarters.index',compact('project','quarters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id, Request $request)
    {
        $year=$request->year;
        $quarters=Quarter::where('project_id',$id)->where('year',$request->year)->get();
        $project=Project::findOrFail($id);

        $dates = $project->termArray($year);
        // dd($dates);
        
        return view('project.quarters.create',compact('quarters','year','project','dates'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id,Request $request)
    {
        $project=Project::findOrFail($id);
        $quarters=json_decode($request->quarters);
        $year=$request->year;
        // dd($request->all());
        // dd($quarters[0],);

        
        
        if($this->validateQuarter($project,$quarters,$year))
        {
            Quarter::where('project_id',$project->id)->where('year',$year)->delete();
            foreach($quarters as $quarter)
            {
                $q= new Quarter;
                $q->start_month=$quarter->start_month;
                $q->end_month=$quarter->end_month;
                $q->year=$year;
                $q->project_id=$project->id;
                $q->created_by=Auth::user()->id;
                $q->updated_by=Auth::user()->id;

                $q->save();

            }
            // dd($request->quarters);  
            return redirect()->to('/project'.'/'.$project->id.'/quarters');
        }
        else{
            return "fail";
        }
       

    }

    private function validateQuarter($project, $quarters ,$year)
    {
        $start_month=$project->getStartMonthOfYear($year);
        $end_month=$project->getEndMonthOfYear($year);
        
        if($quarters[0]->start_month==$start_month && $quarters[count($quarters)-1]->end_month==$end_month)
        {
            for($i=0;$i<count($quarters)-1;$i+=1)
            {
                if(Carbon::parse($quarters[$i]->end_month)->addMonths(1)->format('Y-m') != $quarters[$i+1]->start_month)
                {
                    return fasle;    
                }
                if(Carbon::parse($quarters[$i]->end_month)->lt(Carbon::parse($quarters[$i]->start_month)))
                    return false;
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
