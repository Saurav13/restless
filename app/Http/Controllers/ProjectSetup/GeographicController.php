<?php

namespace App\Http\Controllers\ProjectSetup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GeographicRegion;
use Auth;

class GeographicController extends Controller
{
    public function index($id)
    {
        $geographicregions=GeographicRegion::where('project_id',$id)->paginate(10);
        return view('project.geographicregions.index',compact('geographicregions'));
    }
    public function create($id)
    {
        return view('project.geographicregions.create');

    }

    public function store($id, Request $request)
    {
        $request->validate(array(
            'ward'=>'required|max:191',
            'metropolitan'=> 'required|max:191',
            'district'=> 'required|max:191',
            'province'=> 'required|max:191',
            'country'=> 'required|max:191',
        ),array(
            'ward.required' => 'Ward of Geographic Area is required',
            'metropolitan.required' => 'Metropolitan of Geographic Area is required',
            'district.required' => 'District of Geographic Area is required',
            'province.required' => 'Province of Geographic Area is required',
            'country.required' => 'Country of Geographic Area is required',
        ));

        $geographicregion= new GeographicRegion;
        $geographicregion->ward=$request->ward;
        $geographicregion->metropolitan=$request->metropolitan;
        $geographicregion->district=$request->district;
        $geographicregion->province=$request->province;
        $geographicregion->country=$request->country;
        $geographicregion->project_id=$id;
        $geographicregion->created_by=Auth::user()->id;
        $geographicregion->updated_by=Auth::user()->id;

        $geographicregion->save();

        $request->session()->flash('success', 'New Geographic Area added');
        return redirect()->to('/project'.'/'.$id.'/geographicregions');

    }
    public function edit($id, $geographicregion_id)
    {
        $geographicregion=GeographicRegion::findOrFail($geographicregion_id);
        return view('project.geographicregions.edit',compact('geographicregion'));
    }

    public function update($id,$geographicregion_id,Request $request )
    {
        $request->validate(array(
            'ward'=>'required|max:191',
            'metropolitan'=> 'required|max:191',
            'district'=> 'required|max:191',
            'province'=> 'required|max:191',
            'country'=> 'required|max:191',
        ),array(
            'ward.required' => 'Ward of Geographic Area is required',
            'metropolitan.required' => 'Metropolitan of Geographic Area is required',
            'district.required' => 'District of Geographic Area is required',
            'province.required' => 'Province of Geographic Area is required',
            'country.required' => 'Country of Geographic Area is required',
        ));

        $geographicregion= GeographicRegion::findOrfail($geographicregion_id);
        $geographicregion->ward=$request->ward;
        $geographicregion->metropolitan=$request->metropolitan;
        $geographicregion->district=$request->district;
        $geographicregion->province=$request->province;
        $geographicregion->country=$request->country;
        $geographicregion->updated_by=Auth::user()->id;

        $geographicregion->save();  

        $request->session()->flash('success', 'Geographic Area edited successfully');
        return redirect()->to('/project'.'/'.$id.'/geographicregions');
    }

    public function destroy($id,$geographicregion_id,Request $request)
    {
        $geographicregion=GeographicRegion::findOrFail($geographicregion_id);
        $geographicregion->delete();
        $request->session()->flash('success', 'Geographic Area deleted successfully');

        return redirect()->to('/project'.'/'.$id.'/geographicregions');

    }
}
