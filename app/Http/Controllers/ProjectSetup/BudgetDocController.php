<?php

namespace App\Http\Controllers\ProjectSetup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;
use App\BudgetDoc;
use Auth;

class BudgetDocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($project_id)
    {
        $project = Project::findOrFail($project_id);
        $budget_docs = $project->budget_docs()->orderBy('id','desc')->paginate(10);

        return view('project.finance.budget_docs.index',compact('budget_docs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store($project_id, Request $request)
    {
        $project = Project::findOrFail($project_id);
        $request->validate(array(
            'title'=>'required|max:191',
            'file'=> 'required|file'
        ),array(
            'title.required' => 'Title of Budget Doc is required',
            'file.required' => 'File is required',
            'file.file' => 'File is invalid',
        ));
        
        $budget_doc= new BudgetDoc;
        $budget_doc->title = $request->title;
        
        if($request->hasFile('file')){
            $file = $request->file('file');
            $filenameWithExt = $file->getClientOriginalName();
            $name = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            $filename = str_replace(' ','',$name).'_'.time(). '.' . $file->getClientOriginalExtension();
            $path = $file->storeAs('public/budget_docs', $filename);
            $budget_doc->file = $filename;
        }
        
        $budget_doc->project_id = $project_id;
        $budget_doc->created_by=Auth::user()->id;
        $budget_doc->updated_by=Auth::user()->id;

        $budget_doc->save();

        $request->session()->flash('success', 'New Budget Doc created');
        return redirect()->route('finance.budget-docs.index',$project_id);
    }

    public function show($project_id, $budget_doc_id){
        $project = Project::findOrFail($project_id);
        $budget_doc = $project->budget_docs()->findOrFail($budget_doc_id);

        try{
            $path = \Storage::get('public/budget_docs/'.$budget_doc->file);
            $mimetype = \Storage::mimeType('public/budget_docs/'.$budget_doc->file);

            return \Response::make($path, 200, ['Content-Type' => $mimetype]);
        }
        catch (\Exception $e) {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $project_id
     * @return \Illuminate\Http\Response
     */
    public function edit($project_id, $budget_doc_id)
    {
        $project = Project::findOrFail($project_id);
        $budget_doc = $project->budget_docs()->findOrFail($budget_doc_id);
        
        return view('project.finance.budget_docs.edit',compact('budget_doc'));
    }

    public function update($project_id,$budget_doc_id,Request $request )
    {
        $project = Project::findOrFail($project_id);
        
        $request->validate(array(
            'title'=>'required|max:191',
            'file'=> 'nullable|file'
        ),array(
            'title.required' => 'Title of Budget Doc is required',
            'file.file' => 'File is invalid',
        ));
        
        $budget_doc = $project->budget_docs()->findOrFail($budget_doc_id);
        
        $budget_doc->title = $request->title;
        
        if($request->hasFile('file')){

            if(file_exists(storage_path('public/budget_docs/'.$budget_doc->file)))
                unlink(storage_path('public/budget_docs/'.$budget_doc->file));

            $file = $request->file('file');
            $filenameWithExt = $file->getClientOriginalName();
            $name = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            $filename = str_replace(' ','',$name).'_'.time(). '.' . $file->getClientOriginalExtension();
            $path = $file->storeAs('public/budget_docs', $filename);
            $budget_doc->file = $filename;
        }

        $budget_doc->updated_by=Auth::user()->id;
        $budget_doc->save();   

        $request->session()->flash('success', 'Budget Doc edited successfully');
        return redirect()->route('finance.budget-docs.index',$project_id);
    }

    public function destroy($project_id,$budget_doc_id, Request $request)
    {
        $project = Project::findOrFail($project_id);
        $budget_doc = $project->budget_docs()->findOrFail($budget_doc_id);

        if(file_exists(storage_path('public/budget_docs/'.$budget_doc->file)))
            unlink(storage_path('public/budget_docs/'.$budget_doc->file));

        $budget_doc->delete();
        $request->session()->flash('success', 'Budget Doc deleted successfully');
        
        return redirect()->back();

    }
}
