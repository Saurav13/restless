<?php

namespace App\Http\Controllers\ProjectSetup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;
use App\NarrativeRecord;
use Auth;

class NarrativeRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($project_id)
    {
        $project = Project::findOrFail($project_id);
        $mne_narrative_records = $project->narrative_records()->where('type','MNE')->orderBy('id','desc')->paginate(10, ['*'], 'mne');
        $project_narrative_records = $project->narrative_records()->where('type','Project')->orderBy('id','desc')->paginate(10, ['*'], 'project');

        $years = $project->years;

        return view('project.finance.narrative_records.index',compact('mne_narrative_records','project_narrative_records','years'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store($project_id, Request $request)
    {
        $project = Project::findOrFail($project_id);
        $years = array_keys($project->years);
        $request->validate(array(
            'title'=>'required|max:191',
            'description'=> 'required',
            'year'=> 'nullable|in:'.implode(',', $years),
            'type'=> 'required|in:MNE,Project',
        ),array(
            'title.required' => 'Title of Narrative Record is required',
            'description.required' => 'Details of Narrative Record is required',
            'year.in' => 'Year of Narrative Record is invalid',
            'type.in' => 'Type of Narrative Record is invalid'
        ));
        
        $narrative_record= new NarrativeRecord;
        $narrative_record->title = $request->title;
        $narrative_record->description = $request->description;
        $narrative_record->year = $request->year;
        $narrative_record->type = $request->type;
        $narrative_record->project_id = $project_id;
        $narrative_record->created_by=Auth::user()->id;
        $narrative_record->updated_by=Auth::user()->id;

        $narrative_record->save();

        $request->session()->flash('success', 'New Narrative Record created');
        return redirect()->route('finance.narrative-records.index',$project_id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $project_id
     * @return \Illuminate\Http\Response
     */
    public function edit($project_id, $narrative_record_id)
    {
        $project = Project::findOrFail($project_id);
        $narrative_record = $project->narrative_records()->findOrFail($narrative_record_id);
        $years = $project->years;
        
        return view('project.finance.narrative_records.edit',compact('narrative_record','years'));
    }

    public function update($project_id,$narrative_record_id,Request $request )
    {
        $project = Project::findOrFail($project_id);
        $years = array_keys($project->years);
        
        $request->validate(array(
            'title'=>'required|max:191',
            'description'=> 'required',
            'year'=> 'nullable|in:'.implode(',', $years)
        ),array(
            'title.required' => 'Title of Narrative Record is required',
            'description.required' => 'Details of Narrative Record is required',
            'year.in' => 'Year of Narrative Record is invalid'
        ));
        $narrative_record = $project->narrative_records()->findOrFail($narrative_record_id);

        $narrative_record->title = $request->title;
        $narrative_record->description = $request->description;
        $narrative_record->year = $request->year;
        $narrative_record->updated_by=Auth::user()->id;
        $narrative_record->save();   

        $request->session()->flash('success', 'Narrative Record edited successfully');
        return redirect()->route('finance.narrative-records.index',$project_id);
    }

    public function destroy($project_id,$narrative_record_id, Request $request)
    {
        $project = Project::findOrFail($project_id);
        $narrative_record = $project->narrative_records()->findOrFail($narrative_record_id);

        $narrative_record->delete();
        $request->session()->flash('success', 'Narrative Record deleted successfully');
        
        return redirect()->back();

    }
}
