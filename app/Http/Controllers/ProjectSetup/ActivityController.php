<?php

namespace App\Http\Controllers\ProjectSetup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Activity;
use App\Project;

use Auth;

class ActivityController extends Controller
{
    public function index($id)
    {
        $output_ids=Project::findOrFail($id)->outputs->pluck('id');
        $activities=Activity::whereIn('output_id',$output_ids)->paginate(10);
        return view('project.activities.index',compact('activities'));
    }
    public function create($id)
    {
        return view('project.activities.create');

    }

    public function store($id, Request $request)
    {
        $request->validate(array(
            'title'=>'required|max:191',
            'description'=> 'required'
        ),array(
            'title.required' => 'Title of Activity is required',
            'description.required' => 'Details of Activity is required'
        ));

        $activity= new Activity;
        $activity->title=$request->title;
        $activity->description=$request->description;
        $activity->objective=$request->objective;
        $activity->output_id=$request->output_id;
        $activity->created_by=Auth::user()->id;
        $activity->updated_by=Auth::user()->id;

        $activity->save();

        $request->session()->flash('success', 'New Activity created');
        return redirect()->to('/project'.'/'.$id.'/activities');

    }
    public function edit($id, $activity_id)
    {
        $activity=Activity::findOrFail($activity_id);
        return view('project.activities.edit',compact('activity'));
    }

    public function update($id,$activity_id,Request $request )
    {
        $request->validate(array(
            'title'=>'required|max:191',
            'description'=> 'required'
        ),array(
            'title.required' => 'Title of Activity is required',
            'description.required' => 'Details of Activity is required'
        ));

        $activity= Activity::findOrFail($activity_id);
        $activity->title=$request->title;
        $activity->description=$request->description;
        $activity->objective=$request->objective;
        $activity->updated_by=Auth::user()->id;

        $activity->save();   

        $request->session()->flash('success', 'Activity edited successfully');
        return redirect()->to('/project'.'/'.$id.'/activities');
    }

    public function destroy($id,$activity_id,Request $request)
    {
        $activity=Activity::findOrFail($activity_id);
        $activity->delete();
        $request->session()->flash('success', 'Activity deleted successfully');

        return redirect()->to('/project'.'/'.$id.'/activities');

    }
}
