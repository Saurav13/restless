<?php

namespace App\Http\Controllers\ProjectSetup;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Beneficiary;
use Auth;

class BeneficiariesController extends Controller
{
    public function index($id)
    {
        $beneficiaries=Beneficiary::where('project_id',$id)->paginate(10);
        return view('project.beneficiaries.index',compact('beneficiaries'));
    }
    public function create($id)
    {
        return view('project.beneficiaries.create');

    }

    public function store($id, Request $request)
    {
        $request->validate(array(
            'title'=>'required|max:191',
            'gender'=> 'required|max:191',
            'age_range_from'=> 'required|numeric',
            'age_range_to'=> 'required|numeric',
            'ethnicity'=> 'required|max:191',
        ),array(
            'title.required' => 'Title is required',
            'gender.required' => 'Gender is required',
            'age_range_from.required' => 'Age range (from) is required',
            'age_range_from.numeric' => 'Age range (from) must be a number',
            'age_range_to.required' => 'Age rangee(to) is required',
            'age_range_to.numeric' => 'Age range (to) must be a number',
            'ethnicity.required' => 'Ethnicity is required',
        ));

        $beneficiary= new Beneficiary;
        $beneficiary->title=$request->title;
        $beneficiary->gender=$request->gender;
        $beneficiary->age_range_from=$request->age_range_from;
        $beneficiary->age_range_to=$request->age_range_to;
        $beneficiary->ethnicity=$request->ethnicity;
        $beneficiary->project_id=$id;
        $beneficiary->created_by=Auth::user()->id;
        $beneficiary->updated_by=Auth::user()->id;

        $beneficiary->save();

        $request->session()->flash('success', 'New Reach added');
        return redirect()->to('/project'.'/'.$id.'/beneficiaries');

    }
    public function edit($id, $beneficiary_id)
    {
        $beneficiary=Beneficiary::findOrFail($beneficiary_id);
        return view('project.beneficiaries.edit',compact('beneficiary'));
    }

    public function update($id,$beneficiary_id,Request $request )
    {
        $request->validate(array(
            'title'=>'required|max:191',
            'gender'=> 'required|max:191',
            'age_range_from'=> 'required|numeric',
            'age_range_to'=> 'required|numeric',
            'ethnicity'=> 'required|max:191',
        ),array(
            'title.required' => 'Title is required',
            'gender.required' => 'Gender is required',
            'age_range_from.required' => 'Age range (from) is required',
            'age_range_from.numeric' => 'Age range (from) must be a number',
            'age_range_to.required' => 'Age rangee(to) is required',
            'age_range_to.numeric' => 'Age range (to) must be a number',
            'ethnicity.required' => 'Ethnicity is required',
        ));

        $beneficiary= Beneficiary::findOrFail($beneficiary_id);
        $beneficiary->title=$request->title;
        $beneficiary->gender=$request->gender;
        $beneficiary->age_range_from=$request->age_range_from;
        $beneficiary->age_range_to=$request->age_range_to;
        $beneficiary->ethnicity=$request->ethnicity;
        $beneficiary->updated_by=Auth::user()->id;

        $beneficiary->save();  

        $request->session()->flash('success', 'Reach edited successfully');
        return redirect()->to('/project'.'/'.$id.'/beneficiaries');
    }

    public function destroy($id,$beneficiary_id,Request $request)
    {
        $beneficiary=Beneficiary::findOrFail($beneficiary_id);
        $beneficiary->delete();
        $request->session()->flash('success', 'Reach deleted successfully');

        return redirect()->to('/project'.'/'.$id.'/beneficiaries');

    }
}
