<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    public function output()
    {
        return $this->belongsTo('App\Output');
    }
    
    public function instances()
    {
        return $this->hasMany('App\ActivityInstance');
    }

    public function budget_items()
    {
        return $this->morphMany('App\BudgetItem', 'indicatorable');
    }

    public function getExpenseAttribute(){
        $expense = 0;

        foreach($this->budget_items as $item){
            $expense += $item->budget_plans()->sum('amount');
        }

        return $expense;
    }
}
