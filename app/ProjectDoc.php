<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectDoc extends Model
{
    public function uploader()
    {
        return $this->belongsTo('App\User', 'created_by');
    }
}
