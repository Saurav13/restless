<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivityInstance extends Model
{
    public function quarter()
    {
        return $this->belongsTo('App\Quarter');
    }

    public function creator()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function partners()
    {
        return $this->belongsToMany('App\Partner', 'activity_instance_partners');
    }
}
