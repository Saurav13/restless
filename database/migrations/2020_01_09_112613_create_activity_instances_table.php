<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityInstancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_instances', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('code')->nullable();
            $table->text('description')->nullable();
            $table->text('objective')->nullable();
            $table->text('approach')->nullable();
            $table->text('major_challenges')->nullable();

            $table->string('participant_target_type');
            $table->string('mass_reach')->nullable();
            
            $table->string('start_date')->nullable();
            $table->string('end_date')->nullable();

            $table->boolean('entry_status')->default(0);

            $table->bigInteger('activity_id')->unsigned();
            $table->bigInteger('geographic_region_id')->unsigned();
            $table->bigInteger('quarter_id')->unsigned();
            $table->bigInteger('created_by')->unsigned();
            $table->bigInteger('updated_by')->unsigned();

            $table->foreign('activity_id')->references('id')->on('activities')->onDelete('cascade');
            $table->foreign('geographic_region_id')->references('id')->on('geographic_regions')->onDelete('cascade');
            $table->foreign('quarter_id')->references('id')->on('quarters')->onDelete('cascade');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('cascade');
           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_instances');
    }
}
