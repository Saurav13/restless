<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivityInstancePartnerTablee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_instance_partners', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('activity_instance_id')->unsigned();
            $table->bigInteger('partner_id')->unsigned();

            $table->foreign('activity_instance_id')->references('id')->on('activity_instances')->onDelete('cascade');
            $table->foreign('partner_id')->references('id')->on('partners')->onDelete('cascade');
         
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_instance_partners');
    }
}
