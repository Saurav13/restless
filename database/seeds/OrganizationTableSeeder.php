<?php

use Illuminate\Database\Seeder;
use DB;

class OrganizationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('organizations')->insert([
            'fullname' => "Restless Development",
            'shortname' => "RD",
        ]);
    }
}
