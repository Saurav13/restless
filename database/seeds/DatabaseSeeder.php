<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('users')->insert([
        //     'name' => 'admin1',
        //     'email' => 'admin1@admin.com',
        //     'password' => bcrypt('admin1admin'),
        // ]);
        // $this->call(UsersTableSeeder::class);
         $this->call(OrganizationTableSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
    }
}
