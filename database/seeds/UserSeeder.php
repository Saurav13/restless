<?php

use Illuminate\Database\Seeder;
use App\Role;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superuser_id = DB::table('users')->insertGetId([
            'name' => 'admin1',
            'email' => 'admin1@admin.com',
            'password' => bcrypt('admin1admin'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        $superuser_role = Role::whereName('Super Admin')->first();

        if ($superuser_role) {
            $superuser_role->users()->attach($superuser_id);
        }

        $member_id = DB::table('users')->insertGetId([
            'name' => 'User1',
            'email' => 'user1@user.com',
            'password' => bcrypt('user1user'),           
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        $member_role = Role::whereName('Staff')->first();

        if ($member_role) {
            $member_role->users()->attach($member_id);
        }


        // for($i = 2; $i < 100; $i++) {
        //     DB::table('users')->insert([
        //         'name' => 'User'.$i,
        //         'email' => 'user'.$i.'@user.com',
        //         'password' => bcrypt('user'.$i.'user'),
        //         'status' => 'Active',            
        //         'created_at' => Carbon::now(),
        //         'updated_at' => Carbon::now()
        //     ]);
        // }
    }
}
