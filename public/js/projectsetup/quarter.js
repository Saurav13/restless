
var quarterapp=angular.module('quarterapp',[]);
quarterapp.controller('QuarterController',function($scope,$filter){
    $scope.quarter=[];
    $scope.start_month;
    $scope.end_month;
    $scope.months=[];
    $scope.monthCount;
    $scope.init=function(quarters,start_month,terms){
        console.log(terms)
        $scope.terms = terms;
        
        $scope.start_month=terms[0];
        $scope.end_month=terms[terms.length-1];
        
        // console.log(quarters,start_month)
        if(quarters.length==0)
            $scope.quarters=[{'start_month':$scope.start_month,'end_month':''}]
        else
            $scope.quarters=quarters;

        $scope.$$postDigest(function() {
            setFormValidation('#FormValidation');
        });
    }
   
    $scope.addQuarter=function(){
        // console.log($scope.quarters.length);
        //  if($scope.quarters.slice(-1)[0].end_month!=12){
        //     console.log($scope.quarters.slice(-1)[0].end_month)
        //     quarter={'start_month':$scope.quarters.slice(-1)[0].end_month,'end_month':''};

        // }
        // else{
        //     quarter=null;
        // }
        var quarter = {};
        $scope.quarters.push(quarter);
        
    }
    $scope.removeQuarter=function(index){
        $scope.quarters.splice(index,1);

        if($scope.quarters.length==0)
            $scope.quarters=[{'start_month':$scope.start_month,'end_month':''}]
    }
    $scope.changeToMonth=function(index){
        // console.log(index);
        if($scope.quarters[index].end_month!=12){
            $scope.quarters[index+1].start_month=$scope.quarters[index].end_month;
            // $scope.$apply();
        }
    }

    $scope.validateMonth=function(m,n,index,i){
        // console.log(m,n,index,i)
        if(!m || !n) return true;
        if(m < n)
            return true; //disabled
        else 
            return false; //enabled
    }
    $scope.addOneMonth=function(index){
        // console.log(index,$scope.quarters.length);
        if(index+1>=$scope.quarters.length)
            return;
        var d=new Date($scope.quarters[index].end_month);
        d.setMonth(d.getMonth()+1);

        $scope.quarters[index+1].start_month= $filter("date")(d,'yyyy-MM');
    }

});

quarterapp.directive('selectpicker', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ctrl) {  
            scope.$$postDigest(function(){
                element.selectpicker();

            })
        }
    };
});

quarterapp.filter('filter', function($filter) {

    return function(input) {
    
        var date = new Date(input);
        return($filter('date')(date, 'MMM yyyy'));
    }
});