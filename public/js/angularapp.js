var app = angular.module('Planner',[]);
app.controller('RepeatController',function($scope){
    $scope.remaining_budget = 0;
    $scope.temp = 0;

    $scope.init = function(oldValue){
        $scope.items = oldValue;
        console.log(oldValue)

        if($scope.items.length == 0)
            $scope.add();

        $scope.$$postDigest(function() {
            setFormValidation('#FormValidation');
        });
    }

    $scope.add= function()
    {
        var k= {};
        $scope.items.push(k);
    };
    
    $scope.remove= function(index)
    {
        var old = $scope.items[index].expense ? $scope.items[index].expense : 0;
        $scope.remaining_budget = $scope.remaining_budget + parseInt(old);

        $scope.items.splice(index,1);
        if($scope.items.length == 0) $scope.add();
    };

    $scope.recalculateBudget = function(index){
        // $scope.temp = angular.copy($scope.remaining_budget);
        var old = $scope.items[index].expense ? $scope.items[index].expense : 0;
        
        $scope.items[index].expense = 0;

        angular.forEach($scope.items[index].budget_plans, function(ele){
   
            $scope.items[index].expense += parseInt(ele.amount ? ele.amount : 0);
        });

        $scope.remaining_budget = $scope.remaining_budget + parseInt(old) - parseInt($scope.items[index].expense);

        console.log($scope.remaining_budget)
    }
});

app.controller('ActivityEvaluationController',function($scope){
    $scope.participants = [];
    $scope.reach = [];
    $scope.budgets = [];

    $scope.init = function(oldValue){
        $scope.participants = oldValue;
        console.log(oldValue)

        if($scope.participants.length == 0)
            $scope.addParticipant();
        if($scope.reach.length == 0)
            $scope.addReach();
        if($scope.budgets.length == 0)
            $scope.addBudget();

        $scope.$$postDigest(function() {
            setFormValidation('#FormValidation');
        });
    }

    $scope.addParticipant = function()
    {
        var k= {};
        $scope.participants.push(k);
    };
    
    $scope.removeParticipant = function(index)
    {
        $scope.participants.splice(index,1);
        if($scope.participants.length == 0) $scope.add();
    };

    $scope.addReach = function()
    {
        var k= {};
        $scope.reach.push(k);
    };
    
    $scope.removeReach = function(index)
    {
        $scope.reach.splice(index,1);
        if($scope.reach.length == 0) $scope.addReach();
    };

    $scope.addBudget = function()
    {
        var k= {};
        $scope.budgets.push(k);
    };
    
    $scope.removeBudget = function(index)
    {
        $scope.budgets.splice(index,1);
        if($scope.budgets.length == 0) $scope.addBudget();
    };
});

app.directive('convertToNumber', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            
            ngModel.$parsers.push(function(val) {
                return parseInt(val);
            });

            ngModel.$formatters.push(function(val) {
                if(!val) return;

                return parseInt(val);
            });
        }
    };
});

app.directive('convertToNumberSelect', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ngModel) {
            
            ngModel.$formatters.push(function(val) {
                if(!val) return '';
                return '' + val;
            });
        }
    };
});

app.directive('selectpicker', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, ctrl) {    
            scope.$$postDigest(function() {

                element.selectpicker();
            })
        }
    };
});

app.directive("preventTypingGreater", function() {
    return {
        link: function(scope, element, attributes) {
            var oldVal = null;
            element.on("keydown keyup", function(e) {
                
                if (scope.remaining_budget <= 0 &&
                    e.keyCode != 46 // delete
                    &&
                    e.keyCode != 8 // backspace
                ) {
                    e.preventDefault();
                    element.val(oldVal);
                    scope.recalculateBudget(attributes.index)
                } else {
                    oldVal = Number(element.val());
                }
            });
        }
    };
});