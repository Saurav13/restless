<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware' => ['web','auth']], function () {
    Route::get('/', function () {
        return view('admin.dashboard');
    });

    /*Organization Routes*/
    Route::get('organization/profile','Admin\OrganizationProfileController@profile')->name('organization.profile');
    Route::post('organization/profile/update','Admin\OrganizationProfileController@UpdateProfile')->name('organization.profile.update');
    /*End Organization Routes*/

    Route::resource('donors', 'Admin\DonorController');
    Route::resource('ethnicities', 'Admin\EthnicityController');
    Route::resource('mne-categories', 'Admin\MNECategoryController');
    Route::resource('budget-categories', 'Admin\BudgetCategoryController');
    Route::resource('partners', 'Admin\PartnerController');
    Route::resource('projects', 'Admin\ProjectController');
});


Route::prefix('project')->middleware('auth')->group(function(){

    Route::get('/{id}/dashboard','ProjectSetup\ProjectController@index');
    
    Route::resource('/{id}/mneactivities','ProjectSetup\MNEActivityController');
    Route::resource('/{id}/geographicregions','ProjectSetup\GeographicController');
    Route::resource('/{id}/beneficiaries','ProjectSetup\BeneficiariesController');
    Route::resource('/{id}/quarters','ProjectSetup\QuarterController');

    Route::resource('/{project_id}/indicators','ProjectSetup\ProjectIndicatorController',['as'=>'projects']);    
    Route::resource('/{project_id}/{type}/{id}/indicators','ProjectSetup\IndicatorController');

    Route::resource('/{project_id}/activities/{activity_id}/activity-planner','ProjectSetup\ActivityPlannerController',['as' => 'activities', 'except' => ['show']]);
    Route::get('/{project_id}/activities/activity-planner','ProjectSetup\ActivityPlannerController@all')->name('activities.activity-planner');

    Route::get('/{project_id}/activities/budget-planner','ProjectSetup\ActivityBudgetController@index')->name('finance.activity-budget-planner.index');
    Route::get('/{project_id}/activities/{activity_id}/budget-planner','ProjectSetup\ActivityBudgetController@show')->name('finance.activity-budget-planner');
    Route::post('/{project_id}/activities/{activity_id}/budget-planner','ProjectSetup\ActivityBudgetController@update');

    Route::get('/{project_id}/mne/budget-planner','ProjectSetup\MNEBudgetController@index')->name('finance.mne-budget-planner.index');
    Route::get('/{project_id}/mne/{mne_id}/budget-planner','ProjectSetup\MNEBudgetController@show')->name('finance.mne-budget-planner');
    Route::post('/{project_id}/mne/{mne_id}/budget-planner','ProjectSetup\MNEBudgetController@update');

    Route::resource('/{project_id}/narrative-records','ProjectSetup\NarrativeRecordController',[ 'as' => 'finance','except' => ['create','show'] ]);
    Route::resource('/{project_id}/budget-docs','ProjectSetup\BudgetDocController',[ 'as' => 'finance','except' => ['create'] ]);

    Route::resource('/{project_id}/docs','ProjectSetup\ProjectDocumentController');

    Route::resource('/{project_id}/questions-planner','ProjectSetup\QuestionController',['as' => 'settings','except' => ['create','show']]);

    Route::group(['namespace' => 'ProjectOperation',], function(){
        Route::get('/{project_id}/indicator-evaluation','ProjectEntryController@getEvaluation')->name('projects.indicator-evaluation.get');
        Route::post('/{project_id}/indicator-evaluation/save','ProjectEntryController@saveEvaluation')->name('projects.indicator-evaluation.save');

        Route::get('/{project_id}/progress-report','ProjectEntryController@getReport')->name('projects.progress-report.get');
        Route::post('/{project_id}/progress-report/save','ProjectEntryController@saveReport')->name('projects.progress-report.save');

        Route::get('/{project_id}/outcomes/indicator-evaluation','OutcomeEntryController@indexEvaluation')->name('outcomes.indicator-evaluation.index');
        Route::get('/{project_id}/outcomes/{outcome_id}/indicator-evaluation','OutcomeEntryController@getEvaluation')->name('outcomes.indicator-evaluation.get');
        Route::post('/{project_id}/outcomes/{outcome_id}/indicator-evaluation/save','OutcomeEntryController@saveEvaluation')->name('outcomes.indicator-evaluation.save');

        Route::get('/{project_id}/outcomes/progress-report','OutcomeEntryController@indexReport')->name('outcomes.progress-report.index');
        Route::get('/{project_id}/outcomes/{outcome_id}/progress-report','OutcomeEntryController@getReport')->name('outcomes.progress-report.get');
        Route::post('/{project_id}/outcomes/{outcome_id}/progress-report/save','OutcomeEntryController@saveReport')->name('outcomes.progress-report.save');

        Route::get('/{project_id}/outputs/indicator-evaluation','OutputEntryController@indexEvaluation')->name('outputs.indicator-evaluation.index');
        Route::get('/{project_id}/outputs/{output_id}/indicator-evaluation','OutputEntryController@getEvaluation')->name('outputs.indicator-evaluation.get');
        Route::post('/{project_id}/outputs/{output_id}/indicator-evaluation/save','OutputEntryController@saveEvaluation')->name('outputs.indicator-evaluation.save');

        Route::get('/{project_id}/outputs/progress-report','OutputEntryController@indexReport')->name('outputs.progress-report.index');
        Route::get('/{project_id}/outputs/{output_id}/progress-report','OutputEntryController@getReport')->name('outputs.progress-report.get');
        Route::post('/{project_id}/outputs/{output_id}/progress-report/save','OutputEntryController@saveReport')->name('outputs.progress-report.save');

        Route::get('/{project_id}/activities/indicator-evaluation','ActivityEntryController@indexEvaluation')->name('activities.indicator-evaluation.index');
        Route::get('/{project_id}/activities/{activity_id}/indicator-evaluation','ActivityEntryController@getEvaluation')->name('activities.indicator-evaluation.get');
        Route::post('/{project_id}/activities/{activity_id}/indicator-evaluation/save','ActivityEntryController@saveEvaluation')->name('activities.indicator-evaluation.save');

    });

    Route::resource('/{id}/outcomes','ProjectSetup\OutcomeController');
    Route::resource('/{id}/outputs','ProjectSetup\OutputController');
    Route::resource('/{id}/activities','ProjectSetup\ActivityController');

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
