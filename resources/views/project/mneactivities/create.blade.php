@extends('layouts.project')

@section('body')
<div class="row">
    
    <div class="col-md-12">
        
        <form action="/project/{{Request::segment(2)}}/mneactivities" method="POST" id="FormValidation" novalidate="novalidate">
            @csrf
            <div class="card ">
                <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">mail_outline</i>
                    </div>
                    <h4 class="card-title">Add M&E Activity</h4>
                </div>
                <div class="card-body ">
                   
                    <div class="form-group bmd-form-group">
                        <label class="bmd-label-floating"> Details* </label>
                        <textarea name="description" class="form-control {{ $errors->has('description') ? ' has-danger' : '' }}" value="{{ old('description') }}" rows="5" required>{{ old('description') }}</textarea>
                        @if ($errors->has('description'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('description') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="form-group bmd-form-group">
                            {{-- <label class="bmd-label-floating"> Output </label> --}}

                    
                        <select required name="mne_category" class="selectpicker" data-style="select-with-transition"  title="Choose M&E Category *" data-width="100%">
                            @foreach(\App\MNECategory::all() as $cats)
                                <option value="{{$cats->title}}" {{ old('mne_category') == $cats->title ? 'selected' : '' }}>{{$cats->title}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('mne_category'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('mne_category') }}</strong>
                            </div>
                        @endif
                    </div>
                    
                    <br>

                    @php
                        $quartarly_years = $project->quarter_years;
                        $old = old('quarters') ? : [];

                    @endphp
                    <div class="table-responsive">
                        <h4 class="card-title " style="text-align:center">Plan Quarter Repetition</h4>
                        @if ($errors->has('quarters'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('quarters') }}</strong>
                            </div>
                        @endif
                        @if ($errors->has('quarters.*'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('quarters.*') }}</strong>
                            </div>
                        @endif
                        <table class="table">
                            <thead class=" text-primary">
                                <tr>
                                    <th>Year</th>
                                    <th>Quaters</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($quartarly_years as $year => $quarters)
                                    <tr>
                                        <td>{!! $year !!}</td>
                                        <td>
                                            <div class="checkbox-radios">
                                                @foreach($quarters as $quarter)

                                                    <div class="form-check form-check-inline">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input quarterCheck" name="quarters[]" type="checkbox" value="{{ $quarter->id }}" {{ in_array($quarter->id, $old) ? 'checked' : '' }}>
                                                            Quarter {{ $loop->iteration }} 
                                                            <span class="form-check-sign" style="top: 5px;">
                                                                <span class="check"></span>
                                                            </span>
                                                            <p style="font-size: 13px;margin-bottom: 0;">From {{ date('M Y',strtotime($quarter->start_date)) }} To {{ date('M Y',strtotime($quarter->end_date)) }}</p>
                                                        </label>
                                                    </div>
                                                @endforeach

                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    @foreach($quartarly_years as $year => $quarters)
                        @foreach($quarters as $quarter)
                            <div class="row" id="measure{{ $quarter->id }}" {{ in_array($quarter->id, $old) ? '' : 'hidden' }}>
                                <label class="col-sm-2 col-form-label"> Quarter {{ $loop->iteration }} of {!! $year !!}</label>
                                <div class="col-sm-10">
                                    <div class="form-group bmd-form-group">
                                        <select class="selectpicker" data-width="100%" name="measures[{{ $quarter->id }}]" data-style="select-with-transition" title="Choose Measure" {{ in_array($quarter->id, $old) ? '' : 'required' }}>
                                            <option disabled="" value="">Choose Measure</option>
                                            <option value="Baseline" {{ old('measures.'.$quarter->id) == 'Baseline' ? 'selected' : '' }}>Baseline</option>
                                            <option value="Midline" {{ old('measures.'.$quarter->id) == 'Midline' ? 'selected' : '' }}>Midline</option>
                                            <option value="Endline" {{ old('measures.'.$quarter->id) == 'Endline' ? 'selected' : '' }}>Endeline</option>
                                        </select>
                                        @if ($errors->has('measures.'.$quarter->id))
                                            <div class="alert alert-danger no-border mb-2">
                                                <strong>{{ $errors->first('measures.'.$quarter->id) }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endforeach
                    <br>
                    <div class="category form-category">* Required fields</div>
                </div>
                <div class="card-footer text-right">
                
                    <button type="submit" class="btn btn-rose">Add</button>
                </div>
            </div>
        </form>
    </div>
    
</div>
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $('.quarterCheck').on('change',function(){
                var qid = $(this).attr('value');
                if(this.checked){
                    $('#measure'+qid).removeAttr('hidden');
                    $('#measure'+qid).find('select').attr('required','required');
                }else{
                    $('#measure'+qid).attr('hidden','hidden');
                    $('#measure'+qid).find('select').removeAttr('required');
                }

            });
            setFormValidation('#FormValidation');

        }); 
    </script>

@endsection