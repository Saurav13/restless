@extends('layouts.project')

@section('body')
<div class="row">
                        
                        
       <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-rose card-header-icon">

                <div class="card-icon">
                    <i class="material-icons">assignment</i>
                </div>
                <a href="/project/{{Request::segment(2)}}/mneactivities/create" class="btn btn-primary pull-right">Add M&E Activity</a>

                <h4 class="card-title">M&E Activities </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        @if(\App\Project::findOrFail(Request::segment(2))->status!='draft')
                            <th>Code</th>
                        @endif
                        <th>Description</th>
                        <th>M&E Category</th>
                        <th>Quarter</th>
                        <th>Measure</th>

                        <th class="text-right">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($mneactivities as $mneactivity)
                        <tr>
                            <td class="text-center">{{$loop->iteration}}</td>
                            @if(\App\Project::findOrFail(Request::segment(2))->status!='draft')
                                <td>{{$mneactivity->code}}</td>
                            @endif
                            <td>{{$mneactivity->description}}</td>
                            <td>{{$mneactivity->mne_category}}</td>
                            <td>{{$mneactivity->quarter ? $mneactivity->quarter->term : '-'}}</td>
                            <td>{{$mneactivity->measure}}</td>

                            {{-- <td>2 indicators <br> <a href="" class="btn btn-sm btn-warning">Manage Indicators</a></td> --}}
                            <td class="td-actions text-right">
                                {{-- <button type="button" rel="tooltip" class="btn btn-info" data-original-title="" title="">
                                    <i class="material-icons">person</i>
                                </button> --}}
                                <a href="/project/{{Request::segment(2)}}/mneactivities/{{$mneactivity->id}}/edit" rel="tooltip" class="btn btn-success" data-original-title="" title="">
                                    <i class="material-icons">edit</i>
                                </a>
                                <form action="/project/{{Request::segment(2)}}/mneactivities/{{$mneactivity->id}}" method="POST" style="display:inline">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE" >
                                    <button  id='deleteInfo{{ $mneactivity->id }}' type="button"  rel="tooltip" class="btn btn-danger" data-original-title="" title="">
                                            <i class="material-icons">close</i>
                                    </button>
                                </form>
                            
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
                {{$mneactivities->links()}}
            </div>
        </div>
    </div>
</div>


@endsection
