@extends('layouts.project')

@section('body')
<div class="row">
    
    <div class="col-md-12">
        
    <form action="/project/{{Request::segment(2)}}/mneactivities/{{$mneactivity->id}}" method="POST" novalidate="novalidate">
            @csrf
            <input type="text" hidden name="_method" value="PUT"/>
            <div class="card ">
                <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">mail_outline</i>
                    </div>
                    <h4 class="card-title">Edit M&E Activity</h4>
                </div>
                <div class="card-body ">
                   
                <div class="form-group bmd-form-group">
                    <label class="bmd-label-floating"> Details* </label>
                    <textarea name="description" class="form-control {{ $errors->has('description') ? ' has-danger' : '' }}" rows="5" required>{{$mneactivity->description}}</textarea>
                    @if ($errors->has('description'))
                        <div class="alert alert-danger no-border mb-2">
                            <strong>{{ $errors->first('description') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="form-group bmd-form-group">
                        {{-- <label class="bmd-label-floating"> Output </label> --}}

                  
                    <select required name="mne_category" class="selectpicker" data-style="select-with-transition"  title="Choose M&E Category *" data-width="100%">
                        @foreach(\App\MNECategory::all() as $cats)
                            <option value="{{$cats->title}}" {{$cats->title==$mneactivity->mne_category?'selected':''}}>{{$cats->title}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('mne_category'))
                        <div class="alert alert-danger no-border mb-2">
                            <strong>{{ $errors->first('mne_category') }}</strong>
                        </div>
                    @endif
                </div>
                
                <br>

                @php
                    $quartarly_years = $project->quarter_years;

                @endphp
                <div class="table-responsive">
                    <h4 class="card-title " style="text-align:center">Plan Quarter Repetition</h4>
                    @if ($errors->has('quarters'))
                        <div class="alert alert-danger no-border mb-2">
                            <strong>{{ $errors->first('quarters') }}</strong>
                        </div>
                    @endif
                    @if ($errors->has('quarters.*'))
                        <div class="alert alert-danger no-border mb-2">
                            <strong>{{ $errors->first('quarters.*') }}</strong>
                        </div>
                    @endif
                    <table class="table">
                        <thead class=" text-primary">
                            <tr>
                                <th>Year</th>
                                <th>Quaters</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($quartarly_years as $year => $quarters)
                                <tr>
                                    <td>{!! $year !!}</td>
                                    <td>
                                        <div class="checkbox-radios">
                                            @foreach($quarters as $quarter)

                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input quarterCheck" type="radio" name="quarter" value="{{ $quarter->id }}" {{ $quarter->id == $mneactivity->quarter_id ? 'checked' : '' }}>
                                                        Quarter {{ $loop->iteration }} 
                                                        <span class="circle" style="top: 5px;">
                                                            <span class="check"></span>
                                                        </span>
                                                        <p style="font-size: 13px;margin-bottom: 0;">From {{ date('M Y',strtotime($quarter->start_date)) }} To {{ date('M Y',strtotime($quarter->end_date)) }}</p>
                                                    </label>
                                                </div>
                                            @endforeach

                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @foreach($quartarly_years as $year => $quarters)
                    @foreach($quarters as $quarter)
                        <div class="row" id="measure{{ $quarter->id }}" {{ $quarter->id == $mneactivity->quarter_id ? '' : 'hidden' }}>
                            <label class="col-sm-2 col-form-label"> Quarter {{ $loop->iteration }} of {!! $year !!}</label>
                            <div class="col-sm-10">
                                <div class="form-group bmd-form-group">
                                    <select class="selectpicker" data-width="100%" name="measures[{{ $quarter->id }}]" data-style="select-with-transition" title="Choose Measure" {{ $quarter->id == $mneactivity->quarter_id ? '' : 'required' }}>
                                        <option disabled="" value="">Choose Measure</option>
                                        <option value="Baseline" {{ $mneactivity->measure == 'Baseline' ? 'selected' : '' }}>Baseline</option>
                                        <option value="Midline" {{ $mneactivity->measure == 'Midline' ? 'selected' : '' }}>Midline</option>
                                        <option value="Endline" {{ $mneactivity->measure == 'Endline' ? 'selected' : '' }}>Endeline</option>
                                    </select>
                                    @if ($errors->has('measures.'.$quarter->id))
                                        <div class="alert alert-danger no-border mb-2">
                                            <strong>{{ $errors->first('measures.'.$quarter->id) }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endforeach
                <br>
                
                <div class="category form-category">* Required fields</div>
                </div>
                <div class="card-footer text-right">
                
                    <button type="submit" class="btn btn-rose">Save</button>
                </div>
            </div>
        </form>
    </div>
    
</div>
@endsection


@section('js')
    <script>
        $(document).ready(function(){
            $('.quarterCheck').on('change',function(){
                var qid = $(this).attr('value');
                
                $("[id*='measure']").attr('hidden','hidden');
                $("[id*='measure']").find('select').removeAttr('required');

                if(this.checked){
                    $('#measure'+qid).removeAttr('hidden');
                    $('#measure'+qid).find('select').attr('required','required');
                }

            });
            setFormValidation('#FormValidation');

        }); 
    </script>

@endsection