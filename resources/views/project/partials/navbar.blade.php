
<body class="">
    <div class="wrapper ">
      <div class="sidebar" data-color="rose" data-background-color="black" data-image="../assets/img/sidebar-1.jpg">
        <!--
          Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"
  
          Tip 2: you can also add an image using data-image tag
      -->
        <div class="logo">
          <a href="http://www.creative-tim.com" class="simple-text logo-mini">
            CT
          </a>
          <a href="http://www.creative-tim.com" class="simple-text logo-normal">
            {{\App\Project::findOrFail(Request::segment(2))->title}}
          </a>
        </div>
        <div class="sidebar-wrapper">
          {{-- <div class="user">
            <div class="photo">
              <img src="../assets/img/faces/avatar.jpg" />
            </div>
            <div class="user-info">
              <a data-toggle="collapse" href="#collapseExample" class="username">
                <span>
                  Tania Andrew
                  <b class="caret"></b>
                </span>
              </a>
              <div class="collapse" id="collapseExample">
                <ul class="nav">
                  <li class="nav-item">
                    <a class="nav-link" href="#">
                      <span class="sidebar-mini"> MP </span>
                      <span class="sidebar-normal"> My Profile </span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">
                      <span class="sidebar-mini"> EP </span>
                      <span class="sidebar-normal"> Edit Profile </span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#">
                      <span class="sidebar-mini"> S </span>
                      <span class="sidebar-normal"> Settings </span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div> --}}
          <ul class="nav">
            <li class="nav-item">
              <a class="nav-link" href="../examples/dashboard.html">
                <i class="material-icons">dashboard</i>
                <p> Dashboard </p>
              </a>
            </li>
            <li class="nav-item ">
              <a class="nav-link" data-toggle="collapse" href="#componentsExamples">
                <i class="material-icons">settings_applications</i>
                <p>Project Setup
                  <b class="caret"></b>
                </p>
              </a>
              <div class="collapse" id="componentsExamples">
                <ul class="nav">
                  <li class="nav-item ">
                    <a class="nav-link" data-toggle="collapse" href="#projectCollapse">
                      <span class="sidebar-mini">  </span>
                      <span class="sidebar-normal" style="margin-left:3rem"> Project
                        <b class="caret"></b>
                      </span>
                    </a>
                    <div class="collapse" id="projectCollapse">
                      <ul class="nav">
                        <li class="nav-item ">
                          <a class="nav-link" href="/project/{{Request::segment(2)}}/indicators">
                            {{-- <span class="sidebar-mini"> E </span> --}}
                            <span class="sidebar-normal" style="margin-left:4rem"> Indicators </span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </li>

                  <li class="nav-item ">
                    <a class="nav-link" data-toggle="collapse" href="#outcomesCollapse">
                      <span class="sidebar-mini">  </span>
                      <span class="sidebar-normal" style="margin-left:3rem"> Outcomes
                        <b class="caret"></b>
                      </span>
                    </a>
                    <div class="collapse" id="outcomesCollapse">
                      <ul class="nav">
                        <li class="nav-item ">
                          <a class="nav-link" href="/project/{{Request::segment(2)}}/outcomes/create">
                            {{-- <span class="sidebar-mini"> E </span> --}}
                            <span class="sidebar-normal" style="margin-left:4rem"> Add Outcome </span>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="/project/{{Request::segment(2)}}/outcomes">
                            {{-- <span class="sidebar-mini"> E </span> --}}
                            <span class="sidebar-normal" style="margin-left:4rem"> List Outcomes </span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </li>
                  <li class="nav-item ">
                    <a class="nav-link" data-toggle="collapse" href="#outputsCollapse">
                      <span class="sidebar-mini">  </span>
                      <span class="sidebar-normal" style="margin-left:3rem"> Outputs
                        <b class="caret"></b>
                      </span>
                    </a>
                    <div class="collapse" id="outputsCollapse">
                      <ul class="nav">
                        <li class="nav-item ">
                           <a class="nav-link" href="/project/{{Request::segment(2)}}/outputs/create">
                            {{-- <span class="sidebar-mini"> E </span> --}}
                            <span class="sidebar-normal" style="margin-left:4rem"> Add Output </span>
                          </a>
                        </li>
                        <li>
                          <a class="nav-link" href="/project/{{Request::segment(2)}}/outputs">
                            {{-- <span class="sidebar-mini"> E </span> --}}
                            <span class="sidebar-normal" style="margin-left:4rem"> List Outputs </span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </li>

                  <li class="nav-item ">
                      <a class="nav-link" data-toggle="collapse" href="#activityCollapse">
                        <span class="sidebar-mini">  </span>
                        <span class="sidebar-normal" style="margin-left:3rem"> Activities
                          <b class="caret"></b>
                        </span>
                      </a>
                      <div class="collapse" id="activityCollapse">
                        <ul class="nav">
                          <li class="nav-item ">
                             <a class="nav-link" href="/project/{{Request::segment(2)}}/activities/create">
                              {{-- <span class="sidebar-mini"> E </span> --}}
                              <span class="sidebar-normal" style="margin-left:4rem"> Add Activitiy </span>
                            </a>
                          </li>
                          <li>
                            <a class="nav-link" href="/project/{{Request::segment(2)}}/activities">
                              {{-- <span class="sidebar-mini"> E </span> --}}
                              <span class="sidebar-normal" style="margin-left:4rem"> List Activities </span>
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="{{ route('activities.activity-planner',Request::segment(2)) }}">
                                {{-- <span class="sidebar-mini"> E </span> --}}
                                <span class="sidebar-normal" style="margin-left:4rem"> Activity Planner </span>
                              </a>
                          </li>
                        </ul>
                      </div>
                  </li>
                  <li class="nav-item ">
                      <a class="nav-link" data-toggle="collapse" href="#mneactivityCollapse">
                        <span class="sidebar-mini">  </span>
                        <span class="sidebar-normal" style="margin-left:3rem">M&E Activities
                          <b class="caret"></b>
                        </span>
                      </a>
                      <div class="collapse" id="mneactivityCollapse">
                        <ul class="nav">
                          <li class="nav-item ">
                             <a class="nav-link" href="/project/{{Request::segment(2)}}/mneactivities/create">
                              {{-- <span class="sidebar-mini"> E </span> --}}
                              <span class="sidebar-normal" style="margin-left:4rem"> Add M&E Activitiy </span>
                            </a>
                          </li>
                          <li>
                            <a class="nav-link" href="/project/{{Request::segment(2)}}/mneactivities">
                              {{-- <span class="sidebar-mini"> E </span> --}}
                              <span class="sidebar-normal" style="margin-left:4rem"> List M&E Activities </span>
                            </a>
                          </li>
                        </ul>
                      </div>
                  </li>
                     <li class="nav-item ">
                      <a class="nav-link" data-toggle="collapse" href="#financeCollapse">
                        <span class="sidebar-mini">  </span>
                        <span class="sidebar-normal" style="margin-left:3rem"> Finance
                          <b class="caret"></b>
                        </span>
                      </a>
                      <div class="collapse" id="financeCollapse">
                        <ul class="nav">
                          <li class="nav-item ">
                             <a class="nav-link" href="{{ route('finance.activity-budget-planner.index',Request::segment(2)) }}">
                              {{-- <span class="sidebar-mini"> E </span> --}}
                              <span class="sidebar-normal" style="margin-left:4rem"> Activity Budget Planner </span>
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="{{ route('finance.mne-budget-planner.index',Request::segment(2)) }}">
                              {{-- <span class="sidebar-mini"> E </span> --}}
                              <span class="sidebar-normal" style="margin-left:4rem"> M&E Budget Planner </span>
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="{{ route('finance.narrative-records.index',Request::segment(2)) }}">
                                {{-- <span class="sidebar-mini"> E </span> --}}
                                <span class="sidebar-normal" style="margin-left:4rem"> Narrative Records</span>
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="{{ route('finance.budget-docs.index',Request::segment(2)) }}">
                                {{-- <span class="sidebar-mini"> E </span> --}}
                                <span class="sidebar-normal" style="margin-left:4rem"> Budget Documents </span>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </li>
                    <li class="nav-item ">
                      <a class="nav-link" data-toggle="collapse" href="#projectDocCollapse">
                        <span class="sidebar-mini">  </span>
                        <span class="sidebar-normal" style="margin-left:3rem"> Project Documents
                          <b class="caret"></b>
                        </span>
                      </a>
                      <div class="collapse" id="projectDocCollapse">
                        <ul class="nav">
                          <li class="nav-item ">
                             <a class="nav-link" href="{{ route('docs.create',Request::segment(2)) }}">
                              {{-- <span class="sidebar-mini"> E </span> --}}
                              <span class="sidebar-normal" style="margin-left:4rem"> Add Documents </span>
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="{{ route('docs.index',Request::segment(2)) }}">
                              {{-- <span class="sidebar-mini"> E </span> --}}
                              <span class="sidebar-normal" style="margin-left:4rem"> List Documents </span>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </li>
                    <li class="nav-item ">
                      <a class="nav-link" data-toggle="collapse" href="#projectSettingsCollapse">
                        <span class="sidebar-mini">  </span>
                        <span class="sidebar-normal" style="margin-left:3rem"> Project Settings
                          <b class="caret"></b>
                        </span>
                      </a>
                      <div class="collapse" id="projectSettingsCollapse">
                        <ul class="nav">
                          <li class="nav-item ">
                             <a class="nav-link" href="/project/{{Request::segment(2)}}/geographicregions">
                              {{-- <span class="sidebar-mini"> E </span> --}}
                              <span class="sidebar-normal" style="margin-left:4rem"> Geographic Areas </span>
                            </a>
                          </li>
                          <li>
                            <a class="nav-link" href="/project/{{Request::segment(2)}}/beneficiaries">
                              {{-- <span class="sidebar-mini"> E </span> --}}
                              <span class="sidebar-normal" style="margin-left:4rem"> Reach Planner </span>
                            </a>
                          </li>
                          <li>
                            <a class="nav-link" href="/project/{{Request::segment(2)}}/quarters">
                                {{-- <span class="sidebar-mini"> E </span> --}}
                                <span class="sidebar-normal" style="margin-left:4rem"> Quarter Planner </span>
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="{{ route('settings.questions-planner.index',Request::segment(2)) }}">
                                {{-- <span class="sidebar-mini"> E </span> --}}
                                <span class="sidebar-normal" style="margin-left:4rem"> Questions Planner </span>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </li>
                
                </ul>

              </div>
            </li>
            <li class="nav-item ">
                <a class="nav-link" data-toggle="collapse" href="#operationCollapse">
                  <i class="material-icons">update</i>
                  <p>Operations
                    <b class="caret"></b>
                  </p>
                </a>
                <div class="collapse" id="operationCollapse">
                  <ul class="nav">
                   
                    <li class="nav-item ">
                      <a class="nav-link" data-toggle="collapse" href="#projectLevelCollapse">
                        <span class="sidebar-mini">  </span>
                        <span class="sidebar-normal" style="margin-left:3rem"> Project Entries
                          <b class="caret"></b>
                        </span>
                      </a>
                      <div class="collapse" id="projectLevelCollapse">
                        <ul class="nav">
                          <li class="nav-item ">
                            <a class="nav-link" href="{{ route('projects.indicator-evaluation.get',Request::segment(2)) }}">
                              {{-- <span class="sidebar-mini"> E </span> --}}
                              <span class="sidebar-normal" style="margin-left:4rem"> Indicator Evaluation</span>
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="{{ route('projects.progress-report.get',Request::segment(2)) }}">
                              {{-- <span class="sidebar-mini"> E </span> --}}
                              <span class="sidebar-normal" style="margin-left:4rem">Progress Report</span>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </li>
                    <li class="nav-item ">
                      <a class="nav-link" data-toggle="collapse" href="#outcomeLevelCollapse">
                        <span class="sidebar-mini">  </span>
                        <span class="sidebar-normal" style="margin-left:3rem"> Outcome Entries
                          <b class="caret"></b>
                        </span>
                      </a>
                      <div class="collapse" id="outcomeLevelCollapse">
                        <ul class="nav">
                          <li class="nav-item ">
                             <a class="nav-link" href="{{ route('outcomes.indicator-evaluation.index',Request::segment(2)) }}">
                              {{-- <span class="sidebar-mini"> E </span> --}}
                              <span class="sidebar-normal" style="margin-left:4rem"> Indicator Evaluation </span>
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="{{ route('outcomes.progress-report.index',Request::segment(2)) }}">
                              {{-- <span class="sidebar-mini"> E </span> --}}
                              <span class="sidebar-normal" style="margin-left:4rem"> Progress Report </span>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </li>
  
                    <li class="nav-item ">
                        <a class="nav-link" data-toggle="collapse" href="#outputLevelCollapse">
                          <span class="sidebar-mini">  </span>
                          <span class="sidebar-normal" style="margin-left:3rem"> Output Entries
                            <b class="caret"></b>
                          </span>
                        </a>
                        <div class="collapse" id="outputLevelCollapse">
                          <ul class="nav">
                            <li class="nav-item ">
                               <a class="nav-link" href="{{ route('outputs.indicator-evaluation.index',Request::segment(2)) }}">
                                {{-- <span class="sidebar-mini"> E </span> --}}
                                <span class="sidebar-normal" style="margin-left:4rem"> Indicator Evaluation </span>
                              </a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="{{ route('outputs.progress-report.index',Request::segment(2)) }}">
                                {{-- <span class="sidebar-mini"> E </span> --}}
                                <span class="sidebar-normal" style="margin-left:4rem"> Progress Report </span>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </li>


                      <li class="nav-item ">
                        <a class="nav-link"  href="{{ route('activities.indicator-evaluation.index',Request::segment(2)) }}">
                          <span class="sidebar-mini">  </span>
                          <span class="sidebar-normal" style="margin-left:3rem"> Activities Entry
                            {{-- <b class="caret"></b> --}}
                          </span>
                        </a>
                        {{-- <div class="collapse" id="activityLevelCollapse">
                          <ul class="nav">
                            <li class="nav-item ">
                               <a class="nav-link" href="{{ route('activities.indicator-evaluation.index',Request::segment(2)) }}">
                                
                                <span class="sidebar-normal" style="margin-left:4rem"> Indicator Evaluation </span>
                              </a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="{{ route('outputs.progress-report.index',Request::segment(2)) }}">
                                
                                <span class="sidebar-normal" style="margin-left:4rem"> Progress Report </span>
                              </a>
                            </li>
                          </ul>
                        </div> --}}
                        
                      </li>
                  
                  
                  </ul>
                </div>
            </li>
            <li class="nav-item ">
                <a class="nav-link" data-toggle="collapse" href="#operationCollapse">
                  <i class="material-icons">update</i>
                  <p>Reports
                    <b class="caret"></b>
                  </p>
                </a>
                <div class="collapse" id="operationCollapse">
                  <ul class="nav">
                   
                    <li class="nav-item ">
                      <a class="nav-link" data-toggle="collapse" href="#projectLevelCollapse">
                        <span class="sidebar-mini">  </span>
                        <span class="sidebar-normal" style="margin-left:3rem"> Project Entries
                          <b class="caret"></b>
                        </span>
                      </a>
                      <div class="collapse" id="projectLevelCollapse">
                        <ul class="nav">
                          <li class="nav-item ">
                            <a class="nav-link" href="#0">
                              {{-- <span class="sidebar-mini"> E </span> --}}
                              <span class="sidebar-normal" style="margin-left:4rem"> Indicator Evaluation </span>
                            </a>
                            <a class="nav-link" href="#0">
                              {{-- <span class="sidebar-mini"> E </span> --}}
                              <span class="sidebar-normal" style="margin-left:4rem">Progress Report</span>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </li>
                    <li class="nav-item ">
                      <a class="nav-link" data-toggle="collapse" href="#outcomeLevelCollapse">
                        <span class="sidebar-mini">  </span>
                        <span class="sidebar-normal" style="margin-left:3rem"> Outcome Entries
                          <b class="caret"></b>
                        </span>
                      </a>
                      <div class="collapse" id="outcomeLevelCollapse">
                        <ul class="nav">
                          <li class="nav-item ">
                             <a class="nav-link" href="#0">
                              {{-- <span class="sidebar-mini"> E </span> --}}
                              <span class="sidebar-normal" style="margin-left:4rem"> Indicator Evaluation </span>
                            </a>
                            <a class="nav-link" href="#0">
                              {{-- <span class="sidebar-mini"> E </span> --}}
                              <span class="sidebar-normal" style="margin-left:4rem"> Progress Report </span>
                            </a>
                          </li>
                        </ul>
                      </div>
                    </li>
  
                    <li class="nav-item ">
                        <a class="nav-link" data-toggle="collapse" href="#outputLevelCollapse">
                          <span class="sidebar-mini">  </span>
                          <span class="sidebar-normal" style="margin-left:3rem"> Output Entries
                            <b class="caret"></b>
                          </span>
                        </a>
                        <div class="collapse" id="outputLevelCollapse">
                          <ul class="nav">
                            <li class="nav-item ">
                               <a class="nav-link" href="#0">
                                {{-- <span class="sidebar-mini"> E </span> --}}
                                <span class="sidebar-normal" style="margin-left:4rem"> Indicator Evaluation </span>
                              </a>
                              <a class="nav-link" href="#0">
                                {{-- <span class="sidebar-mini"> E </span> --}}
                                <span class="sidebar-normal" style="margin-left:4rem"> Progress Report </span>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </li>


                      <li class="nav-item ">
                        <a class="nav-link"  href="#activityLevelCollapse">
                          <span class="sidebar-mini">  </span>
                          <span class="sidebar-normal" style="margin-left:3rem"> Activities Entry
                            {{-- <b class="caret"></b> --}}
                          </span>
                        </a>
                        
                      </li>
                  
                  
                  </ul>
                </div>
            </li>
          
          </ul>
        </div>
      </div>
      <div class="main-panel">
        <!-- Navbar -->
        <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
          <div class="container-fluid">
            <div class="navbar-wrapper">
              <div class="navbar-minimize">
                <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
                  <i class="material-icons text_align-center visible-on-sidebar-regular">more_vert</i>
                  <i class="material-icons design_bullet-list-67 visible-on-sidebar-mini">view_list</i>
                </button>
              </div>
              <a class="navbar-brand" href="#pablo">{{\App\Project::findOrFail(Request::segment(2))->title}} </a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
              <span class="sr-only">Toggle navigation</span>
              <span class="navbar-toggler-icon icon-bar"></span>
              <span class="navbar-toggler-icon icon-bar"></span>
              <span class="navbar-toggler-icon icon-bar"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end">
              <form class="navbar-form">
                <div class="input-group no-border">
                  <input type="text" value="" class="form-control" placeholder="Search...">
                  <button type="submit" class="btn btn-white btn-round btn-just-icon">
                    <i class="material-icons">search</i>
                    <div class="ripple-container"></div>
                  </button>
                </div>
              </form>
              <ul class="navbar-nav">
                <li class="nav-item">
                  <a class="nav-link" href="#pablo">
                    <i class="material-icons">dashboard</i>
                    <p class="d-lg-none d-md-block">
                      Stats
                    </p>
                  </a>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="material-icons">notifications</i>
                    <span class="notification">5</span>
                    <p class="d-lg-none d-md-block">
                      Some Actions
                    </p>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="#">Mike John responded to your email</a>
                    <a class="dropdown-item" href="#">You have 5 new tasks</a>
                    <a class="dropdown-item" href="#">You're now friend with Andrew</a>
                    <a class="dropdown-item" href="#">Another Notification</a>
                    <a class="dropdown-item" href="#">Another One</a>
                  </div>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="material-icons">person</i>
                    <p class="d-lg-none d-md-block">
                      Account
                    </p>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                    <a class="dropdown-item" href="#">Profile</a>
                    <a class="dropdown-item" href="#">Settings</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Log out</a>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <!-- End Navbar -->
        <div class="content">
          <div class="content">
            <div class="container-fluid">