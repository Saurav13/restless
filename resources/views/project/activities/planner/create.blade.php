@extends('layouts.project')

@section('body')

<div class="row">
                        
                        
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-rose card-header-icon">

                <div class="card-icon">
                    <i class="material-icons">chrome_reader_mode</i>
                </div>

                <h4 class="card-title">Planner for {{ $activity->title }}</h4>
            </div>
            <div class="card-body">

                <form action="{{ route('activities.activity-planner.store',[Request::segment(2),$activity->id]) }}" id="FormValidation" novalidate method="POST">
                    @csrf 
                    @php
                        $quartarly_years = $project->quarter_years;
                        $old = old('quarters') ? : [];
                    @endphp
    
                    <div class="form-group bmd-form-group">
                        <select class="selectpicker" data-width="100%" name="geographic_region" id="selectGeographicRegion" data-style="select-with-transition" title="Choose Geographic Region" required>
                            <option disabled="" value="" {{ !old('geographic_region') ? 'selected' : '' }}>Choose Geographic Region</option>
                            @foreach($geographic_regions as $region)
                                <option value="{{ $region->id }}" {{ old('geographic_region') == $region->id ? 'selected' : '' }}>Province {{ $region->province }} - {{ $region->district }} - Ward {{ $region->ward }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('geographic_region'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('geographic_region') }}</strong>
                            </div>
                        @endif
                    </div>
                    <br>
                    <div id="hiddenDiv" {{ $errors->count() > 0 ? '' : 'hidden' }}>

                        <div class="form-group bmd-form-group">
                            <label for="title" class="bmd-label-floating"> Title *</label>
                            <input type="text" name="title" class="form-control" id="title" required="true" value="{{ old('title') ? : $activity->title }}">
                            @if ($errors->has('title'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="form-group bmd-form-group">
                            <label class="bmd-label-floating"> Description* </label>
                            <textarea name="description" class="form-control" rows="5" required>{{ old('description') ? : $activity->description }}</textarea>
                            @if ($errors->has('description'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="form-group bmd-form-group">
                            <label class="bmd-label-floating"> Objective* </label>
                            <textarea name="objective" class="form-control" rows="5" required>{{ old('objective') ? : $activity->objective }}</textarea>
                            @if ($errors->has('objective'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('objective') }}</strong>
                                </div>
                            @endif
                        </div>

                        <div class="form-group bmd-form-group">
                            <label class="bmd-label-floating"> Approach* </label>
                            <textarea name="approach" class="form-control" rows="5" required>{{ old('approach') }}</textarea>
                            @if ($errors->has('approach'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('approach') }}</strong>
                                </div>
                            @endif
                        </div>

                        @php
                            $oldPartners = old('partners') ? : [];
                        @endphp
                        <div class="form-group bmd-form-group">
                            <select class="selectpicker" multiple data-width="100%" name="partners[]" data-style="select-with-transition" title="Choose Partners" required>
                                <option disabled="" value="" >Choose Partners</option>
                                @foreach($partners as $partner)
                                    <option value="{{ $partner->id }}" {{ in_array($partner->id,$oldPartners) ? 'selected' : '' }}>{{ $partner->fullname }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('partners'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('partners') }}</strong>
                                </div>
                            @endif
                        </div>
                        
                        <div class="form-group bmd-form-group">
                            <select class="selectpicker" data-width="100%" name="participant_target_type" data-style="select-with-transition" title="Choose Participant Target Type" required>
                                <option disabled="" value="" {{ !old('participant_target_type') ? 'selected' : '' }}>Choose Participant Target Type</option>
                                <option value="Limited" {{ old('participant_target_type') == 'Limited' ? 'selected' : '' }}>Limited</option>
                                <option value="Mass" {{ old('participant_target_type') == 'Mass' ? 'selected' : '' }}>Mass</option>
                            </select>
                            @if ($errors->has('participant_target_type'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('participant_target_type') }}</strong>
                                </div>
                            @endif
                        </div>
                        <br>
                        <hr>
                        <div class="table-responsive">
                            <h4 class="card-title " style="text-align:center">Plan Quarter Repetition</h4>
                            @if ($errors->has('quarters'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('quarters') }}</strong>
                                </div>
                            @endif
                            @if ($errors->has('quarters.*'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('quarters.*') }}</strong>
                                </div>
                            @endif
                            
                            <table class="table">
                                <thead class=" text-primary">
                                    <tr>
                                        <th>Year</th>
                                        <th>Quaters</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($quartarly_years as $year => $quarters)
                                        <tr>
                                            <td>{!! $year !!}</td>
                                            <td>
                                                <div class="checkbox-radios">
                                                    @foreach($quarters as $quarter)

                                                        <div class="form-check form-check-inline">
                                                            <label class="form-check-label">
                                                                <input class="form-check-input" type="checkbox" name="quarters[]" {{ in_array($quarter->id, $old) ? 'checked' : '' }} value="{{ $quarter->id }}">
                                                                Quarter {{ $loop->iteration }} 
                                                                <span class="form-check-sign" style="top: 5px;">
                                                                    <span class="check"></span>
                                                                </span>
                                                                <p style="font-size: 13px;margin-bottom: 0;">From {{ date('M Y',strtotime($quarter->start_date)) }} To {{ date('M Y',strtotime($quarter->end_date)) }}</p>
                                                            </label>
                                                        </div>
                                                    @endforeach

                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-success">Save</button>
                            <a href="{{ route('activities.activity-planner.index',[Request::segment(2),$activity->id]) }}" class="btn btn-neutral">Cancel</a>
                            
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

    <script>
        $(document).ready(function(){
            var title = '{{ $activity->title }}';

            setFormValidation('#FormValidation');

            $('#selectGeographicRegion').on('change', function(){
                $('#hiddenDiv').removeAttr('hidden');
                $("input[name='title']").val(title+' - '+$( "#selectGeographicRegion option:selected" ).text());

            })
        });
    </script>
@endsection