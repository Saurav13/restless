@extends('layouts.project')

@section('body')

<div class="row">
                        
                        
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-rose card-header-icon">

                <div class="card-icon">
                    <i class="material-icons">chrome_reader_mode</i>
                </div>

                <h4 class="card-title">Activity Planner </h4>
            </div>
            <div class="card-body">
                <form action="" method="GET">
                    <select class="selectpicker"  id="selectActivity" data-style="btn btn-primary btn-round" title="Select an Activity">
                        <option disabled {{ !isset($activity) ? 'selected' : '' }}>Select an Activity</option>
                        @foreach ($activities as $act)
                            <option value="{{ $act->id }}" {{ isset($activity) && $activity->id == $act->id ? 'selected' : '' }}>{{ $act->title }}</option>
                        @endforeach
                    </select>
                </form>

                @isset($activity)
                    <a href="{{ route('activities.activity-planner.create',[Request::segment(2),$activity->id]) }}" class="btn btn-primary pull-right">Add Activity Occurence</a>

                    <div class="table-responsive">
                        <table class="table">
                            <thead class=" text-primary">
                                <tr>
                                    <th class="text-center">#</th>
                                    @if($project->status!='draft')
                                        <th>Code</th>
                                    @endif
                                    <th>Title</th>
                                    <th width="30%">Description</th>
                                    <th>Project Quarter</th>
                                    <th>Created By</th>
                                    <th class="text-right">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($activity_instances as $instance)
                                    <tr>
                                        <td class="text-center">{{$loop->iteration}}</td>
                                        @if($project->status!='draft')
                                            <td>{{$instance->code}}</td>
                                        @endif
                                        <td>{{$instance->title}}</td>
                                        <td>{{$instance->description}}</td>
                                        <td>{{ $instance->quarter->term }}</td>
                                        <td>{{$instance->creator->name}}</td>
                                        
                                        <td class="td-actions text-right">
                                        
                                            <a href="{{ route('activities.activity-planner.edit',[Request::segment(2),$activity->id,$instance->id]) }}" rel="tooltip" class="btn btn-success" data-original-title="" title="">
                                                <i class="material-icons">edit</i>
                                            </a>
                                            <form action="{{ route('activities.activity-planner.destroy',[Request::segment(2),$activity->id,$instance->id]) }}" method="POST" style="display:inline">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="DELETE" >
                                                <button id='deleteInfo{{ $instance->id }}' type="button"  rel="tooltip" class="btn btn-danger" data-original-title="" title="">
                                                        <i class="material-icons">close</i>
                                                </button>
                                            </form>
                                    
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $activity_instances->links() }}
                @endisset

            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    
    <script>
        $(document).ready(function(){
            $('button[type="submit"]').removeAttr('hidden');
            
            $('#selectActivity').on('change', function(){
                location.href = '/project/{{Request::segment(2)}}/activities/'+$(this).val()+'/activity-planner';
            })
        });
    </script>
@endsection