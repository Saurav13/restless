@extends('layouts.project')

@section('body')
<div class="row">
                        
                        
       <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-rose card-header-icon">

                <div class="card-icon">
                    <i class="material-icons">assignment</i>
                </div>
                <a href="/project/{{Request::segment(2)}}/activities/create" class="btn btn-primary pull-right">Add Activity</a>

                <h4 class="card-title">Activitys </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        @if(\App\Project::findOrFail(Request::segment(2))->status!='draft')
                            <th>Code</th>
                        @endif
                        <th>Title</th>
                        <th>Description</th>
                        <th>Output</th>
                        <th>Outcome</th>

                        <th>Activity Plans</th>

                        <th class="text-right">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($activities as $activity)
                        <tr>
                            <td class="text-center">{{$loop->iteration}}</td>
                                @if(\App\Project::findOrFail(Request::segment(2))->status!='draft')
                                    <td>{{$activity->code}}</td>
                                @endif
                                <td>{{$activity->title}}</td>
                                <td>{{$activity->description}}</td>
                                <td>{{$activity->output->title}}</td>
                                <td>{{$activity->output->outcome->title}}</td>

                                <td> <a href="{{ route('activities.activity-planner.index',[Request::segment(2),$activity->id]) }}" class="btn btn-sm btn-warning">Plan Activities</a></td>
                                <td class="td-actions text-right">
                                <button type="button" rel="tooltip" class="btn btn-info" data-original-title="" title="">
                                    <i class="material-icons">person</i>
                                </button>
                                <a href="/project/{{Request::segment(2)}}/activities/{{$activity->id}}/edit" rel="tooltip" class="btn btn-success" data-original-title="" title="">
                                    <i class="material-icons">edit</i>
                                </a>
                                <form action="/project/{{Request::segment(2)}}/activities/{{$activity->id}}" method="POST" style="display:inline">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE" >
                                    <button  id='deleteInfo{{ $activity->id }}' type="button"  rel="tooltip" class="btn btn-danger" data-original-title="" title="">
                                            <i class="material-icons">close</i>
                                    </button>
                                </form>
                            
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
                {{$activities->links()}}
            </div>
        </div>
    </div>
</div>


@endsection
