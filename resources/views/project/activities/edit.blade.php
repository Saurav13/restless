@extends('layouts.project')

@section('body')
<div class="row">
    
    <div class="col-md-12">
        
    <form action="/project/{{Request::segment(2)}}/activities/{{$activity->id}}" method="POST" novalidate="novalidate">
            @csrf
            <input type="text" hidden name="_method" value="PUT"/>

            <div class="card ">
                <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">mail_outline</i>
                    </div>
                    <h4 class="card-title">Edit Activity</h4>
                </div>
                <div class="card-body ">
                    <div class="form-group bmd-form-group">
                        <label for="exampleEmail" class="bmd-label-floating"> Title *</label>
                        <input type="text" name="title" class="form-control {{ $errors->has('title') ? ' has-danger' : '' }}" id="exampleEmail" required="true" value="{{ $activity->title }}" aria-required="true">
                    </div>

                    @if ($errors->has('title'))
                        <div class="alert alert-danger no-border mb-2">
                            <strong>{{ $errors->first('title') }}</strong>
                        </div>
                    @endif
                <div class="form-group bmd-form-group">
                    <label class="bmd-label-floating"> Description* </label>
                    <textarea name="description" class="form-control {{ $errors->has('description') ? ' has-danger' : '' }}" value="" rows="5">{{ $activity->description  }}</textarea>
                    @if ($errors->has('description'))
                        <div class="alert alert-danger no-border mb-2">
                            <strong>{{ $errors->first('description') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="form-group bmd-form-group">
                    <label class="bmd-label-floating"> Objective </label>
                    <textarea name="objective" class="form-control {{ $errors->has('objective') ? ' has-danger' : '' }}" rows="5">{{ $activity->objective  }}</textarea>
                    @if ($errors->has('objective'))
                        <div class="alert alert-danger no-border mb-2">
                            <strong>{{ $errors->first('objective') }}</strong>
                        </div>
                    @endif
                </div>
           
                <div class="form-group bmd-form-group">
                        {{-- <label class="bmd-label-floating"> Output </label> --}}

                    
                </div>
                
                <div class="category form-category">* Required fields</div>
                </div>
                <div class="card-footer text-right">
                
                    <button type="submit" class="btn btn-rose">Save</button>
                </div>
            </div>
        </form>
    </div>
    
</div>
@endsection



