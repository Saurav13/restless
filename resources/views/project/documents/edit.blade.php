@extends('layouts.project')

@section('body')
<div class="row">
    
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header card-header-rose card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">attach_file</i>
                </div>
                <h4 class="card-title">Edit Project Document</h4>
            </div>
            <div class="card-body ">
                <form id="FormValidation" action="{{ route('docs.update',[Request::segment(2),$project_doc->id]) }}" method="POST" enctype="multipart/form-data" novalidate="novalidate">
                    @csrf
                    <input type="text" hidden name="_method" value="PUT"/>
                    
                    <div class="form-group bmd-form-group">
                        <label for="title" class="bmd-label-floating"> Title *</label>
                        <input type="text" name="title" class="form-control {{ $errors->has('title') ? ' has-danger' : '' }}" id="title" required="true" value="{{ $project_doc->title }}">

                        @if ($errors->has('title'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('title') }}</strong>
                            </div>
                        @endif
                    </div>
                    
                    <div class="row">
                        <div class="form-group col-md-12 col-sm-12">
                            {{-- Current File <br> --}}
                        
                            <label>File*</label><br>
                            <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                              
                                <div class="fileinput-preview fileinput-exists thumbnail" style="display:block"><a href="{{ route('docs.show',[Request::segment(2),$project_doc->id]) }}" target="_blank">{{ $project_doc->file }}</div>
                                <div>
                                    <span class="btn btn-rose btn-round btn-file">
                                        <span class="fileinput-new">Change</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" name="file" style="z-index: 2;"/>

                                    </span>
                                </div>
                            </div><br>
                            @if ($errors->has('file'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('file') }}</strong>
                                </div>
                            @endif
                        </div>
                        
                        
                    </div>
                    <div class="modal-footer">
                    
                        <button type="submit" class="btn btn-success">Save</button>
                        <a href="{{ route('docs.index',Request::segment(2)) }}" type="button" class="btn btn-neutral">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
</div>
@endsection

@section('js')

    <script>
        $(document).ready(function(){
            setFormValidation('#FormValidation');
  
        });
    </script>
@endsection