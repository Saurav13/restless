@extends('layouts.project')

@section('body')
<div class="row">
                        
                        
    <div class="col-md-12">
        
        <div class="card">
            <div class="card-header card-header-rose card-header-icon">

                <div class="card-icon">
                    <i class="material-icons">attach_file</i>
                </div>
                <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#addRecord">Upload Document</button>

                <h4 class="card-title">Project Documents </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class=" text-primary">
                            <tr>
                                <th class="text-center">#</th>
                                
                                <th>Title</th>
                                <th>File</th>
                                <th>Uploaded By</th>

                                <th class="text-right">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($project_docs as $project_doc)
                                <tr>
                                    <td class="text-center">{{$loop->iteration}}</td>
                                    
                                    <td>{{$project_doc->title}}</td>

                                    <td><a href="{{ route('docs.show',[Request::segment(2),$project_doc->id]) }}" target="_blank" rel="tooltip" title="View File"><i class="material-icons">file_copy</i></a></td>
                                    <td>{{ $project_doc->uploader->name }}</td>
                                    <td class="td-actions text-right">
                                        
                                        <a href="{{ route('docs.edit',[Request::segment(2),$project_doc->id]) }}" rel="tooltip" class="btn btn-success" data-original-title="" title="Edit">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        <form action="{{ route('docs.destroy',[Request::segment(2),$project_doc->id]) }}" method="POST" style="display:inline">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE" >
                                            <button id='deleteInfo{{ $project_doc->id }}' type="button"  rel="tooltip" class="btn btn-danger" data-original-title="" title="Delete">
                                                    <i class="material-icons">close</i>
                                            </button>
                                        </form>
                                
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $project_docs->links() }}
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addRecord" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Upload Project Document</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <i class="material-icons">clear</i>
                </button>
            </div>
            <div class="modal-body">
                <form id="FormValidation" action="{{ route('docs.store',Request::segment(2)) }}" method="POST" enctype="multipart/form-data" novalidate>
                    @csrf
                   
                    <div class="form-group bmd-form-group">
                        <label for="title" class="bmd-label-floating"> Title *</label>
                        <input type="text" name="title" class="form-control {{ $errors->has('title') ? ' has-danger' : '' }}" id="title" required="true" value="{{ old('title') }}">

                        @if ($errors->has('title'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('title') }}</strong>
                            </div>
                        @endif
                    </div>
                    
                    <div class="row">
                        <div class="form-group col-md-12 col-sm-12">
                            <label>File*</label><br>
                            <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                              
                                <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                <div>
                                    <span class="btn btn-rose btn-round btn-file">
                                        <span class="fileinput-new">Select File</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" name="file" style="z-index: 2;" required/>

                                    </span>
                                </div>
                            </div><br>
                            @if ($errors->has('file'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('file') }}</strong>
                                </div>
                            @endif
                        </div>
                        
                        
                    </div>
                    <div class="modal-footer" style="padding: 0;">
                    
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection

@section('js')
    @if($errors->count() > 0)
        <script>
            $('#addRecord').modal('show');
        </script>
    @endif
    <script>
        $(document).ready(function(){
            setFormValidation('#FormValidation');
        });
    </script>
@endsection
