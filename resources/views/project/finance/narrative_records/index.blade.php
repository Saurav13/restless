@extends('layouts.project')

@section('body')
<div class="row">
                        
                        
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-rose card-header-icon">

                <div class="card-icon">
                    <i class="material-icons">assignment</i>
                </div>
                <a href="#" id="addMNERecord" class="btn btn-primary pull-right">Add Narrative Record</a>

                <h4 class="card-title">M&E Activities Narrative Records </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class=" text-primary">
                            <tr>
                                <th class="text-center">#</th>
                                
                                <th>Title</th>
                                <th>Description</th>
                                <th>Year</th>

                                <th class="text-right">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($mne_narrative_records as $narrative_record)
                                <tr>
                                    <td class="text-center">{{$loop->iteration}}</td>
                                    
                                    <td>{{$narrative_record->title}}</td>
                                    <td>{{$narrative_record->description}}</td>
                                    <td>{{$narrative_record->year}}</td>
                                    <td class="td-actions text-right">
                                        
                                        <a href="{{ route('finance.narrative-records.edit',[Request::segment(2),$narrative_record->id]) }}" rel="tooltip" class="btn btn-success" data-original-title="" title="">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        <form action="{{ route('finance.narrative-records.destroy',[Request::segment(2),$narrative_record->id]) }}" method="POST" style="display:inline">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE" >
                                            <button id='deleteInfo{{ $narrative_record->id }}' type="button"  rel="tooltip" class="btn btn-danger" data-original-title="" title="">
                                                    <i class="material-icons">close</i>
                                            </button>
                                        </form>
                                
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{$mne_narrative_records->appends(array_except(Request::query(), 'mne'))->links()}}
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-rose card-header-icon">

                <div class="card-icon">
                    <i class="material-icons">assignment</i>
                </div>
                <a href="#" id="addProjectRecord" class="btn btn-primary pull-right">Add Narrative Record</a>

                <h4 class="card-title">Project Activities Narrative Records </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class=" text-primary">
                            <tr>
                                <th class="text-center">#</th>
                                
                                <th>Title</th>
                                <th>Description</th>
                                <th>Year</th>
                                <th class="text-right">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($project_narrative_records as $narrative_record)
                                <tr>
                                    <td class="text-center">{{$loop->iteration}}</td>
                                    
                                    <td>{{$narrative_record->title}}</td>
                                    <td>{{$narrative_record->description}}</td>
                                    <td>{{ $narrative_record->year }}</td>
                                    <td class="td-actions text-right">
                                    
                                        <a href="{{ route('finance.narrative-records.edit',[Request::segment(2),$narrative_record->id]) }}" rel="tooltip" class="btn btn-success" data-original-title="" title="">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        <form action="{{ route('finance.narrative-records.destroy',[Request::segment(2),$narrative_record->id]) }}" method="POST" style="display:inline">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE" >
                                            <button id='deleteInfo{{ $narrative_record->id }}' type="button"  rel="tooltip" class="btn btn-danger" data-original-title="" title="">
                                                    <i class="material-icons">close</i>
                                            </button>
                                        </form>
                                
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                {{$project_narrative_records->appends(array_except(Request::query(), 'project'))->links()}}
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addRecord" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Narrative Record</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <i class="material-icons">clear</i>
                </button>
            </div>
            <div class="modal-body">
                <form id="RecordValidation" action="{{ route('finance.narrative-records.store',Request::segment(2)) }}" method="POST" novalidate="novalidate">
                    @csrf
                    <input type="text" hidden name="type" value="{{ old('type') }}"/>
                    @if ($errors->has('type'))
                        <div class="alert alert-danger no-border mb-2">
                            <strong>{{ $errors->first('type') }}</strong>
                        </div>
                    @endif
                    <div class="form-group bmd-form-group">
                        <label for="title" class="bmd-label-floating"> Title *</label>
                        <input type="text" name="title" class="form-control {{ $errors->has('title') ? ' has-danger' : '' }}" id="title" required="true" value="{{ old('title') }}">

                        @if ($errors->has('title'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('title') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="form-group bmd-form-group">
                        <label class="bmd-label-floating"> Year (Leave empty if record is for entire project duration)</label><br>    
                        <select class="selectpicker" name="year" data-style="select-with-transition" title="Choose Year" data-width="100%">
                            <option value="" {{ !old('year') ? 'selected' : '' }}>Choose Year</option>
                            @foreach($years as $year=>$text)
                                <option value="{{ $year }}" {{ old('year') == $year ? 'selected' : '' }}>{{ $text }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('year'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('year') }}</strong>
                            </div>
                        @endif
                    </div>
                    <br>
                    <div class="form-group bmd-form-group">
                        <label class="bmd-label-floating"> Description* </label>
                        <textarea name="description" class="form-control {{ $errors->has('description') ? ' has-danger' : '' }}"  rows="5" required>{{ old('description') }}</textarea>
                        @if ($errors->has('description'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('description') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="modal-footer" style="padding: 0;">
                    
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection

@section('js')
    @if($errors->count() > 0)
        <script>
            $('#addRecord').modal('show');
        </script>
    @endif

    <script>
        $(document).ready(function(){
            $('#addMNERecord').click(function(){
                $("#addRecord input[name='type']").val('MNE');

                $('#addRecord').modal('show');
            })

            $('#addProjectRecord').click(function(){
                $("#addRecord input[name='type']").val('Project');

                $('#addRecord').modal('show');
            })

            setFormValidation('#RecordValidation');
  
        });
    </script>
@endsection
