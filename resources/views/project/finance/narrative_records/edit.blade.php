@extends('layouts.project')

@section('body')
<div class="row">
    
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header card-header-rose card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">assignment</i>
                </div>
                <h4 class="card-title">Edit Narrative Record</h4>
            </div>
            <div class="card-body ">
                <form id="RecordValidation" action="{{ route('finance.narrative-records.update',[Request::segment(2),$narrative_record->id]) }}" method="POST" novalidate="novalidate">
                    @csrf
                    <input type="text" hidden name="_method" value="PUT"/>
                    
                    <div class="form-group bmd-form-group">
                        <label for="title" class="bmd-label-floating"> Title *</label>
                        <input type="text" name="title" class="form-control {{ $errors->has('title') ? ' has-danger' : '' }}" id="title" required="true" value="{{ $narrative_record->title }}">

                        @if ($errors->has('title'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('title') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="form-group bmd-form-group">
                        <label class="bmd-label-floating"> Year (Leave empty if record is for entire project duration)</label><br>    
                        <select class="selectpicker" name="year" data-style="select-with-transition" title="Choose Year" data-width="100%">
                            <option value="" {{ !$narrative_record->year ? 'selected' : '' }}>Choose Year</option>
                            @foreach($years as $year=>$text)
                                <option value="{{ $year }}" {{ $narrative_record->year == $year ? 'selected' : '' }}>{{ $text }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('year'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('year') }}</strong>
                            </div>
                        @endif
                    </div>
                    <br>
                    <div class="form-group bmd-form-group">
                        <label class="bmd-label-floating"> Description* </label>
                        <textarea name="description" class="form-control {{ $errors->has('description') ? ' has-danger' : '' }}"  rows="5" required>{{ $narrative_record->description }}</textarea>
                        @if ($errors->has('description'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('description') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="modal-footer">
                    
                        <button type="submit" class="btn btn-success">Save</button>
                        <a href="{{ route('finance.narrative-records.index',Request::segment(2)) }}" type="button" class="btn btn-neutral">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
</div>
@endsection

@section('js')

    <script>
        $(document).ready(function(){
            setFormValidation('#RecordValidation');
  
        });
    </script>
@endsection