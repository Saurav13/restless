@if ($instances->count() > 0)
    <div class="card-description" style="color: #4caf50;">
        One or more activities are planned to be conducted in this quarter.
    </div>
@else
    <div class="card-description" style="color: #f44336;">
        No activities are planned for this quarter.
    </div>

@endif