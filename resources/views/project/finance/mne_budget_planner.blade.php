@extends('layouts.project')

@section('body')
<style>
    input[type='number']::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
        font-weight: 600;
        opacity: 1; /* Firefox */
    }

    input[type='number']::-ms-input-placeholder { /* Internet Explorer 10-11 */
        font-weight: 600;
    }

    input[type='number']::-ms-input-placeholder { /* Microsoft Edge */
        font-weight: 600;
    }
    td > i {
        position: absolute;
        top: 0;
        right: 0;
        font-size: 17px!important;
    }
</style>
<div class="row">
                        
                        
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-rose card-header-icon">

                <div class="card-icon">
                    <i class="material-icons">chrome_reader_mode</i>
                </div>

                <h4 class="card-title">M&E Budget Planner </h4>
            </div>
            <div class="card-body">
                <form action="" method="GET">
                    <select class="selectpicker" data-width="50%" id="selectActivity" data-style="btn btn-primary btn-round" title="Select an Activity">
                        <option disabled {{ !isset($activity) ? 'selected' : '' }}>Select an M&E Activity</option>
                        @foreach ($activities as $act)
                            <option value="{{ $act->id }}" {{ isset($activity) && $activity->id == $act->id ? 'selected' : '' }}>{{ $act->description.' ('.$act->quarter->term.', '.$act->measure.')' }}</option>
                        @endforeach
                    </select>
                </form>

                @isset($activity)
                    @if($errors->count() > 0)
                        <div class="alert alert-danger no-border mb-2">
                            <strong>Please Provide Valid Information</strong>
                        </div>
                    @endif
                    <form ng-app="Planner" ng-controller="RepeatController"  action="{{ route('finance.mne-budget-planner',[Request::segment(2),$activity->id]) }}" id="FormValidation" novalidate method="POST">
                        @csrf 
                        @php
                            $budget_items = old('budget_items') ? : $budget_items->toArray();
                            $quartarly_years = $project->quarter_years;
                            $start_month = (int)\Carbon\Carbon::parse($project->start_date)->format('m');

                        @endphp
                        <div ng-init="remaining_budget={{ $project->remaining_budget }};init({{ json_encode($budget_items) }})" ng-cloak>
                            <div class="alert alert-info alert-dismissible" data-notify="container" style="position: fixed; bottom:0; right:20px;display:block;padding: 16px;width: 25%;z-index:2">
                    
                                <span data-notify="message" style="    font-size: 15px;font-weight: 500;" ng-style="remaining_budget < 0 && {'color':'red'}">
                                    Total Budget: Rs. {{ $project->total_budget }}<br>
                                    Remaining Budget: Rs. @{{ remaining_budget ? remaining_budget : 0 }}
                                </span>
                            </div>
                            <div ng-repeat="item in items" style="position: relative">
                                <div style="text-align:right">
                                    <p data-notify="message" style="    font-size: 15px;font-weight: 500;">
                                        Expense: Rs. @{{ item.expense ? item.expense : 0 }}
                                    </p>
                                </div>
                                <input type="hidden" name="budget_items[@{{ $index }}][id]" ng-model="item.id" required/>

                                <button ng-hide="items.length==1" style="position: absolute;top: 50%;left: -35px;" type="button" class="btn btn-fab btn-danger btn-round btn-sm" ng-click="remove($index)" rel="tooltip" title="Remove"><i class="material-icons">delete</i></button>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group bmd-form-group">
                                            <select selectpicker name="budget_items[@{{ $index }}][budget_category]" ng-model="item.budget_category" data-style="select-with-transition" title="Choose Budget Category" required>
                                                <option disabled="" value="">Choose Budget Category</option>
                                                @foreach($budget_categories as $category)
                                                    <option value="{{ $category->title }}">{{ $category->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group bmd-form-group">
                                            <select selectpicker convert-to-number-select name="budget_items[@{{ $index }}][partner_id]" ng-model="item.partner_id" data-style="select-with-transition" title="Choose Partner" required>
                                                <option disabled="" value="">Choose Partner</option>
                                                @foreach($partners as $partner)
                                                    <option value="{{ $partner->id }}">{{ $partner->fullname }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group bmd-form-group">
                                            <textarea class="form-control" name="budget_items[@{{ $index }}][description]" ng-model="item.description" rows="1" placeholder="Description" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class=" text-primary">
                                            <tr>
                                                <th class="text-left">Year</th>
                                                @for($i=$start_month; $i<($start_month+12); $i++)
                                                    <th>{{ date("M", mktime(0, 0, 0, $i)) }}</th>
                                                @endfor
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $c = 0;
                                            @endphp
                                            @foreach($quartarly_years as $year => $quarters)
                                                
                                                <tr>
                                                    <td class="text-center" style="font-weight: 600;width:11%">{!! $year !!}</td>
                                                    
                                                    @foreach($quarters as $quarter)
                                                        
                                                        @php
                                                            // $color = 'rgba(' . mt_rand(0, 255).','.mt_rand(0, 255).','.mt_rand(0, 255).',0.3)';
                                                            $color = 'hsl(' . mt_rand(0, 360).','.mt_rand(25, 95).'%,'.mt_rand(85, 95).'%)';
                                                            $period = \Carbon\CarbonPeriod::create($quarter->start_month,'1 month', $quarter->end_month);
                                                        @endphp
                                                        
                                                        @foreach($period as $p)
                                                            <td style="background-color: {{ $color }}; position:relative">
                                                                <input type="hidden" name="budget_items[@{{ $index }}][budget_plans][{{ $c }}][year]" value="{{ $p->format('Y') }}" required/>
                                                                <input type="hidden" name="budget_items[@{{ $index }}][budget_plans][{{ $c }}][month]" value="{{ (int)$p->format('m') }}" required/>
                                                                <input min="0" style="font-weight: 600" type="number" class="form-control" placeholder="Rs." name="budget_items[@{{ $index }}][budget_plans][{{ $c }}][amount]" ng-model="item.budget_plans[{{ $c++ }}].amount" ng-change="recalculateBudget($index)" convert-to-number required/>
                                                            </td>
                                                        @endforeach
                                                    @endforeach
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <hr>
                            </div>
                            <div style="text-align: right;">

                                <button class="btn btn-primary btn-lg btn-round btn-fab" ng-click="add()" type="button" rel="tooltip" title="Add Budget Item"><i class="material-icons">add_circle</i></button>
                            </div>
                        </div>

                        <div class="text-center">
                            <button type="submit" ng-disabled="remaining_budget < 0" hidden class="btn btn-success">Save</button>
                        </div>
                    </form>
                @endisset

            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.8/angular.min.js"></script>
    <script src="/js/angularapp.js"></script>

    <script>
        $(document).ready(function(){
            $('button[type="submit"]').removeAttr('hidden');
            
            $('#selectActivity').on('change', function(){
                location.href = '/project/{{Request::segment(2)}}/mne/'+$(this).val()+'/budget-planner';
            })
        });
    </script>
@endsection