@extends('layouts.project')

@section('body')

<div class="row" ng-app="quarterapp" ng-controller="QuarterController" ng-init="init({{$quarters}},'{{$project->start_term}}',{{json_encode($dates)}})">
    
    <div class="col-md-12">
        <form action="/project/{{Request::segment(2)}}/quarters" id="FormValidation" method="POST" novalidate="novalidate" ng-cloak>
            @csrf
        <input type="text" hidden  name="year" value="{{$year}}" />

        <input type="text" hidden name="quarters" value="@{{quarters}}" />
            <div class="card ">
                <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">mail_outline</i>
                    </div>

                <h4 class="card-title">Manage Quarters for {{$project->start_year+$year-1}} - {{$project->start_year+$year}}</h4>
                </div>
                
                <table class="table" style="width:60%" >
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                           
                            <th>From </th>
                            <th >To </th>
    
                        <th class="text-right">Actions  </th>
                        </tr>
                        </thead>
                        <tbody>

                            <tr ng-repeat="q in quarters">
                                <td class="text-center">Quarter @{{$index +1 }} </td>
                                   
                                <td>
                                    <select ng-if="$index==0" name="from@{{$index}}" disabled ng-model="start_month"  class="form-control" data-style="select-with-transition"   title="Choose Month" data-size="12">
                                        <option value="" disabled>Choose Month</option>

                                        <option ng-repeat="t in terms" value="@{{ t }}">@{{ t | filter }}</option>
                                    </select>
                                    <select ng-if="$index!=0" name="from" disabled ng-model="q.start_month"  data-style="select-with-transition" class="form-control" title="Choose Month" data-size="12" required>
                                        <option value="" disabled>Choose Month</option>
                                        <option ng-repeat="t in terms" value="@{{ t }}" ng-disabled="validateMonth(t,quarters[$parent.$index-1].end_month,$parent.$index,1)">@{{ t | filter }}</option>
                                        
                                    </select>
                                </td>
                                <td>
                                    <select selectpicker required ng-model="q.end_month" ng-change="addOneMonth($index)" data-style="select-with-transition" title="Choose Month" data-size="12">
                                            <option value="" disabled>Choose Month</option>
                                            <option ng-repeat="t in terms" value="@{{ t }}" ng-disabled="$parent.$index!=0 && validateMonth(t,quarters[$parent.$index-1].end_month,$parent.$index,2)">@{{ t | filter }}</option>
                                    </select>
                                </td>
                                <td class="td-actions text-right">
                                
                                    <a href="#!" rel="tooltip" class="btn btn-danger" ng-if="quarters.length > 1"  ng-click="removeQuarter($index)" data-original-title="" title="">
                                        <i class="material-icons">clear</i>
                                    </a>
                                    <input type="text" hidden name="quarters" value="@{{quarters}}" />
                                  
                                
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                <div class="card-footer ">
                
                    <button type="button" class="btn btn-rose" ng-disabled="quarters[quarters.length-1].end_month >= end_month" ng-click="addQuarter()">Add</button> 
                    <button type="submit" class="btn btn-rose pull-left" ng-disabled="!quarters[quarters.length-1].end_month || quarters[quarters.length-1].end_month < end_month">Save</button>

                </div>
                
            </div>
        </form>
    </div>
    
</div>
@endsection

@section('js')
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
<script src="/js/projectsetup/quarter.js"></script>

@endsection


