@extends('layouts.project')

@section('body')
<div class="row">
                        
                        
       <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-rose card-header-icon">

                <div class="card-icon">
                    <i class="material-icons">assignment</i>
                </div>

                <h4 class="card-title">Quarters </h4>
            </div>

            @for($year=1; $year<=$project->n_years; $year++)
            
            <div class="card-body">

            <h4 class="card-title" style="margin-top:1rem">Quarters for Year {{$year}} ({{$project->start_year +$year -1}}-{{$project->start_year +$year}}) </h4>
            <a href="/project/{{Request::segment(2)}}/quarters/create?year={{$year}}" class="btn btn-warning btn-sm">Add/Edit Quarters</a>

                <div class="table-responsive">
                <table class="table" style="width:50%">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                       
                        <th>Months</th>

                        {{-- <th class="text-right">Actions</th> --}}
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($quarters->where('year',$year) as $quarter)
                        <tr>
                            <td class="text-center">Quarter {{$loop->iteration}}</td>
                               
                                <td>From {{$quarter->start_month}} to {{$quarter->end_month}} </td>
                               
                        @endforeach
                    </tbody>
                </table>
                </div>
                {{-- {{$quarters->links()}} --}}
            </div>
            @endfor
        </div>
    </div>
</div>


@endsection
