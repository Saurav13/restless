@extends('layouts.project')

@section('body')
<div class="row">
                        
                        
       <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-rose card-header-icon">

                <div class="card-icon">
                    <i class="material-icons">assignment</i>
                </div>

                <h4 class="card-title">Outputs </h4>
            </div>

            @foreach($outcomes as $outcome)
            
            <div class="card-body">
            <a href="/project/{{Request::segment(2)}}/outputs/create?outcome_id={{$outcome->id}}" class="btn btn-primary pull-right">Add Output</a>

            <h4 class="card-title" style="margin-top:1rem">Outputs for {{$outcome->title}} </h4>

                <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        @if(\App\Project::findOrFail(Request::segment(2))->status!='draft')
                            <th>Code</th>
                        @endif
                        <th>Title</th>
                        <th>Description</th>
                        <th>Indicators</th>

                        <th class="text-right">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($outputs->where('outcome_id',$outcome->id) as $output)
                        <tr>
                            <td class="text-center">{{$loop->iteration}}</td>
                                @if(\App\Project::findOrFail(Request::segment(2))->status!='draft')
                                    <td>{{$output->code}}</td>
                                @endif
                                <td>{{$output->title}}</td>
                                <td>{{$output->description}}</td>
                                <td>{{$output->indicators()->count() }} indicators <br> <a href="{{ route('indicators.index',[Request::segment(2),'outputs',$output->id]) }}" class="btn btn-sm btn-warning">Manage Indicators</a></td>

                                <td class="td-actions text-right">
                                <button type="button" rel="tooltip" class="btn btn-info" data-original-title="" title="">
                                    <i class="material-icons">person</i>
                                </button>
                                <a href="/project/{{Request::segment(2)}}/outputs/{{$output->id}}/edit" rel="tooltip" class="btn btn-success" data-original-title="" title="">
                                    <i class="material-icons">edit</i>
                                </a>
                                <form action="/project/{{Request::segment(2)}}/outputs/{{$output->id}}" method="POST" style="display:inline">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE" >
                                    <button  id='deleteInfo{{ $output->id }}' type="button"  rel="tooltip" class="btn btn-danger" data-original-title="" title="">
                                            <i class="material-icons">close</i>
                                    </button>
                                </form>
                            
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
                {{-- {{$outputs->links()}} --}}
            </div>
            @endforeach
        </div>
    </div>
</div>


@endsection
