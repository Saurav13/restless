@extends('layouts.project')

@section('body')
<div class="row">
    
    <div class="col-md-12">
        
    <form action="/project/{{Request::segment(2)}}/outputs/{{$output->id}}" method="POST"  novalidate="novalidate">
            @csrf
            <input type="text" hidden name="_method" value="PUT"/>
            <div class="card ">
                <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">mail_outline</i>
                    </div>
                    <h4 class="card-title">Edit Output</h4>
                </div>
                <div class="card-body ">
                    <div class="form-group bmd-form-group">
                        <label for="exampleEmail" class="bmd-label-floating"> Title *</label>
                        <input type="text" name="title" class="form-control {{ $errors->has('title') ? ' has-danger' : '' }}" id="exampleEmail" required="true" value="{{ $output->title}}" aria-required="true">
                    </div>

                    @if ($errors->has('title'))
                        <div class="alert alert-danger no-border mb-2">
                            <strong>{{ $errors->first('title') }}</strong>
                        </div>
                    @endif
                <div class="form-group bmd-form-group">
                    <label class="bmd-label-floating"> Description* </label>
                    <textarea name="description" class="form-control {{ $errors->has('description') ? ' has-danger' : '' }}"  rows="5">{{ $output->description}}</textarea>
                    @if ($errors->has('description'))
                        <div class="alert alert-danger no-border mb-2">
                            <strong>{{ $errors->first('description') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="form-group bmd-form-group">
                    <label class="bmd-label-floating"> Initial Situation </label>
                    <textarea name="initial_situation" class="form-control {{ $errors->has('initial_situation') ? ' has-danger' : '' }}"  rows="5">{{ $output->initial_situation}}</textarea>
                    @if ($errors->has('initial_situation'))
                        <div class="alert alert-danger no-border mb-2">
                            <strong>{{ $errors->first('initial_situation') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="form-group bmd-form-group">
                    <label class="bmd-label-floating"> Expected Situation </label>
                    <textarea name="expected_situation" class="form-control {{ $errors->has('expected_situation') ? ' has-danger' : '' }}" rows="5">{{ $output->expected_situation}}</textarea>
                    @if ($errors->has('expected_situation'))
                        <div class="alert alert-danger no-border mb-2">
                            <strong>{{ $errors->first('expected_situation') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="form-group bmd-form-group">
                    <label class="bmd-label-floating"> Approach/Strategy </label>
                    <textarea name="approach" class="form-control {{ $errors->has('approach') ? ' has-danger' : '' }}" rows="5">{{ $output->approach}}</textarea>
                    @if ($errors->has('approach'))
                        <div class="alert alert-danger no-border mb-2">
                            <strong>{{ $errors->first('approach') }}</strong>
                        </div>
                    @endif
                </div>
                
                <div class="category form-category">* Required fields</div>
                </div>
                <div class="card-footer text-right">
                
                    <button type="submit" class="btn btn-rose">Save</button>
                </div>
            </div>
        </form>
    </div>
    
</div>
@endsection