@extends('layouts.project')

@section('body')
<div class="row">
                        
                        
       <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-rose card-header-icon">

                <div class="card-icon">
                    <i class="material-icons">assignment</i>
                </div>
                <a href="{{ route('indicators.create',[Request::segment(2),$model->text,$model->id]) }}" class="btn btn-primary pull-right">Add Indicator</a>

                <h4 class="card-title">Indicators for {{ $model->title }}</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>Title</th>
                        <th>Qualitative Description</th>
                        <th>Reach</th>

                        <th class="text-right">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($indicators as $instance)
                            <tr>
                                <td class="text-center">{{$loop->iteration}}</td>
                                
                                <td>{{$instance->title}}</td>
                                <td>{{$instance->description}}</td>
                                <td>{{ $instance->beneficiaries()->count() }}</td>
                                <td class="td-actions text-right">
                                    <a href="{{ route('indicators.edit',[Request::segment(2),$model->text,$model->id,$instance->id]) }}" rel="tooltip" class="btn btn-success" data-original-title="" title="">
                                        <i class="material-icons">edit</i>
                                    </a>
                                    <form action="{{ route('indicators.destroy',[Request::segment(2),$model->text,$model->id,$instance->id]) }}" method="POST" style="display:inline">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE" >
                                        <button id='deleteInfo{{ $instance->id }}' type="button"  rel="tooltip" class="btn btn-danger" data-original-title="" title="">
                                                <i class="material-icons">close</i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
                {{$indicators->links()}}
            </div>
        </div>
    </div>
</div>
@endsection