@extends('layouts.project')

@section('body')
<div class="row">
    
    <div class="col-md-12">
        <div class="card ">
            <div class="card-header card-header-rose card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">question_answer</i>
                </div>
                <h4 class="card-title">Edit Question</h4>
            </div>
            <div class="card-body ">
                <form id="RecordValidation" action="{{ route('settings.questions-planner.update',[Request::segment(2),$question->id]) }}" method="POST" novalidate="novalidate">
                    @csrf
                    <input type="text" hidden name="_method" value="PUT"/>
                    
                    <div class="form-group bmd-form-group">
                        <label for="question" class="bmd-label-floating"> Question *</label>
                        <textarea name="question" class="form-control {{ $errors->has('question') ? ' has-danger' : '' }}"  rows="5" required>{{ $question->question }}</textarea>

                        @if ($errors->has('question'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('question') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="form-group bmd-form-group">
                        <label class="bmd-label-floating"> Question Type</label><br>    
                        <select class="selectpicker" name="question_type" data-style="select-with-transition" title="Choose Type" data-width="100%" required>
                            <option value="" disabled>Choose Type</option>
                            <option value="choice" {{ $question->question_type == 'choice' ? 'selected' : '' }}>Choice</option>
                            <option value="non-choice" {{ $question->question_type == 'non-choice' ? 'selected' : '' }}>Non-Choice</option>
                        </select>
                        @if ($errors->has('question_type'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('question_type') }}</strong>
                            </div>
                        @endif
                    </div>
                    <br>

                    <div class="modal-footer">
                    
                        <button type="submit" class="btn btn-success">Save</button>
                        <a href="{{ route('settings.questions-planner.index',Request::segment(2)) }}" type="button" class="btn btn-neutral">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
</div>
@endsection

@section('js')

    <script>
        $(document).ready(function(){
            setFormValidation('#RecordValidation');
  
        });
    </script>
@endsection