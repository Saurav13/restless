@extends('layouts.project')

@section('body')
<div class="row">
                        
                        
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-rose card-header-icon">

                <div class="card-icon">
                    <i class="material-icons">question_answer</i>
                </div>
                <button data-toggle="modal" data-target="#addQuestion" class="btn btn-primary pull-right">Add Question</button>

                <h4 class="card-title">Questions </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class=" text-primary">
                            <tr>
                                <th class="text-center">#</th>
                                
                                <th>Question</th>
                                <th>Type</th>
                                <th>Created By</th>

                                <th class="text-right">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($questions as $question)
                                <tr>
                                    <td class="text-center">{{$loop->iteration}}</td>
                                    
                                    <td>{{ $question->question }}</td>
                                    <td style="text-transform:capitalize">{{ $question->question_type }}</td>
                                    <td>{{ $question->creator->name }}</td>
                                    <td class="td-actions text-right">
                                        
                                        <a href="{{ route('settings.questions-planner.edit',[Request::segment(2),$question->id]) }}" rel="tooltip" class="btn btn-success" data-original-title="" title="">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        <form action="{{ route('settings.questions-planner.destroy',[Request::segment(2),$question->id]) }}" method="POST" style="display:inline">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="DELETE" >
                                            <button id='deleteInfo{{ $question->id }}' type="button"  rel="tooltip" class="btn btn-danger" data-original-title="" title="">
                                                    <i class="material-icons">close</i>
                                            </button>
                                        </form>
                                
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{$questions->links()}}
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addQuestion" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Question</h5>
                <button type="button" class="close" data-dismiss="modal">
                    <i class="material-icons">clear</i>
                </button>
            </div>
            <div class="modal-body">
                <form id="RecordValidation" action="{{ route('settings.questions-planner.store',Request::segment(2)) }}" method="POST" novalidate="novalidate">
                    @csrf
                    
                    <div class="form-group bmd-form-group">
                        <label for="question" class="bmd-label-floating"> Question *</label>
                        <textarea name="question" class="form-control {{ $errors->has('question') ? ' has-danger' : '' }}"  rows="5" required>{{ old('question') }}</textarea>

                        @if ($errors->has('question'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('question') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="form-group bmd-form-group">
                        <label class="bmd-label-floating"> Question Type</label><br>    
                        <select class="selectpicker" name="question_type" data-style="select-with-transition" title="Choose Type" data-width="100%" required>
                            <option value="" {{ !old('question_type') ? 'selected' : '' }} disabled>Choose Type</option>
                            <option value="choice" {{ old('question_type') == 'choice' ? 'selected' : '' }}>Choice</option>
                            <option value="non-choice" {{ old('question_type') == 'non-choice' ? 'selected' : '' }}>Non-Choice</option>
                        </select>
                        @if ($errors->has('question_type'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('question_type') }}</strong>
                            </div>
                        @endif
                    </div>
                    <br>
                    
                    <div class="modal-footer" style="padding: 0;">
                    
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection

@section('js')
    @if($errors->count() > 0)
        <script>
            $('#addQuestion').modal('show');
        </script>
    @endif

    <script>
        $(document).ready(function(){

            setFormValidation('#RecordValidation');
  
        });
    </script>
@endsection
