@extends('layouts.project')

@section('body')
<div class="row">
                        
                        
       <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-rose card-header-icon">

                <div class="card-icon">
                    <i class="material-icons">assignment</i>
                </div>
                <a href="{{ route('projects.indicators.create',[Request::segment(2)]) }}" class="btn btn-primary pull-right">Add Indicator</a>

                <h4 class="card-title">Project Indicators</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>Title</th>
                        <th>Qualitative Description</th>
                        <th>Reach</th>

                        <th class="text-right">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($indicators as $instance)
                            <tr>
                                <td class="text-center">{{$loop->iteration}}</td>
                                
                                <td>{{$instance->title}}</td>
                                <td>{{$instance->description}}</td>
                                <td>{{ $instance->beneficiaries()->count() }}</td>
                                <td class="td-actions text-right">
                                    <a href="{{ route('projects.indicators.edit',[Request::segment(2),$instance->id]) }}" rel="tooltip" class="btn btn-success" data-original-title="" title="">
                                        <i class="material-icons">edit</i>
                                    </a>
                                    <form action="{{ route('projects.indicators.destroy',[Request::segment(2),$instance->id]) }}" method="POST" style="display:inline">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="DELETE" >
                                        <button id='deleteInfo{{ $instance->id }}' type="button"  rel="tooltip" class="btn btn-danger" data-original-title="" title="">
                                                <i class="material-icons">close</i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
                {{$indicators->links()}}
            </div>
        </div>
    </div>
</div>
@endsection