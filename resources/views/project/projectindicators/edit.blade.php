@extends('layouts.project')

@section('body')
<div class="row">
    
    <div class="col-md-12">
        
        <form id="FormValidation" action="{{ route('projects.indicators.update',[Request::segment(2),$indicator->id]) }}" method="POST" enctype="multipart/form-data" novalidate="novalidate">
            @csrf
            <input type="hidden" name="_method" value="PUT" />
            <div class="card ">
                <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">mail_outline</i>
                    </div>
                    <h4 class="card-title">Edit Indicator</h4>
                </div>
                <div class="card-body ">
                    <div class="form-group bmd-form-group">
                        <label for="title" class="bmd-label-floating"> Title *</label>
                        <input type="text" name="title" class="form-control" id="title" required="true" value="{{ $indicator->title }}">
                        @if ($errors->has('title'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('title') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="form-group bmd-form-group">
                        <label class="bmd-label-floating">Qualitative Description* </label>
                        <textarea name="description" class="form-control" rows="5" required>{{ $indicator->description }}</textarea>
                        @if ($errors->has('description'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('description') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="form-group bmd-form-group">
                        <label class="bmd-label-floating">Reporting Frequency* </label>
                        <input name="reporting_frequency" class="form-control" required value="{{ $indicator->reporting_frequency }}" />
                        @if ($errors->has('reporting_frequency'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('reporting_frequency') }}</strong>
                            </div>
                        @endif
                    </div>

                    <div class="form-group bmd-form-group">
                        <select class="selectpicker" data-width="100%" name="overall_target_type" data-style="select-with-transition" title="Choose Overall Target Type" required>
                            <option disabled="" value="" {{ !$indicator->overall_target_type ? 'selected' : '' }}>Choose Overall Target Type</option>
                            <option value="Number" {{ $indicator->overall_target_type == 'Number' ? 'selected' : '' }}>Number</option>
                            <option value="Percentage" {{ $indicator->overall_target_type == 'Percentage' ? 'selected' : '' }}>Percentage</option>
                        </select>
                        @if ($errors->has('overall_target_type'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('overall_target_type') }}</strong>
                            </div>
                        @endif
                    </div>

                    <div class="form-group bmd-form-group">
                        <label class="bmd-label-floating">Overall Target* </label>
                        <input name="overall_target" min="0" class="form-control" type="number" required value="{{ $indicator->overall_target }}" />
                        @if ($errors->has('overall_target'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('overall_target') }}</strong>
                            </div>
                        @endif
                    </div>

                    <div class="form-group bmd-form-group">
                        <label class="bmd-label-floating"> Means of Verification* </label>
                        <textarea name="means_of_verification" class="form-control" rows="5" required>{{ $indicator->means_of_verification }}</textarea>
                        @if ($errors->has('means_of_verification'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('means_of_verification') }}</strong>
                            </div>
                        @endif
                    </div>

                    <div class="row">
                        <div class="form-group col-md-12 col-sm-12">
                        
                            <label>File</label><br>
                            <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                            
                                <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                <div>
                                    <span class="btn btn-rose btn-round btn-file">
                                        <span class="fileinput-new">Select File</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" name="file" style="z-index: 2;"/>

                                    </span>
                                </div>
                            </div><br>
                            @if ($errors->has('file'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('file') }}</strong>
                                </div>
                            @endif
                        </div>
                        
                        
                    </div>
                    
                    <hr>
                    <div>
                        <h4 class="card-title " style="text-align:center">Reach</h4>
                        @if ($errors->has('beneficiaries'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('beneficiaries') }}</strong>
                            </div>
                        @endif
                        @if ($errors->has('beneficiaries.*'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>Reach couldn't be set</strong>
                            </div>
                        @endif
                        @php
                            $indicator_beneficiaries = $indicator_beneficiaries->toArray();

                        @endphp

                        <div ng-app="Planner" ng-controller="RepeatController" ng-init="init({{ json_encode($indicator_beneficiaries) }})" ng-cloak>
                            <div class="mb-2" style="text-align: right;">
                                <button class="btn btn-primary btn-sm" ng-click="add()" type="button" rel="tooltip" title="Add"><i class="material-icons">add_circle</i> Add</button>
                            </div>
                            <div class="row" ng-repeat="item in items">
                                    
                                <div class="col-md-4">
                                    <div class="form-group bmd-form-group">
                                        <select convert-to-number-select selectpicker data-width="100%" name="beneficiaries[@{{ $index }}][id]" ng-model="item.beneficiary_id" data-style="select-with-transition" title="Choose Reach Target" required>
                                            <option disabled="" value="">Choose Reach Target</option>
                                            @foreach ($beneficiaries as $b)
                                                <option value="{{ $b->id }}">{{ $b->title }}</option>
                                            @endforeach
                                            
                                        </select>
                                        
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <div class="form-group bmd-form-group">
                                        <select selectpicker data-width="100%" name="beneficiaries[@{{ $index }}][target_type]" ng-model="item.target_type" data-style="select-with-transition" title="Choose Overall Target Type" required>
                                            <option disabled="" value="">Choose Overall Target Type</option>
                                            <option value="Number">Number</option>
                                            <option value="Percentage">Percentage</option>
                                        </select>
                                        
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group bmd-form-group">
                                        <label class="bmd-label-floating">Overall Target* </label>
                                        <input convert-to-number name="beneficiaries[@{{ $index }}][target]" ng-model="item.target" min="0" class="form-control" type="number" required />
                                        
                                    </div>
                                </div>
                                <div class="col-md-1">

                                    <button ng-hide="items.length==1" type="button" class="btn btn-fab btn-danger btn-round btn-sm" ng-click="remove($index)" rel="tooltip" title="Remove"><i class="material-icons">close</i></button>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    
                    <button type="submit" class="btn btn-success">Save</button>
                    <a href="{{ route('projects.indicators.index',[Request::segment(2)]) }}" type="button" class="btn btn-neutral">Cancel</a>
                </div>
            </div>
        </form>
    </div>
    
</div>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.8/angular.min.js"></script>
    <script src="/js/angularapp.js"></script>
    <script>
        $(document).ready(function(){
  
        });
    </script>
@endsection