@extends('layouts.project')

@section('body')
<div class="row">
                        
                        
       <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-rose card-header-icon">

                <div class="card-icon">
                    <i class="material-icons">assignment</i>
                </div>
                <a href="/project/{{Request::segment(2)}}/beneficiaries/create" class="btn btn-primary pull-right">Add Reach</a>

                <h4 class="card-title">Reach </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>Title</th>
                        <th>Gender</th>
                        <th>Age Range</th>
                        <th>Ethnicity</th>


                        <th class="text-right">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($beneficiaries as $beneficiary)
                        <tr>
                                <td class="text-center">{{$loop->iteration}}</td>
                                <td>{{$beneficiary->title}}</td>
                              
                                <td>{{$beneficiary->gender}}</td>
                                <td>{{$beneficiary->age_range_from}} - {{$beneficiary->age_range_to}} </td>
                                <td>{{$beneficiary->ethnicity}}</td>


                                {{-- <td>2 indicators <br> <a href="" class="btn btn-sm btn-warning">Manage Indicators</a></td> --}}
                                <td class="td-actions text-right">
                                <button type="button" rel="tooltip" class="btn btn-info" data-original-title="" title="">
                                    <i class="material-icons">person</i>
                                </button>
                                <a href="/project/{{Request::segment(2)}}/beneficiaries/{{$beneficiary->id}}/edit" rel="tooltip" class="btn btn-success" data-original-title="" title="">
                                    <i class="material-icons">edit</i>
                                </a>
                                <form action="/project/{{Request::segment(2)}}/beneficiaries/{{$beneficiary->id}}" method="POST" style="display:inline">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE" >
                                    <button  id='deleteInfo{{ $beneficiary->id }}' type="button"  rel="tooltip" class="btn btn-danger" data-original-title="" title="">
                                            <i class="material-icons">close</i>
                                    </button>
                                </form>
                            
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
                {{$beneficiaries->links()}}
            </div>
        </div>
    </div>
</div>


@endsection
