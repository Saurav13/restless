@extends('layouts.project')

@section('body')
<div class="row">
    
    <div class="col-md-12">
        
    <form action="/project/{{Request::segment(2)}}/beneficiaries/{{$beneficiary->id}}" method="POST" novalidate="novalidate">
            @csrf
            <input type="text" hidden name="_method" value="PUT"/>

            <div class="card ">
                <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">mail_outline</i>
                    </div>
                    <h4 class="card-title">Edit Reach</h4>
                </div>
                <div class="card-body ">
                    <div class="form-group bmd-form-group">
                        <label for="exampleEmail" class="bmd-label-floating"> Title *</label>
                        <input type="text" name="title" class="form-control {{ $errors->has('title') ? ' has-danger' : '' }}"  required="true" value="{{ $beneficiary->title }}" aria-required="true">
                    </div>

                    @if ($errors->has('title'))
                        <div class="alert alert-danger no-border mb-2">
                            <strong>{{ $errors->first('title') }}</strong>
                        </div>
                    @endif

                  
                    <div class="form-group bmd-form-group">
                            {{-- <label class="bmd-label-floating"> Output </label> --}}
    
                      
                        <select required name="gender" class="selectpicker" data-style="select-with-transition"  title="Choose Gender *" data-size="7">
                            <option value="Male" {{$beneficiary->gender=="Male"?'selected':''}}>Male</option>
                            <option value="Female" {{$beneficiary->gender=="Female"?'selected':''}}>Female</option>
                            <option value="Others" {{$beneficiary->gender=="Others"?'selected':''}}>Others</option>

                           
                        </select>
                        @if ($errors->has('gender'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('gender') }}</strong>
                            </div>
                        @endif
                    </div>

                   

                    <div class="form-group bmd-form-group">
                        <label for="exampleEmail" class="bmd-label-floating"> Age Range (From) *</label>
                        <input type="number" name="age_range_from" class="form-control {{ $errors->has('age_range_from') ? ' has-danger' : '' }}"  required="true" value="{{ $beneficiary->age_range_from }}" aria-required="true">
                    </div>

                    @if ($errors->has('age_range_from'))
                        <div class="alert alert-danger no-border mb-2">
                            <strong>{{ $errors->first('age_range_from') }}</strong>
                        </div>
                    @endif
                
                    <div class="form-group bmd-form-group">
                        <label for="exampleEmail" class="bmd-label-floating"> Age Range (To) *</label>
                        <input type="number" name="age_range_to" class="form-control {{ $errors->has('age_range_to') ? ' has-danger' : '' }}"  required="true" value="{{ $beneficiary->age_range_to }}" aria-required="true">
                    </div>

                    @if ($errors->has('age_range_to'))
                        <div class="alert alert-danger no-border mb-2">
                            <strong>{{ $errors->first('age_range_to') }}</strong>
                        </div>
                    @endif

                    <div class="form-group bmd-form-group">
                            {{-- <label class="bmd-label-floating"> Output </label> --}}
    
                      
                        <select required name="ethnicity" class="selectpicker" data-style="select-with-transition"  title="Choose Ethinicity *" data-size="7">
                            @foreach(App\Ethnicity::all() as $eth)
                            <option value="{{$eth->name}}" {{$beneficiary->ethnicity==$eth->name?'selected':''}}>{{$eth->name}}</option>
                           @endforeach
                           
                        </select>
                        @if ($errors->has('ethnicity'))
                            <div class="alert alert-danger no-border mb-2">
                                <strong>{{ $errors->first('ethnicity') }}</strong>
                            </div>
                        @endif
                    </div>
                    <div class="category form-category">* Required fields</div>
                    </div>
                    <div class="card-footer text-right">
                    
                        <button type="submit" class="btn btn-rose">Save</button>
                    </div>
            </div>
        </form>
    </div>
    
</div>
@endsection