@extends('layouts.project')

@section('body')
<div class="row">
    
    <div class="col-md-12">
        
    <form action="/project/{{Request::segment(2)}}/geographicregions" method="POST" novalidate="novalidate">
            @csrf
            
            <div class="card ">
                <div class="card-header card-header-rose card-header-icon">
                    <div class="card-icon">
                        <i class="material-icons">mail_outline</i>
                    </div>
                    <h4 class="card-title">Add Geographic Area</h4>
                </div>
                <div class="card-body ">
                    <div class="form-group bmd-form-group">
                        <label for="exampleEmail" class="bmd-label-floating"> Ward *</label>
                        <input type="text" name="ward" class="form-control {{ $errors->has('ward') ? ' has-danger' : '' }}"  required="true" value="{{ old('ward') }}" aria-required="true">
                    </div>

                    @if ($errors->has('ward'))
                        <div class="alert alert-danger no-border mb-2">
                            <strong>{{ $errors->first('ward') }}</strong>
                        </div>
                    @endif

                    <div class="form-group bmd-form-group">
                        <label for="exampleEmail" class="bmd-label-floating"> Metropolitan *</label>
                        <input type="text" name="metropolitan" class="form-control {{ $errors->has('metropolitan') ? ' has-danger' : '' }}"  required="true" value="{{ old('metropolitan') }}" aria-required="true">
                    </div>

                    @if ($errors->has('metropolitan'))
                        <div class="alert alert-danger no-border mb-2">
                            <strong>{{ $errors->first('metropolitan') }}</strong>
                        </div>
                    @endif

                    <div class="form-group bmd-form-group">
                        <label for="exampleEmail" class="bmd-label-floating"> District *</label>
                        <input type="text" name="district" class="form-control {{ $errors->has('district') ? ' has-danger' : '' }}"  required="true" value="{{ old('district') }}" aria-required="true">
                    </div>

                    @if ($errors->has('district'))
                        <div class="alert alert-danger no-border mb-2">
                            <strong>{{ $errors->first('district') }}</strong>
                        </div>
                    @endif
                
                    <div class="form-group bmd-form-group">
                        <label for="exampleEmail" class="bmd-label-floating"> Province *</label>
                        <input type="text" name="province" class="form-control {{ $errors->has('province') ? ' has-danger' : '' }}"  required="true" value="{{ old('province') }}" aria-required="true">
                    </div>

                    @if ($errors->has('province'))
                        <div class="alert alert-danger no-border mb-2">
                            <strong>{{ $errors->first('province') }}</strong>
                        </div>
                    @endif

                    <div class="form-group bmd-form-group">
                        <label for="exampleEmail" class="bmd-label-floating"> Country *</label>
                        <input type="text" name="country" class="form-control {{ $errors->has('country') ? ' has-danger' : '' }}"  required="true" value="{{ old('country') }}" aria-required="true">
                    </div>

                    @if ($errors->has('country'))
                        <div class="alert alert-danger no-border mb-2">
                            <strong>{{ $errors->first('country') }}</strong>
                        </div>
                    @endif
                
                    <div class="category form-category">* Required fields</div>
                    </div>
                    <div class="card-footer text-right">
                    
                        <button type="submit" class="btn btn-rose">Add</button>
                    </div>
            </div>
        </form>
    </div>
    
</div>
@endsection