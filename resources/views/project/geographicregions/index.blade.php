@extends('layouts.project')

@section('body')
<div class="row">
                        
                        
       <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-rose card-header-icon">

                <div class="card-icon">
                    <i class="material-icons">assignment</i>
                </div>
                <a href="/project/{{Request::segment(2)}}/geographicregions/create" class="btn btn-primary pull-right">Add Geographic Area</a>

                <h4 class="card-title">Geographic Area </h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        
                        <th>Ward</th>
                        <th>Metropolitan</th>
                        <th>District</th>
                        <th>Province</th>
                        <th>Country</th>


                        <th class="text-right">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($geographicregions as $geographicregion)
                        <tr>
                                <td class="text-center">{{$loop->iteration}}</td>
                              
                                <td>{{$geographicregion->ward}}</td>
                                <td>{{$geographicregion->metropolitan}}</td>
                                <td>{{$geographicregion->district}}</td>
                                <td>{{$geographicregion->province}}</td>
                                <td>{{$geographicregion->country}}</td>


                                {{-- <td>2 indicators <br> <a href="" class="btn btn-sm btn-warning">Manage Indicators</a></td> --}}
                                <td class="td-actions text-right">
                                <button type="button" rel="tooltip" class="btn btn-info" data-original-title="" title="">
                                    <i class="material-icons">person</i>
                                </button>
                                <a href="/project/{{Request::segment(2)}}/geographicregions/{{$geographicregion->id}}/edit" rel="tooltip" class="btn btn-success" data-original-title="" title="">
                                    <i class="material-icons">edit</i>
                                </a>
                                <form action="/project/{{Request::segment(2)}}/geographicregions/{{$geographicregion->id}}" method="POST" style="display:inline">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE" >
                                    <button  id='deleteInfo{{ $geographicregion->id }}' type="button"  rel="tooltip" class="btn btn-danger" data-original-title="" title="">
                                            <i class="material-icons">close</i>
                                    </button>
                                </form>
                            
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
                {{$geographicregions->links()}}
            </div>
        </div>
    </div>
</div>


@endsection
