@include('project.partials.header',['title'=>'| Project Panel'])
@include('project.partials.navbar')
@yield('body')
@include('project.partials.footer')
