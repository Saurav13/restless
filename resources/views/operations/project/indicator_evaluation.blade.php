@extends('layouts.project')

@section('body')

    <div class="row">

        <div class="col-md-12">
            
            <div class="card">
                <div class="card-header card-header-rose card-header-icon">

                    <div class="card-icon">
                        <i class="material-icons">chrome_reader_mode</i>
                    </div>

                    <h4 class="card-title">Project Indicator Evaluation</h4>
                </div>
                <div class="card-body">
                    @if ($errors->count() > 0)
                        <div class="alert alert-danger no-border mb-2">
                            <strong>Please Provide Correct Information</strong>
                        </div>
                    @endif
                    <form action="{{ route('projects.indicator-evaluation.save',[Request::segment(2)]) }}" id="FormValidation" novalidate method="POST">
                        @csrf
                        @foreach ($indicators as $indicator)                    
                            @php
                                switch ($loop->iteration % 5) {
                                    case 1:
                                        $color = 'warning';
                                        break;
                                    case 2:
                                        $color = 'success';
                                        break;
                                    case 3:
                                        $color = 'danger';
                                        break;
                                    case 4:
                                        $color = 'info';
                                        break;
                                    case 0:
                                        $color = 'primary';
                                        break;
                                }
                            @endphp
                            <div class="card">
                                <div class="card-header card-header-text card-header-{{ $color }}" >
                                    <div class="card-text">
                                        <h4 class="card-title">{{ $indicator->title }}</h4>
                                        <p class="card-category">{{ $indicator->description }}</p>
                                    </div>
                                </div>
                                <div class="card-body table-responsive">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <input type="hidden" value="{{ $indicator->id }}" name="indicators[{{ $indicator->id }}][id]" />
                                            <div class="form-group bmd-form-group">
                                                <label class="bmd-label-floating"> 
                                                    Actual {{ $indicator->target_type }}
                                                </label>
                                                <input type="number" min="0" {{ $indicator->target_type == 'Percentage' ? "max=100" : '' }} name="indicators[{{ $indicator->id }}][actual_number]" class="form-control {{ $errors->has('indicators.'.($indicator->id).'.actual_number') ? ' has-danger' : '' }}" required="true"  aria-required="true" required="true" value="{{ old('indicators.'.($indicator->id).'.actual_number') ? : $indicator->actual_number }}">
                                                @if($indicator->target_type == 'Percentage')
                                                    <span class="form-control-feedback" style="font-size: 15px;opacity:1">
                                                        %
                                                    </span>
                                                @endif
                                            </div>
                        
                                            @if ($errors->has('indicators.'.($indicator->id).'.actual_number'))
                                                <div class="alert alert-danger no-border mb-2">
                                                    <strong>{{ $errors->first('indicators.'.($indicator->id).'.actual_number') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="col-md-8">
                                        
                                            <div class="form-group bmd-form-group">
                                                <label class="bmd-label-floating"> Comment </label>
                                                <textarea name="indicators[{{ $indicator->id }}][actual_qualitative_description]" class="form-control {{ $errors->has('indicators.'.($indicator->id).'.actual_qualitative_description') ? ' has-danger' : '' }}" value="" rows="1" required>{{ old('indicators.'.($indicator->id).'.actual_qualitative_description') ? : $indicator->actual_qualitative_description }}</textarea>
                                                @if ($errors->has('indicators.'.($indicator->id).'.actual_qualitative_description'))
                                                    <div class="alert alert-danger no-border mb-2">
                                                        <strong>{{ $errors->first('indicators.'.($indicator->id).'.actual_qualitative_description') }}</strong>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <table class="table table-hover">
                                        <thead class="text-{{ $color }}">
                                            <tr>
                                                <th>ID</th>
                                                <th style="width:20%">Reach</th>
                                                <th style="width:20%">Actual Number/Percentage</th>
                                                <th>Comment</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($indicator->beneficiaries as $reach)
                                                <tr>
                                                    @php
                                                        $beneficiary = $reach->beneficiary;
                                                    @endphp
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $beneficiary->gender }}, Age Range ({{ $beneficiary->age_range_from }} - {{ $beneficiary->age_range_to }})</td>
                                                    <td>
                                                        <input type="hidden" value="{{ $reach->id }}" name="beneficiaries[{{ $reach->id }}][id]" />
                                                        <div class="form-group bmd-form-group">
                                                            <label class="bmd-label-floating"> 
                                                                Actual {{ $reach->target_type }}
                                                            </label>
                                                            <input type="number" min="0" {{ $reach->target_type == 'Percentage' ? "max=100" : '' }} name="beneficiaries[{{ $reach->id }}][actual_number]" class="form-control {{ $errors->has('beneficiaries.'.($reach->id).'.actual_number') ? ' has-danger' : '' }}" required="true"  aria-required="true" required="true" value="{{ old('beneficiaries.'.($reach->id).'.actual_number') ? : $reach->actual_number }}">
                                                            @if($reach->target_type == 'Percentage')
                                                                <span class="form-control-feedback" style="font-size: 15px;opacity:1">
                                                                    %
                                                                </span>
                                                            @endif
                                                        </div>
                                    
                                                        @if ($errors->has('beneficiaries.'.($reach->id).'.actual_number'))
                                                            <div class="alert alert-danger no-border mb-2">
                                                                <strong>{{ $errors->first('beneficiaries.'.($reach->id).'.actual_number') }}</strong>
                                                            </div>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <div class="form-group bmd-form-group">
                                                            <label class="bmd-label-floating"> Comment </label>
                                                            <textarea name="beneficiaries[{{ $reach->id }}][actual_qualitative_description]" class="form-control {{ $errors->has('beneficiaries.'.($reach->id).'.actual_qualitative_description') ? ' has-danger' : '' }}" value="" rows="1" required>{{ old('beneficiaries.'.($reach->id).'.actual_qualitative_description') ? : $reach->actual_qualitative_description }}</textarea>
                                                            @if ($errors->has('beneficiaries.'.($reach->id).'.actual_qualitative_description'))
                                                                <div class="alert alert-danger no-border mb-2">
                                                                    <strong>{{ $errors->first('beneficiaries.'.($reach->id).'.actual_qualitative_description') }}</strong>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endforeach

                        <div class="text-right">
                            <button type="submit" class="btn btn-success">Save</button>
                            
                        </div>
                    </form>
                </div>
            </div>

            
        </div>
    </div>
@endsection

@section('js')

    <script>
        $(document).ready(function(){

            setFormValidation('#FormValidation');
        });
    </script>
@endsection
