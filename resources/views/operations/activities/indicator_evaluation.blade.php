@extends('layouts.project')

@section('body')

    <div class="row">

        <div class="col-md-12">
            
            <div class="card">
                <div class="card-header card-header-rose card-header-icon">

                    <div class="card-icon">
                        <i class="material-icons">chrome_reader_mode</i>
                    </div>

                    <h4 class="card-title">Activity Indicator Evaluation</h4>
                </div>
                <div class="card-body">
                    <form action="" method="GET">
                        
                        <select class="selectpicker"  id="selectActivity" data-style="btn btn-primary btn-round" title="Select an Activity">
                            <option disabled {{ !isset($activity) ? 'selected' : '' }}>Select an Activity</option>
                            @foreach ($activities as $act)
                                @foreach ($act->instances as $instance)
                                    <option value="{{ $instance->id }}" {{ isset($activity) && $activity->id == $instance->id ? 'selected' : '' }}>{{ $instance->title }}</option>
                                @endforeach
                            @endforeach
                        </select>
                    </form>
    
                    @isset($activity)
                    
                        @if ($errors->count() > 0)
                            <div class="alert alert-danger no-border mb-2">
                                <strong>Please Provide Correct Information</strong>
                            </div>
                        @endif
                        <form ng-app="Planner" action="{{ route('activities.indicator-evaluation.save',[Request::segment(2),$activity->id]) }}" id="FormValidation" novalidate method="POST">
                            @csrf
                                
                            <div class="card" ng-controller="ActivityEvaluationController" ng-cloak ng-init="init({{ json_encode(old('participants') ? : []) }})">

                                <div class="card-body">
                                    @if($activity->participant_target_type == 'Limited')
                                        <div class="table-responsive">
                                            <span style="font-size: 20px;">Participants</span>
                                            <div class="mb-2" style="float: right;">
                                                <button class="btn btn-primary btn-sm" ng-click="addParticipant()" type="button" rel="tooltip" title="Add"><i class="material-icons">add_circle</i> Add</button>
                                            </div>
                                            <table class="table table-hover">
                                                <thead class="text-primary">
                                                    <tr>
                                                        <th width="5%">S.N</th>
                                                        <th>Name</th>
                                                        <th width="10%">Age</th>
                                                        <th>Gender</th>
                                                        <th>Ethnicity</th>
                                                        <th width="5%"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    
                                                    <tr ng-repeat="item in participants">
                                                        
                                                        <td>@{{ $index+1 }}</td>
                                                        <td>
                                                            <div class="form-group bmd-form-group">
                                                                <label class="bmd-label-floating"> 
                                                                    Name 
                                                                </label>
                                                                <input type="text" name="participants[@{{ $index }}][name]" ng-model="item.name" class="form-control" required="true"  aria-required="true" required="true" value="">
                                                                
                                                            </div>
        
                                                        </td>
                                                        <td>
                                                            <div class="form-group bmd-form-group">
                                                                <label class="bmd-label-floating"> 
                                                                    Age 
                                                                </label>
                                                                <input type="number" min="0" name="participants[@{{ $index }}][age]" ng-model="item.age" class="form-control" required="true"  aria-required="true" required="true" value="">
                                                                
                                                            </div>
                                        
                                                        </td>
                                                        <td>
                                                            
                                                            <div class="form-group bmd-form-group">
                                                                <select selectpicker data-width="100%" name="participants[@{{ $index }}][gender]" ng-model="item.gender" data-style="select-with-transition" title="Choose Gender" required>
                                                                    <option disabled="" value="">Choose Gender</option>
                                                                    <option value="Male">Male</option>
                                                                    <option value="Female">Female</option>
                                                                    <option value="Others">Others</option>
                                                                    
                                                                </select>
                                                                
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group bmd-form-group">
                                                                <select selectpicker required name="participants[@{{ $index }}][ethnicity]" ng-model="item.ethnicity" data-style="select-with-transition"  title="Choose Ethinicity " data-width="100%">
                                                                    <option disabled="" value="">Choose Ethnicity</option>
                                                                    @foreach(App\Ethnicity::all() as $eth)
                                                                        <option value="{{ $eth->name }}">{{ $eth->name }}</option>
                                                                    @endforeach
                                                                   
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <button ng-hide="participants.length==1" type="button" class="btn btn-fab btn-danger btn-round btn-sm" ng-click="removeParticipant($index)" rel="tooltip" title="Remove"><i class="material-icons">close</i></button>
                                                        </td>
                                                    </tr>
                                                
                                                </tbody>
                                            </table>
                                        </div>
                                    @endif
                                    <hr>         
                                    <div class="table-responsive">
                                        <span style="font-size: 20px;">Reach</span>
                                        <div class="mb-2" style="float: right;">
                                            <button class="btn btn-primary btn-sm" ng-click="addReach()" type="button" rel="tooltip" title="Add"><i class="material-icons">add_circle</i> Add</button>
                                        </div>
                                        <table class="table table-hover">
                                            <thead class="text-primary">
                                                <tr>
                                                    <th width="5%">S.N</th>
                                                    <th style="width:30%">Reach</th>
                                                    <th style="width:20%">Actual Number</th>
                                                    <th>Comment</th>
                                                    <th width="5%"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                <tr ng-repeat="item in reach">
                                                    
                                                    <td>@{{ $index + 1 }}</td>
                                                    <td>
                                                        <div class="form-group bmd-form-group">
                                                            <select selectpicker data-width="100%" name="reach[@{{ $index }}][beneficiary_id]" ng-model="item.beneficiary_id" data-style="select-with-transition" title="Choose Reach Target" required>
                                                                <option disabled="" value="">Choose Reach Target</option>
                                                                @foreach($reach as $r)
                                                                    <option value="{{ $r->id }}">{{ $r->title }}</option>
                                                                @endforeach
                                                                
                                                            </select>
                                                            
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group bmd-form-group">
                                                            <label class="bmd-label-floating"> 
                                                                Actual Number
                                                            </label>
                                                            <input type="number" name="reach[@{{ $index }}][actual_number]" ng-model="item.actual_number" min="0" class="form-control" required="true"  aria-required="true" required="true" value="">
                                                            
                                                        </div>
                                    
                                                    </td>
                                                    <td>
                                                        <div class="form-group bmd-form-group">
                                                            <label class="bmd-label-floating"> Comment </label>
                                                            <textarea name="reach[@{{ $index }}][actual_qualitative_description]" ng-model="item.actual_qualitative_description" class="form-control" rows="1" required></textarea>
                                                            
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button ng-hide="reach.length==1" type="button" class="btn btn-fab btn-danger btn-round btn-sm" ng-click="removeReach($index)" rel="tooltip" title="Remove"><i class="material-icons">close</i></button>
                                                    </td>
                                                </tr>
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                    <hr>    
                                    <div class="table-responsive">
                                        <span style="font-size: 20px;">Budget Planning</span>
                                        <div class="mb-2" style="float: right;">
                                            <button class="btn btn-primary btn-sm" ng-click="addBudget()" type="button" rel="tooltip" title="Add"><i class="material-icons">add_circle</i> Add</button>
                                        </div>
                                        <table class="table table-hover">
                                            <thead class="text-primary">
                                                <tr>
                                                    <th width="5%">S.N</th>
                                                    <th>Budget Category</th>
                                                    <th style="width:30%">Amount (Rs)</th>
                                                    <th width="5%"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                <tr ng-repeat="item in budgets">
                                                    
                                                    <td>@{{ $index + 1 }}</td>
                                                    <td>
                                                        <div class="form-group bmd-form-group">
                                                            <select selectpicker data-width="100%" name="budgets[@{{ $index }}][category_id]" ng-model="item.category_id" data-style="select-with-transition" title="Choose Budget Category" required>
                                                                <option disabled="" value="">Choose Budget Category</option>
                                                                @foreach($budget_categories as $b)
                                                                    <option value="{{ $b->id }}">{{ $b->title }}</option>
                                                                @endforeach
                                                                
                                                            </select>
                                                            
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-group bmd-form-group">
                                                            <label class="bmd-label-floating"> 
                                                                Amount
                                                            </label>
                                                            <input type="number" name="budgets[@{{ $index }}][amount]" ng-model="item.amount" min="0" class="form-control" required="true"  aria-required="true" required="true" value="">
                                                            
                                                        </div>
                                    
                                                    </td>
                                                    
                                                    <td>
                                                        <button ng-hide="budgets.length==1" type="button" class="btn btn-fab btn-danger btn-round btn-sm" ng-click="removeBudget($index)" rel="tooltip" title="Remove"><i class="material-icons">close</i></button>
                                                    </td>
                                                </tr>
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="text-right">
                                <button type="submit" hidden class="btn btn-warning">Save</button>
                                <button type="submit" hidden class="btn btn-success">Publish</button>
                                
                            </div>
                        </form>
                    @endisset
                </div>
            </div>

            
        </div>
    </div>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.8/angular.min.js"></script>
    <script src="/js/angularapp.js"></script>
    <script>
        $(document).ready(function(){
            $('button[type="submit"]').removeAttr('hidden');
           
            $('#selectActivity').on('change', function(){
                if(!$(this).val()) return
                location.href = '/project/{{Request::segment(2)}}/activities/'+$(this).val()+'/indicator-evaluation';
            })
        });
    </script>
@endsection
