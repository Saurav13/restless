@extends('layouts.project')

@section('body')
<div class="row">
                        
                        
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-rose card-header-icon">

                <div class="card-icon">
                    <i class="material-icons">assignment</i>
                </div>
                @isset($activity)

                    <button data-toggle="modal" data-target="#addReport" class="btn btn-primary pull-right">Add Record</button>

                @endisset
                <h4 class="card-title">Progress Report </h4>
            </div>
            <div class="card-body">
                
                <form action="" method="GET">
                    <select class="selectpicker"  id="selectOutcome" data-style="btn btn-primary btn-round" title="Select an Activity">
                        <option disabled {{ !isset($activity) ? 'selected' : '' }}>Select an Activity</option>
                        @foreach ($outcomes as $o)
                            <option value="{{ $o->id }}" {{ isset($activity) && $activity->outcome_id == $o->id ? 'selected' : '' }}>{{ $o->title }}</option>
                        @endforeach
                    </select>
                    <select class="selectpicker"  id="selectOutput" data-style="btn btn-primary btn-round" title="Select an Activity">
                        <option disabled {{ !isset($activity) ? 'selected' : '' }}>Select an Activity</option>
                        @foreach ($activities as $o)
                            <option value="{{ $o->id }}" outcome="{{ $o->outcome_id }}" {{ isset($activity) && $activity->id == $o->id ? 'selected' : '' }}>{{ $o->title }}</option>
                        @endforeach
                    </select>
                </form>

                @isset($activity)
                    <ul class="navbar-nav" style="text-align: right;">
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#pablo" id="navbarDropdownSort" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="material-icons">today</i>
                            <span>Sort By Date</span>
                            <div class="ripple-container"></div></a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownSort">
                            <a class="dropdown-item" href="{{ route('activities.progress-report.get',['project_id'=>Request::segment(2),'output_id'=>$activity->id,'sort'=>'desc']) }}">Newest First</a>
                            <a class="dropdown-item" href="{{ route('activities.progress-report.get',['project_id'=>Request::segment(2),'output_id'=>$activity->id,'sort'=>'asc']) }}">Oldest Frist</a>
                            </div>
                        </li>
                    </ul>
                
                    @forelse($logs as $log)

                        <div class="card">
                            
                            <div class="card-body">
                                <p class="card-category">{{ date('M j, Y h:i A', strtotime($log->date)) }}</p>
                                
                                <div class="card-description" style="color: #3c4858;">
                                    {!! $log->log !!}
                                </div>
                                <div class="stats" style="    text-align: right;">
                                    <p class="card-category"><i class="material-icons">person</i> {{ $log->creater->name }}</p>
                                </div>
                            </div>
                        </div>
                    @empty
                        <div class="alert alert-warning alert-dismissible alert-with-icon" data-notify="container">
                            <i class="material-icons" data-notify="icon">notifications</i>
                            <span data-notify="message">Currently there are no progress reports to display</span>
                        </div>
                    @endforelse
                    {{ $logs->appends(request()->input())->links() }}
                @endisset
            </div>
        </div>
    </div>

</div>
@isset($activity)

    <div class="modal fade" id="addReport" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Record</h5>
                    <button type="button" class="close" data-dismiss="modal">
                        <i class="material-icons">clear</i>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="FormValidation" action="{{ route('activities.progress-report.save',[Request::segment(2),$activity->id]) }}" method="POST" novalidate="novalidate">
                        @csrf
                        
                        <div class="form-group bmd-form-group">
                            <label class="bmd-label-floating"> Description* </label>
                            @if ($errors->has('log'))
                                <div class="alert alert-danger no-border mb-2">
                                    <strong>{{ $errors->first('log') }}</strong>
                                </div>
                            @endif
                            <textarea id="log" name="log" class="form-control {{ $errors->has('log') ? ' has-danger' : '' }}"  rows="5">{{ old('log') }}</textarea>
                            
                        </div>
                        <div class="modal-footer" style="padding: 0;">
                        
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endisset


@endsection

@section('js')
    @if($errors->count() > 0)
        <script>
            $('#addReport').modal('show');
        </script>
    @endif
    
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>

    <script>
        $(document).ready(function(){
            var outcome = $('#selectOutcome').val();

            $("#selectOutput option").hide();

            if(outcome){
                $("#selectOutput option[outcome='"+outcome+"']").show();
            }
            $('#selectOutput').selectpicker('refresh');

            $('#selectOutcome').on('change',function(){
                outcome = $(this).val();
                $("#selectOutput option").hide();
                $("#selectOutput option[outcome='"+outcome+"']").show();
                $("#selectOutput").val('');
                $('#selectOutput').selectpicker('refresh');
            });

            $('#selectOutput').on('change', function(){
                if(!$(this).val()) return
                location.href = '/project/{{Request::segment(2)}}/activities/'+$(this).val()+'/progress-report';
            })

            tinymce.init({
                selector: 'textarea#log',
                theme: "modern",
                height: 200,
                plugins: [
                    "advlist autolink lists link charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code",
                    "insertdatetime media nonbreaking save table contextmenu directionality",
                    "emoticons paste textcolor colorpicker textpattern"
                ],
                toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
                toolbar2: "print preview | forecolor backcolor emoticons",
            });
            $('#FormValidation').submit(function(e){
                e.preventDefault();

                var editorContent = tinyMCE.get('log').getContent();
                
                if (editorContent == '')
                {
                    $.notify({
                        message: 'Content cannot be empty.' 
                    },{
                        type: 'danger'
                    });
                }
                else
                {
                    $('#FormValidation').unbind('submit').submit();
                }
            });
        });
    </script>
@endsection
