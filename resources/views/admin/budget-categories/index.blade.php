@extends('layouts.admin')
@section('body')
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="{{route($base_route.'.index')}}">{{$panel}}</a></li>
        </ol>
    </nav>
    <div class="content">
        <div class="container-fluid">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">category</i>
                            </div>
                            <div class="pull-right">
                                <button class="card-icon btn btn-primary btn-round" data-toggle="modal" data-target="#AddModal">
                                    <i class="material-icons">add</i> Add {{$panel}}
                                </button>
                            </div>
                            <h4 class="card-title">{{$panel}} List</h4>

                        </div>

                        <div class="card-body">

                            @include('admin.common.messages')
                            @if(count($budget_categories) == null)
                                <div class="alert alert-warning alert-with-icon" data-notify="container">
                                    <i class="material-icons" data-notify="icon">notifications</i>
                                    <span data-notify="message">Currently there are no {{$panel}}'s to display</span>
                                </div>
                            @else
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th>Title</th>
                                            {{-- <th>Created At</th>
                                            <th>Last Updated At</th> --}}
                                            <th class="text-right">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($budget_categories as $b)
                                            <tr>
                                                <td class="text-center">{{$loop->iteration}}</td>
                                                <td><strong>{{$b->title}}</strong></td>
                                                {{-- @php
                                                    $created_at = \Carbon\Carbon::parse($b->created_at);
                                                    $updated_at = \Carbon\Carbon::parse($b->updated_at);
                                                @endphp
                                                <td>{!! date('l M j, Y h:i A', strtotime($created_at)).' <b><i style="font-size: 12px; color: #ed1c24;">('. $created_at->diffForHumans().')</i></b>'!!}</td>
                                                <td>{!! date('l M j, Y h:i A', strtotime($updated_at)).' <b><i style="font-size: 12px; color: #ed1c24;">('. $updated_at->diffForHumans().')</i></b>'!!}</td> --}}
                                                <td class="td-actions text-right">
                                                    <button data-toggle="modal" data-target="#EditModal" data-action="{{route($base_route.'.update',$b->id)}}" cid="{{$b->id}}" value="{{$b->title}}" type="button" rel="tooltip" class="btn btn-success edit" data-original-title="Edit" title="Edit">
                                                        <i class="material-icons">edit</i>
                                                    </button>
                                                    <button type="button" cid="{{$b->id}}" data-action="{{route($base_route.'.destroy',$b->id)}}" panel = {{$panel}} rel="tooltip" class="btn btn-danger delete" data-original-title="Delete" title="Delete">
                                                        <i class="material-icons">close</i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {!! $budget_categories->links() !!}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Create Modal -->
    <div class="modal fade" id="AddModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add {{$panel}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route($base_route.'.store')}}" method="POST">
                    <div class="modal-body">
                        {{csrf_field()}}
                        <div class="form-group bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="material-icons">category</i></div>
                                </div>
                                <input type="text" class="form-control" name="title" placeholder="Title">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Create Modal -->

    <!-- Edit Modal -->
    <div class="modal fade" id="EditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit {{$panel}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST" id="edit_form" enctype="multipart/form-data">
                    <div class="modal-body">
                        {{csrf_field()}}
                        {{ method_field('PUT') }}
                        <div class="form-group bmd-form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="material-icons">category</i></div>
                                </div>
                                <input type="text" id="panel_title" class="form-control" value="" name="title" placeholder="Title">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Edit Modal -->
@endsection
@section('js')
    <script>
        $('.edit').click(function(){
            var title = $(this).attr("value");
            var url = $(this).attr("data-action");
            $('#panel_title').val(title);
            console.log(url);
            $('#edit_form').attr('action', url);
        });
    </script>
@endsection