@extends('layouts.admin')
@section('body')
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="{{route($base_route.'.index')}}">{{$panel}}</a></li>
        </ol>
    </nav>
    <div class="content">
        <div class="container-fluid">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">assignment</i>
                            </div>
                            <div class="pull-right">
                                <a href="{{route($base_route.'.create')}}" class="card-icon btn btn-primary btn-round">
                                    <i class="material-icons">add</i> Create {{$panel}}
                                </a>
                            </div>
                            <h4 class="card-title">{{$panel}} List</h4>

                        </div>

                        <div class="card-body">

                            @include('admin.common.messages')
                            @if(count($projects) == null)
                            <div class="alert alert-warning alert-with-icon" data-notify="container">
                                <i class="material-icons" data-notify="icon">notifications</i>
                                <span data-notify="message">Currently there are no {{$panel}}'s to display</span>
                            </div>
                            @else
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th>Project Title</th>
                                        <th>Code</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Total Budget</th>
                                        <th class="text-right">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($projects as $p)
                                    <tr>
                                        <td class="text-center">{{$loop->iteration}}</td>
                                        <td>{{$p->title}}</td>
                                        @if($p->status == "drafted")
                                            <td>N/A</td>
                                        @else
                                            <td>{{$p->code ? $p->code : 'N/A'}}</td>
                                        @endif
                                        @php
                                        $start_date = \Carbon\Carbon::parse($p->start_date);
                                        $end_date = \Carbon\Carbon::parse($p->end_date);
                                        @endphp


                                        <td>{{date_format($start_date, "M d, Y") }}</td>
                                        <td>{{date_format($end_date,"M d, Y")}}</td>
                                        <td>${{$p->total_budget}}</td>
                                        <td class="td-actions text-right">
                                        <a href="/project/{{$p->id}}/dashboard" type="button" rel="tooltip" class="btn btn-info" data-original-title="View" title="View">
                                                <i class="material-icons">remove_red_eye</i>
                                            </a>
                                            <a href="{{route($base_route.'.edit',$p->id)}}" type="button" rel="tooltip" class="btn btn-success" data-original-title="Edit" title="Edit">
                                                <i class="material-icons">edit</i>
                                            </a>
                                            <button type="button" cid="{{$p->id}}" data-action="{{route($base_route.'.destroy',$p->id)}}" panel = {{$panel}} rel="tooltip" class="btn btn-danger delete" data-original-title="Delete" title="Delete">
                                                <i class="material-icons">close</i>
                                            </button>
                                        </td>
                                    </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                               {!! $projects->links() !!}
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
