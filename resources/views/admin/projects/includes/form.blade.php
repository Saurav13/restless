<div class="row">
    <label class="col-sm-2 col-form-label">Title*</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::text('title', null, ['placeholder' => 'Title of the project', 'class' => 'form-control','required'=>"true"]); !!}
        </div>
    </div>
</div>
<div class="row">
    <label class="col-sm-2 col-form-label">Description</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::textarea('description', null, ['placeholder' => 'General description of the project', 'class' => 'form-control',"rows" => '4']); !!}
        </div>
    </div>
</div>
<div class="row">
    <label class="col-sm-2 col-form-label">Goal Description</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::textarea('goal_description', null, ['placeholder' => 'Goal of the project', 'class' => 'form-control',"rows" => '4']); !!}
        </div>
    </div>
</div>
<div class="row">
    <label class="col-sm-2 col-form-label">Goal Terminology*</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::text('goal_term', null, ['placeholder' => 'Goal Terminology', 'class' => 'form-control','required'=>"true"]); !!}
        </div>
    </div>
</div>
<div class="row">
    <label class="col-sm-2 col-form-label">Initial Situation</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::textarea('initial_situation', null, ['placeholder' => 'Initial situation of the project', 'class' => 'form-control',"rows" => '4']); !!}
        </div>
    </div>
</div>
<div class="row">
    <label class="col-sm-2 col-form-label">Expected Situation</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::textarea('expected_situation', null, ['placeholder' => 'Expected situation after project completion', 'class' => 'form-control',"rows" => '4']); !!}
        </div>
    </div>
</div>

<div class="row">
    <label class="col-sm-2 col-form-label">Project Status*</label>
    <div class="col-sm-8">
        <div class="form-group">
            <div class="dropdown bootstrap-select">
                {!! Form::select('status', array('ongoing' => 'Ongoing', 'drafted' => 'Drafted','completed' => 'Completed'),'drafted', ['class' => 'selectpicker','data-style' => 'btn btn-primary btn-round','title' => 'Choose Status','required','tabindex'=>'-98']); !!}
            </div>
        </div>
    </div>
</div>


<div class="row">
    <label class="col-sm-2 col-form-label">Start Date*</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::text('start_date', null, ['placeholder' => 'Start date of the project', 'class' => 'form-control datepicker','required'=>"true"]); !!}
        </div>
    </div>
</div>

<div class="row">
    <label class="col-sm-2 col-form-label">End Date*</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::text('end_date', null, ['placeholder' => 'End date of the project', 'class' => 'form-control datepicker','required'=>"true"]); !!}
        </div>
    </div>
</div>

<div class="row">
    <label class="col-sm-2 col-form-label">Total Budget*</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::number('total_budget', null, ['placeholder' => 'Total Budget allocated for the project', 'class' => 'form-control','required'=>"true"]); !!}
        </div>
    </div>
</div>











