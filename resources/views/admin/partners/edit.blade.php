@extends('layouts.admin')
@section('body')
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route($base_route.'.index')}}">{{$panel}}</a></li>
            <li class="breadcrumb-item active"><a href="{{route($base_route.'.create')}}">Update {{$panel}}</a></li>
        </ol>
    </nav>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card ">
                        <div class="card-header card-header-primary card-header-text">
                            <div class="card-text">
                                <h4 class="card-title">Update {{$panel}}</h4>
                            </div>
                            <div class="pull-right">
                                <a href="{{route($base_route.'.show',$partners->id)}}" class="card-icon btn btn-success btn-round">
                                    <i class="material-icons">remove_red_eye</i> View {{$panel}}
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            @include('admin.common.messages')
                            {!! Form::model($partners,['url' => route($base_route.'.update',$partners->id), 'class' => 'form-horizontal', 'method' => 'PUT','role' => 'form','id' => 'ValidateForm','enctype' => 'multipart/form-data']) !!}
                            {{csrf_field()}}
                            @include($base_view.'.includes.form')
                            <div class="card-footer pull-right">
                                <button type="submit" class="btn btn-fill btn-success">Submit</button>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script>
        function setFormValidation(id) {
            $(id).validate({
                highlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
                    $(element).closest('.form-check').removeClass('has-success').addClass('has-danger');
                },
                success: function(element) {
                    $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
                    $(element).closest('.form-check').removeClass('has-danger').addClass('has-success');
                },
                errorPlacement: function(error, element) {
                    $(element).closest('.form-group').append(error);
                },
            });
        }

        $(document).ready(function() {
            setFormValidation('#ValidateForm');
            md.initFormExtendedDatetimepickers();
            if ($('.slider').length != 0) {
                md.initSliders();
            }
        });
   
        var count = 1;
        $('.add_registration_detail_1').on('click', function() {
            newcount = count + 1;
            var element = $('.registration_detail_1');
            var newelement = element.clone();
            newelement.removeAttr("id");
            newelement.attr("id",'registration_'+newcount);
            newelement.removeClass('registration_detail_1');
            newelement.find(':input').val('');
            newelement.find('label').empty();
            newelement.find('button').addClass("remove_registration_detail").attr('div_id',newcount).addClass("btn btn-danger").removeClass('add_registration_detail_1');
            newelement.find("i").text('remove');
            newelement.insertAfter(element);
            count++;
        });
    
       $(document).on('click', '.remove_registration_detail', function(){
            var id = $(this).attr('div_id');
            $('#registration_'+id).remove();
       });
    </script>
@endsection