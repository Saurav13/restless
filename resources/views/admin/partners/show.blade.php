@extends('layouts.admin')
@section('body')
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route($base_route.'.index')}}">{{$panel}}</a></li>
            <li class="breadcrumb-item active"><a href="{{route($base_route.'.show',$data->id)}}">View {{$panel}}</a></li>
        </ol>
    </nav>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">people_outline</i>
                            </div>
                            <div class="pull-right">
                                <a href="{{route($base_route.'.index')}}" class="card-icon btn btn-primary btn-round">
                                    <i class="material-icons">list</i> List {{$panel}}
                                </a>
                                <a href="{{route($base_route.'.create')}}" class="card-icon btn btn-primary btn-round">
                                    <i class="material-icons">add</i> Create {{$panel}}
                                </a>
                                <a href="{{route($base_route.'.edit',$data->id)}}" class="card-icon btn btn-primary btn-round">
                                    <i class="material-icons">edit</i> Edit {{$panel}}
                                </a>
                                <button class="card-icon btn btn-primary btn-round delete-view">
                                    <i class="material-icons">close</i> Delete {{$panel}}
                                </button>
                                <form action="{{route($base_route.'.destroy',$data->id,$from = "view")}}" id="delete_from_view" method="POST">
                                    {{method_field('DELETE')}}
                                    <input type="hidden" name="id" value="{{$data->id}}">
                                    {{csrf_field()}}
                                </form>

                            </div>
                            <h4 class="card-title">{{$data->title}}</h4>
                        </div>
                        <div class="card-body">
                            @include('admin.common.messages')
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th width="200">{{$panel}} Attributes</th>
                                        <th>Values</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($view_data as $key => $value)
                                        <tr>
                                            <td><strong>{{ucwords(str_replace('_', ' ', $key))}}</strong></td>
                                            <td>{!! $value !!}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
