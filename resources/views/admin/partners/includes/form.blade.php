<div class="row">
    <label class="col-sm-2 col-form-label">Full Name*</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::text('fullname', null, ['placeholder' => 'Full Name of the partner organization', 'class' => 'form-control','required'=>"true"]); !!}
        </div>
    </div>
</div>
<div class="row">
    <label class="col-sm-2 col-form-label">Short Name*</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::text('shortname', null, ['placeholder' => 'Short Name of the partner organization', 'class' => 'form-control','required'=>"true"]); !!}
        </div>
    </div>
</div>
<div class="row">
    <label class="col-sm-2 col-form-label">Description</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::textarea('description', null, ['placeholder' => 'General description of the partner organization', 'class' => 'form-control',"rows" => '4']); !!}
        </div>
    </div>
</div>
<div class="row">
    <label class="col-sm-2 col-form-label">Office Address</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::text('office_address', null, ['placeholder' => 'Office address of the partner organization', 'class' => 'form-control']); !!}
        </div>
    </div>
</div>

<div class="row">
    <label class="col-sm-2 col-form-label">Contact Person</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::text('contact_person', null, ['placeholder' => 'Contact Person', 'class' => 'form-control']); !!}
        </div>
    </div>
</div>

<div class="row">
    <label class="col-sm-2 col-form-label">Contact Person Email</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::email('contact_email', null, ['placeholder' => 'Contact Email', 'class' => 'form-control']); !!}
        </div>
    </div>
</div>

<div class="row">
    <label class="col-sm-2 col-form-label">Contact Number</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::number('contact_number', null, ['placeholder' => 'Contact Number', 'class' => 'form-control']); !!}
        </div>
    </div>
</div>

@if(old('reg_number') != null || old('reg_details') != null)
<?php
    $loop_count = count(old('reg_number')) > count(old('reg_details')) ? count(old('reg_number')) : count(old('reg_details')); 
?>
@for($i = 1; $i <= $loop_count ; $i++)
<div class="row registration_detail_{{$i}}" id="registration_{{$i}}">
    @if($i == 1)
    <label class="col-sm-2 col-form-label registration_label">Registration Details</label>
    @else
    <label class="col-sm-2 col-form-label registration_label"></label>
    @endif
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::text('reg_number[]', null, ['placeholder' => 'Registration Number of the organization', 'class' => 'form-control reg_number_'.$i]); !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::text('reg_details[]', null, ['placeholder' => 'Registration Details of the organization', 'class' => 'form-control reg_detail'.$i]); !!}
        </div>
    </div>
       <div class="col-sm-2" style="padding-left: 0px;padding-top: 5px;">
        @if($i == 1)
        <button type="button" rel="tooltip" data-title="Add More Registration Information" class="btn btn-primary btn-fab btn-fab-mini btn-round add_registration_detail_{{$i}}">
            <i class="material-icons">add</i>
          </button>
          @else
          <button type="button" rel="tooltip" class="btn btn-danger btn-fab btn-fab-mini btn-round remove_registration_detail" div_id="{{$i}}">
            <i class="material-icons">remove</i>
          </button>
          @endif
    </div>
</div>
@endfor
@elseif(isset($partners->reg_details))
<?php
    $loop_count = count(json_decode($partners->reg_details)); 
?>

@for($i = 1; $i <= $loop_count ; $i++)
<div class="row registration_detail_{{$i}}" id="registration_{{$i}}">
    @if($i == 1)
    <label class="col-sm-2 col-form-label registration_label">Registration Details</label>
    @else
    <label class="col-sm-2 col-form-label registration_label"></label>
    @endif
    <div class="col-sm-4">
        <div class="form-group">
        <input type="text" name="reg_number[]" placeholder="Registration Number of the organization" class="form-control reg_number_{{$i}}" value="{{json_decode($partners->reg_details)[$i - 1]->registration_number}}">
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <input type="text" name="reg_detail[]" placeholder="Registration Details of the organization" class="form-control reg_detail_{{$i}}" value="{{json_decode($partners->reg_details)[$i - 1]->registration_details}}">
        </div>
    </div>
       <div class="col-sm-2" style="padding-left: 0px;padding-top: 5px;">
        @if($i == 1)
        <button type="button" rel="tooltip" data-title="Add More Registration Information" class="btn btn-primary btn-fab btn-fab-mini btn-round add_registration_detail_1">
            <i class="material-icons">add</i>
          </button>
          @else
          <button type="button" rel="tooltip" class="btn btn-danger btn-fab btn-fab-mini btn-round remove_registration_detail" div_id="{{$i}}">
            <i class="material-icons">remove</i>
          </button>
          @endif
    </div>
</div>
@endfor
@else
<div class="row registration_detail_1" id="registration_1">
    <label class="col-sm-2 col-form-label registration_label">Registration Details</label>
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::text('reg_number[]', null, ['placeholder' => 'Registration Number of the organization', 'class' => 'form-control reg_number_1']); !!}
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            {!! Form::text('reg_detail[]', null, ['placeholder' => 'Registration Details of the organization', 'class' => 'form-control reg_detail_1']); !!}
        </div>
    </div>
       <div class="col-sm-2" style="padding-left: 0px;padding-top: 5px;">
        <button type="button" rel="tooltip" data-title="Add More Registration Information" class="btn btn-primary btn-fab btn-fab-mini btn-round add_registration_detail_1">
            <i class="material-icons" id="registration_icon_1">add</i>
          </button>
    </div>
</div>
@endif

<div class="row">
    <label class="col-sm-2 col-form-label">Pan Number</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::text('pan_number', null, ['placeholder' => 'Pan Number of the organization', 'class' => 'form-control']); !!}
        </div>
    </div>
</div>


<div class="row">
    <label class="col-sm-2 col-form-label">Organization Email</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::email('organization_email', null, ['placeholder' => 'Organizations Email', 'class' => 'form-control']); !!}
        </div>
    </div>
</div>

<div class="row">
    <label class="col-sm-2 col-form-label">Website</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::text('website', null, ['placeholder' => 'Organization website link', 'class' => 'form-control']); !!}
        </div>
    </div>
</div>

<div class="row">
    <label class="col-sm-2 col-form-label">Partner Organization Logo</label>
    <div class="col-sm-8">
        <div class="fileinput text-center fileinput-new" data-provides="fileinput">
            <div class="fileinput-new thumbnail">
                @if(isset($partners->logo))
                    <img src="{{asset('images/partners'.'/'.$partners->logo)}}" alt="{{$partners->fullname}}">
                @endif
            </div>
            <div class="fileinput-preview fileinput-exists thumbnail" style=""></div>
            <div>
                                              <span class="btn btn-rose btn-round btn-file">
                                                <span class="fileinput-new">Select Organizations image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="hidden" value="" name="logo"><input type="file" name="logo">
                                              <div class="ripple-container"></div></span>
                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove<div class="ripple-container"><div class="ripple-decorator ripple-on ripple-out" style="left: 58.7344px; top: 24px; background-color: rgb(255, 255, 255); transform: scale(15.5098);"></div></div></a>
            </div>
        </div>
    </div>
</div>










