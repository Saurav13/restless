<table class="table">
    <thead>
    <tr>
        <th>#</th>
        <th>Registration Number</th>
        <th>Registration Details</th>
    </tr>
    </thead>
    <tbody>
        @foreach($registration_details as $reg)
        <tr>
        <td><strong>{{$loop->iteration}}</strong></td>
            <td>{!! $reg->registration_number ? $reg->registration_number : 'N/A' !!}</td>
            <td>{!! $reg->registration_details ? $reg->registration_details : 'N/A'  !!}</td>
        </tr>
    @endforeach
    </tbody>
</table>