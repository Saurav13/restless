<div class="row">
    <label class="col-sm-2 col-form-label">Full Name*</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::text('fullname', null, ['placeholder' => 'Full Name of the donor organization', 'class' => 'form-control','required'=>"true"]); !!}
        </div>
    </div>
</div>
<div class="row">
    <label class="col-sm-2 col-form-label">Short Name*</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::text('shortname', null, ['placeholder' => 'Short Name of the donor organization', 'class' => 'form-control','required'=>"true"]); !!}
        </div>
    </div>
</div>
<div class="row">
    <label class="col-sm-2 col-form-label">Country</label>
    <div class="col-sm-8">
        <div class="form-group">
            <div class="dropdown bootstrap-select">
                {!! Form::select('country', $country_list,null, ['class' => 'selectpicker','data-style' => 'select-with-transition','title' => 'Choose Status','required','tabindex'=>'-98']); !!}
            </div>
        </div>
    </div>
</div>

<div class="row">
    <label class="col-sm-2 col-form-label">Description</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::textarea('description', null, ['placeholder' => 'General description of the donor organization', 'class' => 'form-control',"rows" => '4']); !!}
        </div>
    </div>
</div>

<div class="row">
    <label class="col-sm-2 col-form-label">MOU Signatory</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::text('mou_signaory', null, ['placeholder' => 'MOU Signatory', 'class' => 'form-control']); !!}
        </div>
    </div>
</div>

<div class="row">
    <label class="col-sm-2 col-form-label">Office Address</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::text('office_address', null, ['placeholder' => 'Office address of the donor organization', 'class' => 'form-control']); !!}
        </div>
    </div>
</div>

<div class="row">
    <label class="col-sm-2 col-form-label">Contact Person</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::text('contact_person', null, ['placeholder' => 'Contact Person', 'class' => 'form-control']); !!}
        </div>
    </div>
</div>

<div class="row">
    <label class="col-sm-2 col-form-label">Contact Person Number</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::number('contact_number', null, ['placeholder' => 'Contact Number', 'class' => 'form-control']); !!}
        </div>
    </div>
</div>

<div class="row">
    <label class="col-sm-2 col-form-label">Registration Number</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::email('reg_number', null, ['placeholder' => 'Registration Number', 'class' => 'form-control']); !!}
        </div>
    </div>
</div>

<div class="row">
    <label class="col-sm-2 col-form-label">Pan Number</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::text('pan_number', null, ['placeholder' => 'Pan Number of the organization', 'class' => 'form-control']); !!}
        </div>
    </div>
</div>

<div class="row">
    <label class="col-sm-2 col-form-label">Email</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::email('email', null, ['placeholder' => 'Email address', 'class' => 'form-control']); !!}
        </div>
    </div>
</div>

<div class="row">
    <label class="col-sm-2 col-form-label">Website</label>
    <div class="col-sm-8">
        <div class="form-group">
            {!! Form::text('website', null, ['placeholder' => 'Organization website link', 'class' => 'form-control']); !!}
        </div>
    </div>
</div>

<div class="row">
    <label class="col-sm-2 col-form-label">Donot Organization Logo</label>
    <div class="col-sm-8">
        <div class="fileinput text-center fileinput-new" data-provides="fileinput">
            <div class="fileinput-new thumbnail">
                @if(isset($donors->logo))
                    <img src="{{asset('images/donors'.'/'.$donors->logo)}}" alt="{{$donors->fullname}}">
                @endif
            </div>
            <div class="fileinput-preview fileinput-exists thumbnail" style=""></div>
            <div>
                                              <span class="btn btn-rose btn-round btn-file">
                                                <span class="fileinput-new">Select Organizations image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="hidden" value="" name="logo"><input type="file" name="logo">
                                              <div class="ripple-container"></div></span>
                <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove<div class="ripple-container"><div class="ripple-decorator ripple-on ripple-out" style="left: 58.7344px; top: 24px; background-color: rgb(255, 255, 255); transform: scale(15.5098);"></div></div></a>
            </div>
        </div>
    </div>
</div>










