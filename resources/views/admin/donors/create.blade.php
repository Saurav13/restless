@extends('layouts.admin')
@section('body')
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{route($base_route.'.index')}}">{{$panel}}</a></li>
            <li class="breadcrumb-item active"><a href="{{route($base_route.'.create')}}">Create {{$panel}}</a></li>
        </ol>
    </nav>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card ">
                        <div class="card-header card-header-primary card-header-text">
                            <div class="card-text">
                                <h4 class="card-title">Create {{$panel}}</h4>
                            </div>
                        </div>
                        <div class="card-body">
                            {!! Form::open(['url' => route($base_route.'.store'), 'class' => 'form-horizontal', 'method' => 'post','role' => 'form','id' => 'ValidateForm','enctype' => 'multipart/form-data']) !!}
                            {{csrf_field()}}
 @include($base_view.'.includes.form')
                            <div class="card-footer pull-right">
                                <button type="submit" class="btn btn-fill btn-success">Submit</button>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script>
        function setFormValidation(id) {
            $(id).validate({
                highlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
                    $(element).closest('.form-check').removeClass('has-success').addClass('has-danger');
                },
                success: function(element) {
                    $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
                    $(element).closest('.form-check').removeClass('has-danger').addClass('has-success');
                },
                errorPlacement: function(error, element) {
                    $(element).closest('.form-group').append(error);
                },
            });
        }

        $(document).ready(function() {
            setFormValidation('#ValidateForm');
        });

    </script>
    <script>
        $(document).ready(function() {
            // initialise Datetimepicker and Sliders
            md.initFormExtendedDatetimepickers();
            if ($('.slider').length != 0) {
                md.initSliders();
            }
        });
    </script>
@endsection