@extends('layouts.admin')
@section('body')
    <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
            <li class="breadcrumb-item active"><a href="{{route($base_route.'.index')}}">{{$panel}}</a></li>
        </ol>
    </nav>
    <div class="content">
        <div class="container-fluid">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary card-header-icon">
                            <div class="card-icon">
                                <i class="material-icons">monetization_on</i>
                            </div>
                            <div class="pull-right">
                                <a href="{{route($base_route.'.create')}}" class="card-icon btn btn-primary btn-round">
                                    <i class="material-icons">add</i> Create {{$panel}}
                                </a>
                            </div>
                            <h4 class="card-title">{{$panel}} List</h4>

                        </div>

                        <div class="card-body">

                            @include('admin.common.messages')
                            @if(count($donors) == null)
                                <div class="alert alert-warning alert-with-icon" data-notify="container">
                                    <i class="material-icons" data-notify="icon">notifications</i>
                                    <span data-notify="message">Currently there are no {{$panel}}'s to display</span>
                                </div>
                            @else
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th>Name</th>
                                            <th>Country</th>
                                            <th>Logo</th>
                                            <th>Contact Person</th>
                                            <th>Contact Number</th>
                                            <th>Email</th>
                                            <th class="text-right">Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($donors as $p)
                                            <tr>
                                                <td class="text-center">{{$loop->iteration}}</td>
                                                <td>{{$p->fullname}}</td>
                                                <td>{{$p->country}}</td>
                                                <td> {!! $p->logo ? '<img class="img" height="100" width="120"  src="'.asset('images/donors'.'/'.$p->logo).'">' : 'N/A'!!}</td>
                                                <td>{{$p->contact_person ? $p->contact_person : 'N/A'}}</td>
                                                <td>{{$p->contact_number ? $p->contact_number : 'N/A'}}</td>
                                                <td>{{$p->email ? $p->email : 'N/A'}}</td>
                                                <td class="td-actions text-right">
                                                    <a href="{{route($base_route.'.show',$p->id)}}" type="button" rel="tooltip" class="btn btn-info" data-original-title="View" title="View">
                                                        <i class="material-icons">remove_red_eye</i>
                                                    </a>
                                                    <a href="{{route($base_route.'.edit',$p->id)}}" type="button" rel="tooltip" class="btn btn-success" data-original-title="Edit" title="Edit">
                                                        <i class="material-icons">edit</i>
                                                    </a>
                                                    <button type="button" cid="{{$p->id}}" data-action="{{route($base_route.'.destroy',$p->id)}}" panel = {{$panel}} rel="tooltip" class="btn btn-danger delete" data-original-title="Delete" title="Delete">
                                                        <i class="material-icons">close</i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                    {!! $donors->links() !!}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
