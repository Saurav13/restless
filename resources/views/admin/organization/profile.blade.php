@extends('layouts.admin')

@section('body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-icon card-header-rose">
                        <div class="card-icon">
                            <i class="material-icons">perm_identity</i>
                        </div>
                        <h4 class="card-title">Update Profile -
                            <small class="category">Complete Organization's profile</small>
                        </h4>
                    </div>

                    <div class="card-body">
                      @include('admin.common.messages')
                        <form id="ProfileValidation" action="{{route('organization.profile.update')}}" enctype="multipart/form-data" method="POST">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group bmd-form-group">
                                        <label class="bmd-label-floating">Full name*</label>
                                        <input type="text" value="{{$profile_info->fullname}}" name="fullname" required="true" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group bmd-form-group">
                                        <label class="bmd-label-floating">Short name*</label>
                                        <input type="text" name="shortname" value="{{$profile_info->shortname}}" required="true" class="form-control">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group bmd-form-group">
                                        <label class="bmd-label-floating">Contact person</label>
                                        <input type="text" name="contact_person" value="{{$profile_info->contact_person}}" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group bmd-form-group">
                                        <label class="bmd-label-floating">Contact Number</label>
                                        <input type="text" name="contact_number" value="{{$profile_info->contact_number}}" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group bmd-form-group">
                                        <label class="bmd-label-floating">Email</label>
                                        <input type="email" name="email" value="{{$profile_info->email}}" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group bmd-form-group">
                                        <label class="bmd-label-floating">Office Address</label>
                                        <input type="text" name="office_address" value="{{$profile_info->office_address}}" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group bmd-form-group">
                                        <label class="bmd-label-floating">Website</label>
                                        <input type="text" name="website" value="{{$profile_info->website}}" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group bmd-form-group">
                                        <label class="bmd-label-floating">Registration Number</label>
                                        <input type="text" name="reg_number" value="{{$profile_info->reg_number}}" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group bmd-form-group">
                                        <label class="bmd-label-floating">Pan Number</label>
                                        <input type="text" name="pan_number" value="{{$profile_info->pan_number}}" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <div class="form-group bmd-form-group">
                                            <label class="bmd-label-floating"> Description about the organization</label>
                                            <textarea class="form-control" name="description" rows="5">{{$profile_info->description}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="fileinput text-center fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail">
                                            @if(isset($profile_info->logo))
                                                    <img src="{{asset('images/organization_images'.'/'.$profile_info->logo)}}" alt="{{$profile_info->fullname}}">
                                                @else
                                                    <img src="{{asset('assets/img/image_placeholder.jpg')}}" alt="{{$profile_info->fullname}}">
                                            @endif
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style=""></div>
                                        <div>
                                              <span class="btn btn-rose btn-round btn-file">
                                                <span class="fileinput-new">Select Organizations image</span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="hidden" value="" name="logo"><input type="file" name="logo">
                                              <div class="ripple-container"></div></span>
                                            <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove<div class="ripple-container"><div class="ripple-decorator ripple-on ripple-out" style="left: 58.7344px; top: 24px; background-color: rgb(255, 255, 255); transform: scale(15.5098);"></div></div></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-rose pull-right">Update Profile</button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        function setFormValidation(id) {
            $(id).validate({
                highlight: function(element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-danger');
                    $(element).closest('.form-check').removeClass('has-success').addClass('has-danger');
                },
                success: function(element) {
                    $(element).closest('.form-group').removeClass('has-danger').addClass('has-success');
                    $(element).closest('.form-check').removeClass('has-danger').addClass('has-success');
                },
                errorPlacement: function(error, element) {
                    $(element).closest('.form-group').append(error);
                },
            });
        }

        $(document).ready(function() {
            setFormValidation('#ProfileValidation');
        });
    </script>
@endsection

